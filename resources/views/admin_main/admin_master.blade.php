<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('backend/images/favvicon.ico') }}">

    <title>@yield('title')</title>
    
	<!-- Vendors Style-->
	<link rel="stylesheet" href="{{ asset('backend/css/vendors_css.css') }}">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('backend/css/skin_color.css') }}">
  <link rel="stylesheet" href="{{asset('css/custom.css')}}">
  <!-- toster msg Css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-primary fixed">
	
<div class="wrapper">

  <!-- main header -->
    @include('admin_main.templates.header')
  
  <!-- Left sidebar column. contains the logo and sidebar -->

    @include('admin_main.templates.sideBar')



  <!-- Main content (Content Wrapper. Contains page content) -->
    @yield('adminContent')
  <!-- /.content-wrapper -->


  <!-- footer part -->
    @include('admin_main.templates.footer')


  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	 
	<!-- Vendor JS -->
	<script src="{{ asset('backend/js/vendors.min.js') }}"></script>
  <script src="{{ asset('../assets/icons/feather-icons/feather.min.js') }}"></script>	
	<script src="{{ asset('../assets/vendor_components/easypiechart/dist/jquery.easypiechart.js') }}"></script>
	<script src="{{ asset('../assets/vendor_components/apexcharts-bundle/irregular-data-series.js') }}"></script>
	<script src="{{ asset('../assets/vendor_components/apexcharts-bundle/dist/apexcharts.js') }}"></script>

  <!-- datatable JS -->
  <script src="{{ asset('../assets/vendor_components/datatable/datatables.min.js') }}"></script>
	<script src="{{ asset('backend/js/pages/data-table.js') }}"></script>

  <!-- scrool to top JS -->
  <script type="text/javascript" src="{{ asset('js/scrollbutton.js') }}"></script>

  <!-- Sunny Admin App -->
	<script src="{{ asset('backend/js/template.js') }}"></script>
	<script src="{{ asset('backend/js/pages/dashboard.js') }}"></script>

  <!-- JAVASCRIPT print data-->
  <!--<script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>-->
  <script type="text/javascript" src="{{ asset('js/jquery.printPage.js') }}"></script>

  <!--Javascript validate.min.js-->
  <script src="{{ asset('backend/js/validate.min.js') }}"></script>

  <!-- sweet alert2 CDN -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <!-- sweet alert2 JS link -->
  <script src="{{ asset('backend/js/sweetAlert.js') }}"></script>

  
  <!-- toster msg js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <!-- toster msg condition -->
  <script>
  @if(Session::has('message'))
  var type = "{{ Session::get('alert-type','info') }}"
  switch(type){
      case 'info':
      toastr.options =
      {
          "closeButton" : true,
          "progressBar" : true
      }
      toastr.info(" {{ Session::get('message') }} ");
      break;
      case 'success':
      toastr.options =
      {
          "closeButton" : true,
          "progressBar" : true
      }    
      toastr.success(" {{ Session::get('message') }} ");
      break;
      case 'warning':
      toastr.options =
      {
          "closeButton" : true,
          "progressBar" : true
      }
      toastr.warning(" {{ Session::get('message') }} ");
      break;
      case 'error':
      toastr.options =
      {
          "closeButton" : true,
          "progressBar" : true
      }
      toastr.error(" {{ Session::get('message') }} ");
      break; 
  }
  @endif 
  </script>
	

	
	
</body>
</html>
