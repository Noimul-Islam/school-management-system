@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit Group
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	<div class="container-full">

        <section class="content">

		 <!-- Basic Forms -->
		    <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Edit Student Group</h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">


                            <form id="myForm" method="POST" action="{{route('group.editing')}}">
                            @csrf    
                            <div class="row">
                                <div class="col-12">	



                                    <div class="form_input form-group">
                                        <h5>Year <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                        <input type="hidden" name="id" value="{{$groupData->id}}">

                                        <input type="text" name="group" value="{{$groupData->groups}}" class="form-control"> 
                                        </div>                                
                                    </div>
                                        

                                    <div class="form-group mt-10">
                                    <input type="submit" class="btn btn-rounded btn-info" value="Update">
                                    <a href="{{route('years.view')}}" style="float:right"class="btn btn-rounded btn-dark" >Back</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                <!-- /.row -->
                </div>
			    <!-- /.box-body -->
		    </div>
		  <!-- /.box -->

		</section>

    </div>
</div>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            group            :{required : true},
           },
           messages : {
            group            : {required : 'Please Enter Group Name' },
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection