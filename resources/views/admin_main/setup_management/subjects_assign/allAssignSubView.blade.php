@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Assigned Subjects
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Assigned Subjects Class List</h3>
                  <a href="{{route('SubAssign.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ASSIGN NEW</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>Class Name</th>
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allData as $count => $data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data['stuClassRelation']['class_name']}}</td>							
                                <td>
								<a href="{{route('AssignSub.details', Crypt::encrypt($data->classId))}}" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="{{route('AssignSub.edit', Crypt::encrypt($data->classId))}}" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>                          
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection
