@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Details Assigned Subjects
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Class {{$allData['0']['stuClassRelation']['class_name']}}</h3></br>
                  <strong>Assigned Subjects Details</strong>
                  <a href="{{route('SubAssign.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ASSIGN NEW</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

                    
					<div class="table-responsive">
					  <table  class="table table-bordered table-striped">
						<thead class="thead-light">
							<tr>
								<th width="5%">SL</th>
                                <th>Subject Name</th>
                                <th>Full Mark</th>
                                <th>Pass Mark</th>
                                <th>Subjective Mark</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allData as $count => $data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data['subject_relation']['subject_name']}}</td>	
                                <td>{{$data->fullMark}}</td>
                                <td>{{$data->passMark}}</td>
                                <td>{{$data->subjectiveMark}}</td>						
							</tr>
						@endforeach
						</tbody>
					  </table>
                      <div class="text-xs-right mt-10">
                            <a href="{{route('assignSubs.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection
