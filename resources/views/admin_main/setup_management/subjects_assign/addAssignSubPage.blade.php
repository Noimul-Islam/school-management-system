@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Assign New Subject
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Assign New Subject</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('SubAssign.adding') }}">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more">
                                                            
                            <div class="form_input form-group">
                                <h5>Class Name<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <select name="classId" class="form-control">
                                    <option value="" selected="" disabled="">Select Class</option>
                                    @foreach($classData as $class)
                                    <option value="{{$class->id}}">{{$class->class_name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>  

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form_input form-group">
                                    <h5>Subject Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="subjectId[]" id="subject_name1" onchange="getSelectValue(this.value);" class="form-control">
                                        <option value="" selected="" disabled="">Select Subject</option>
                                        @foreach($subData as $sub)
                                        <option value="{{$sub->id}}">{{$sub->subject_name}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    </div> 
                                </div> 

                                <div class="col-md-2">
            
                                    <div class="form_input form-group">
                                        <h5>Full Mark<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="fullMark[]" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-2">
            
                                    <div class="form_input form-group">
                                        <h5>Pass Mark<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="passMark[]" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-2">
            
                                    <div class="form_input form-group">
                                        <h5>Subjective Mark<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="subjectiveMark[]" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-2" style="padding-top: 25px;">
                                    <span class="btn btn-info addMoreEvent"><i class="fa fa-plus-circle"></i> </span>    		
                                </div>
                                
                            </div> <!-- end Row -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Assign">
                            <a href="{{route('assignSubs.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>





<!-- Add new Item div-->
<div style="visibility: hidden;">
  	<div class="extra_item_add" id="extra_item_add">
  		<div class="delete_extra_item" id="delete_extra_item">
  			<div class="form-row">

              <div class="col-md-4">
                    <div class="form_input form-group">
                    <h5>Subject Name<span class="text-danger">*</span></h5>
                    <div class="controls">
                    <select name="subjectId[]" id="subject_name2" onchange="getSelectValue(this.value);" class="form-control">
                        <option value="" selected="" disabled="">Select Subject</option>
                        @foreach($subData as $sub)
                        <option value="{{$sub->id}}">{{$sub->subject_name}}</option>
                        @endforeach
                    </select>
                    </div>
                    </div> 
                </div> 

                <div class="col-md-2">

                    <div class="form_input form-group">
                        <h5>Full Mark<span class="text-danger">*</span></h5>
                        <div class="controls">
                    <input type="text" name="fullMark[]" class="form-control" > 
                    </div>		 
                    </div>

                </div>

                <div class="col-md-2">

                    <div class="form_input form-group">
                        <h5>Pass Mark<span class="text-danger">*</span></h5>
                        <div class="controls">
                    <input type="text" name="passMark[]" class="form-control" > 
                    </div>		 
                    </div>

                </div>

                <div class="col-md-2">

                    <div class="form_input form-group">
                        <h5>Subjective Mark<span class="text-danger">*</span></h5>
                        <div class="controls">
                    <input type="text" name="subjectiveMark[]" class="form-control" > 
                    </div>		 
                    </div>

                </div>



                <div class="col-md-2" style="padding-top: 25px;">
                    <span class="btn btn-info addMoreEvent"><i class="fa fa-plus-circle"></i> </span>
                    <span class="btn btn-danger removeMore"><i class="fa fa-minus-circle"></i> </span>    		
                </div>
	
  			</div>  			
  		</div>  		
  	</div>  	
  </div>

<!--Javascript hide selected value function -->
<script type="text/javascript">
    function  getSelectValue(subject_name1){

        if(subject_name1 != ''){

            $("#subject_name2 option[value='"+subject_name1+"']").hide();
            $("#subject_name2 option[value !='"+subject_name1+"']").show();
        }
    }

</script>
  
<!--Javascript Add More filed function -->
 <script type="text/javascript">
 	$(document).ready(function(){
 		var counter = 0;
 		$(document).on("click",".addMoreEvent",function(){
 			var extra_item_add = $('#extra_item_add').html();
 			$(this).closest(".add_more").append(extra_item_add);
 			counter++;
 		});
 		$(document).on("click",'.removeMore',function(event){
 			$(this).closest(".delete_extra_item").remove();
 			counter -= 1
 		});

 	});
 </script>





<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            classId                     :{required : true},
            "subjectId[]"                :{required : true},
            "fullMark[]"                :{required : true, number: true,  range:[10, 100]}, 
            "passMark[]"                :{required : true, number: true,  range:[10, 40]},
            "subjectiveMark[]"          :{required : true, number: true,  range:[10, 100]},
           },
           messages : {
            classId                     : {required : 'Please Select Class Name'},
            "subjectId[]"                : {required : 'Please Select Subject Name'},
            "fullMark[]"                 : {required : 'Please Enter Full Mark Value', number: 'Enter in Number', range:'Full Mark must be between {0} to {1}'},
            "passMark[]"                 : {required : 'Please Enter Pass Mark Value', number: 'Enter in Number', range:'Pass Mark must be between {0} to {1}'},
            "subjectiveMark[]"                 : {required : 'Please Enter Subjective Mark Value', number: 'Enter in Number', range:'Subjective Mark must be between {0} to {1}'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection
