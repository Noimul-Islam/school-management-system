@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add Fee Amount
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Add New Fee Amount</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('feeAmount.adding') }}">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more">
                                                            
                            <div class="form_input form-group">
                                <h5>Fee Category <span class="text-danger">*</span></h5>
                                <div class="controls">
                                <select name="feeCatgryId" class="form-control">
                                    <option value="" selected="" disabled="">Select Category</option>
                                    @foreach($catgryData as $cat)
                                    <option value="{{$cat->id}}">{{$cat->categories}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div> 

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form_input form-group">
                                    <h5>Class Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="classId[]" id="classSelect1" onchange="getSelectValue(this.value);" class="className form-control">
                                        <option value="" selected="" disabled="">Select Class</option>
                                        @foreach($classData as $class)
                                        <option value="{{$class->id}}">{{$class->class_name}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    </div> 
                                </div> 

                                <div class="col-md-5">
            
                                    <div class="form_input form-group">
                                        <h5>Amount Rate<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="amount[]" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-2" style="padding-top: 25px;">
                                    <span class="btn btn-info addMoreEvent"><i class="fa fa-plus-circle"></i> </span>    		
                                </div>
                                
                            </div> <!-- end Row -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Add Amount">
                            <a href="{{route('feeAmount.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>





<!-- Add new Item div-->
<div style="visibility: hidden;">
  	<div class="extra_item_add" id="extra_item_add">
  		<div class="delete_extra_item" id="delete_extra_item">
  			<div class="form-row">

            

	        <div class="col-md-5">
     	     		
                <div class="form_input form-group mr-10">
                <h5>Class Name<span class="text-danger">*</span></h5>
                <div class="controls">
                <select name="classId[]" id="classSelect2" onchange="getSelectValue(this.value);" class="form-control className">
                    <option value="" selected="" disabled="">Select Class</option>
                    @foreach($classData as $class)
                    <option value="{{$class->id}}">{{$class->class_name}}</option>
                    @endforeach
                </select>
                </div>
                </div>  
            </div> 

            <div class="col-md-5">                      
                <div class="form_input form-group ml-10">
                    <h5>Amount Rate<span class="text-danger">*</span></h5>
                    <div class="controls">
                <input type="text" name="amount[]" class="form-control" > 
                </div>		 
                </div>
            </div>


            <div class="col-md-2" style="padding-top: 25px;">
                <span class="btn btn-info addMoreEvent"><i class="fa fa-plus-circle"></i> </span>
                <span class="btn btn-danger removeMore"><i class="fa fa-minus-circle"></i> </span>    		
            </div>
	
  			</div>  			
  		</div>  		
  	</div>  	
  </div>


<!--Javascript limited selected value function -->
<script type="text/javascript">
    function  getSelectValue(classSelect1){

        if(classSelect1 != ''){

            $("#classSelect2 option[value='"+classSelect1+"']").hide();
            $("#classSelect2 option[value !='"+classSelect1+"']").show();
        }
    }

</script>



  
<!--Javascript Add More filed function -->
 <script type="text/javascript">
 	$(document).ready(function(){
 		var counter = 0;
 		$(document).on("click",".addMoreEvent",function(){
 			var extra_item_add = $('#extra_item_add').html();
 			$(this).closest(".add_more").append(extra_item_add);
 			counter++;
 		});
 		$(document).on("click",'.removeMore',function(event){
 			$(this).closest(".delete_extra_item").remove();
 			counter -= 1
 		});

 	});
 </script>






<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            feeCatgryId            :{required : true},
            "classId[]"                :{required : true},
            "amount[]"                :{required : true, number: true,  range:[1000, 10000]},
           },
           messages : {
            feeCatgryId            : {required : 'Please Select Fee Category'},
            "classId[]"                : {required : 'Please Select Class Name'},
            "amount[]"                 : {required : 'Please Enter Fee Amount', number: 'Enter valid Amount', range:'Amount must be between {0} to {1}'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection
