@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Details Fee Amount
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">{{$allData['0']['feeCatgryRelation']['categories']}}</h3></br>
                  <strong>Amount Details</strong>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
                    
					<div class="table-responsive">
					  <table  class="table table-bordered table-striped">
						<thead class="thead-light">
							<tr>
								<th width="5%">SL</th>
								<th>Class Name</th>
                                <th>Fee Amount</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allData as $count => $data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data['stuClassRelation']['class_name']}}</td>	
                                <td>{{$data->amount}}</td>						
							</tr>
						@endforeach
						</tbody>
					  </table>
                      <div class="text-xs-right mt-10">
                            <a href="{{route('feeAmount.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection