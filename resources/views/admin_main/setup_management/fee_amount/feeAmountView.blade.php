@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Fee Category Amount
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Student Fee Amount By Category</h3>
                  <a href="{{route('feeAmount.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD FEE AMOUNT</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>Fee Categories</th>
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allData as $count => $data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data['feeCatgryRelation']['categories']}}</td>							
                                <td>
								<a href="{{route('feeAmount.details', Crypt::encrypt($data->feeCatgryId))}}" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="{{route('feeAmount.edit', Crypt::encrypt($data->feeCatgryId))}}" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>                          
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection