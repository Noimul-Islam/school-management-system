@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Leave Purpose 
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Employee Leave Purpose List</h3>
                  <a href="{{route('purpose.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD NEW</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>Purposes</th>
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data->perposes}}</td>								
                                <td>
                                    <a href="{{route('purpose.edit', Crypt::encrypt($data->id))}}" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="{{route('purpose.delete', Crypt::encrypt($data->id))}}" id="delete" class="btn btn-danger mb-5"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection
