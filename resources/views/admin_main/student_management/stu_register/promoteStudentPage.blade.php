@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Promote Student
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Student Promosition</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('student.promoting') }}">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                        <input type="hidden" name="sid" value="{{$editStuData->student_id}}">
                        <input type="hidden" name="id" value="{{$editStuData->id}}">

                            <!-- /row-1 -->
                            <div class="row">


                                <div class="col-md-2">

                                    <div class="form-group">
                                        <img id="imgShow" class="img-thumbnail" src="{{ (!empty($editStuData['userrelation']['profile_photo']))? url($editStuData['userrelation']['profile_photo']): url('Upload/no_img.jpg')}}" alt="Card image cap">
                                    </div>
                                    
                                </div> 

                                <div class="col-md-5">
                                    <div class="form_input form-group">
                                        <h5>Student Name</h5>
                                        <div class="controls">
                                    <input type="text" name="name" value="{{$editStuData['userrelation']['name']}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div> 
                                </div> 

                                <div class="col-md-5">

                                    <div class="form_input form-group">
                                        <h5>Father Name</h5>
                                        <div class="controls">
                                    <input type="text" name="fatherName" value="{{$editStuData['userrelation']['fatherName']}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div>

                                </div>

                                
                            </div> <!-- end Row-1 -->


                            <!-- /row-2 -->
                            <div class="row">

                                <div class="col-md-2">

            
                                </div>


                                <div class="col-md-5">

                                    <div class="form_input form-group">
                                        <h5>Mother Name</h5>
                                        <div class="controls">
                                    <input type="text" name="motherName" value="{{$editStuData['userrelation']['motherName']}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div>
            
                                </div>

                                <div class="col-md-5">

                                    <div class="form_input form-group">
                                        <h5>Phone</h5>
                                        <div class="controls">
                                    <input type="text" name="phone" value="{{$editStuData['userrelation']['phone']}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div>                          
                                </div>                          
                            </div> <!-- end Row-2 -->

                            <!-- /row-3 -->
                            <div class="row">

                            <div class="col-md-2">          

                            </div>
                            <div class="col-md-5">

                                    <div class="form_input form-group">
                                        <h5>Address</h5>
                                        <div class="controls">
                                    <input type="text" name="address" value="{{$editStuData['userrelation']['address']}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div>
            
                                </div>

                                <div class="col-md-5">

                                <div class="form_input form-group">
                                        <h5>Date of Birth</h5>
                                        <div class="controls">
                                    <input type="date" name="dob" value="{{$editStuData['userrelation']['dob']}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div>
            
                                </div>
                                
                            </div> <!-- end Row-3 -->

                            <hr>

                            <!-- /row-4 -->
                            <div class="row">

                            <div class="col-md-2"> </div>


                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                    <h5>Class Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="class_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Class</option>                                    
                                        @foreach($classes as $class)
                                        <option value="{{$class->id}}" {{($editStuData->class_id) == $class->id? 'selected': ''}}>{{$class->class_name}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                    <h5>Group<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="group_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Group</option>                                    
                                        @foreach($groups as $group)
                                        <option value="{{$group->id}}" {{($editStuData->group_id) == $group->id? 'selected': ''}}>{{$group->groups}}</option>
                                        @endforeach                                
                                    </select>
                                    </div>
                                    </div>

                                </div>

                                
                                
                            </div> <!-- end Row-4 -->

                            <!-- /row-5 -->
                            <div class="row">
                                <div class="col-md-2"> </div>

                                <div class="col-md-4">
        
                                    <div class="form_input form-group">
                                    <h5>Year<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="year_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Year</option>                                    
                                        @foreach($years as $year)
                                        <option value="{{$year->id}}" {{($editStuData->year_id) == $year->id? 'disabled': ''}}>{{$year->years}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form_input form-group">
                                        <h5>Discount (in '%')<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="discount" value="{{$editStuData['discountRelation']['discount']}}" class="form-control" > 
                                    </div>		 
                                    </div>
                                    
                                </div>                               
                            </div> <!-- end Row-5 -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Promote">
                            <a href="{{route('students.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>






<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            discount             :{required : true, number : true, range:[0, 100]},
            class_id             :{required : true},
            group_id             :{required : true},  
            year_id             :{required : true},          
           },
           messages : {
            discount                 : {required : 'Please Enter Discount', number: 'Enter in Number', range: 'discount must be between {0} to {1} parcent'},
            class_id                 : {required : 'Please Select Class'},
            group_id                 : {required : 'Please Select Group'},
            year_id                 : {required : 'Please Select Year'},
           
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>


<!--js script for load image-->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#imgUpload').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#imgShow').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>





@endsection
