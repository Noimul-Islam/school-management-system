@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - All Students 
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

            <div class="box bl-3 border-primary">
				  <div class="box-header">
					<h4 class="box-title">Student Search</h4>
				  </div>

				  <div class="box-body">

                    <form id="myForm" method="get" action="{{route('student.search')}}">
                        @csrf
                        <div class="row">

                            <div class="col-md-5">

                                <div class="form_input form-group">
                                <h5>Class Name</h5>
                                <div class="controls">
                                <select name="class_id" class="form-control">
                                    <option value="" selected="" disabled="">Select Class</option>                                    
                                    @foreach($classes as $class)
                                    <option value="{{$class->id}}" {{ ($classId == $class->id)? "selected":"" }}>{{$class->class_name}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                                </div>

                            </div> 


                            <div class="col-md-5">
        
                                <div class="form_input form-group">
                                <h5>Year</h5>
                                <div class="controls">
                                <select name="year_id" class="form-control">
                                    <option value="" selected="" disabled="">Select Year</option>                                    
                                    @foreach($years as $year)
                                    <option value="{{$year->id}}" {{ ($yearId == $year->id)? "selected":"" }}>{{$year->years}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                                </div>

                            </div>

                            <div class="col-md-2 mt-20">

                            <input type="submit" name="search" value="Search" class="btn btn-rounded btn-dark">

                            </div>

                        </div><!--end row -->
                    </form>
				  </div>
				</div>


			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">All Students list</h3>
                  <a href="{{route('student.register')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD STUDENT</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="3%">SL</th>
                <th>Image</th>
                <th>Name</th>
                <th>ID No.</th>                               
                <th>Group</th>         
                <th width="5%">Roll</th>                               
                @if(Auth::guard('user')->user()->role == 'Admin')
								<th>Code</th>
                @endif
                <th width="25%">Action</th>
							</tr>
						</thead>
						<tbody>
            @foreach($allData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								<td>
                    <img id="imgShow" class="rounded avatar-md avatar-xl" 
                    src="{{ (!empty($data['userrelation']['profile_photo']))? url($data['userrelation']['profile_photo']): url('Upload/no_img.jpg')}}" 
                    alt="Card image cap">
                </td>
                <td>{{$data['userrelation']['name']}}</td>
                <td>{{$data['userrelation']['id_no']}}</td>

                @if($data->group_id !== null)
                <td>{{$data['group_relation']['groups']}}</td>
                @endif

                @if($data->roll == null)
                <td>Roll will be genetated</td>
                @elseif($data->roll < 10)
                <td>0{{$data->roll}}</td>			
                @else
                <td>{{$data->roll}}</td>
                @endif		
                		
                <td>{{$data['userrelation']['code']}}</td>
                <td>
                    <a href="{{route('student.details', [Crypt::encrypt($data->student_id),Crypt::encrypt($data->id)])}}" target="_blank" title="Details PDF Download"class="btn btn-light mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="{{route('student.edit', [Crypt::encrypt($data->student_id),Crypt::encrypt($data->id)])}}" title="Edit Student Details" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a href="{{route('student.promote', [Crypt::encrypt($data->student_id),Crypt::encrypt($data->id)])}}" title="Promote Student"class="btn btn-dark mb-5"><i class="fa fa-child" aria-hidden="true"></i></a>
                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection