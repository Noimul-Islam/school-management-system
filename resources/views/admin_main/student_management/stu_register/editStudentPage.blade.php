@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit Student
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Edit Student</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('student.editing') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                        <input type="hidden" name="sid" value="{{$editStuData->student_id}}">
                        <input type="hidden" name="id" value="{{$editStuData->id}}">

                            <!-- /row-1 -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form_input form-group">
                                        <h5>Student Full Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="name" value="{{$editStuData['userrelation']['name']}}" class="form-control" > 
                                    </div>		 
                                    </div> 
                                </div> 

                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                    <h5>Gender<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="gender" class="form-control">
                                        <option value="" selected="" disabled="">Select Gender</option>                                    
                                        <option value="Male" {{($editStuData['userrelation']['gender']) == 'Male'? 'selected': ''}}>Male</option>
                                        <option value="Female" {{($editStuData['userrelation']['gender']) == 'Female'? 'selected': ''}}>Female</option>                               
                                    </select>
                                    </div>
                                    </div> 

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Date of Birth<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="dob" value="{{$editStuData['userrelation']['dob']}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-1 -->


                            <!-- /row-2 -->
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                        <h5>Father Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="fatherName" value="{{$editStuData['userrelation']['fatherName']}}" class="form-control" > 
                                    </div>		 
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Mother Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="motherName" value="{{$editStuData['userrelation']['motherName']}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Phone<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="phone" id="phoneNumCheck" onKeyup="validation()" value="{{$editStuData['userrelation']['phone']}}" class="form-control" >
                                    <div id="phone_error" style="color:red; font-size:12px;" class="d-none">Please enter a valid phone number</div> 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-2 -->


                            <!-- /row-3 -->
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                    <h5>Religion<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="religion" class="form-control">
                                        <option value="" selected="" disabled="">Select Religion</option>                                    
                                        <option value="Islam" {{($editStuData['userrelation']['religion']) == 'Islam'? 'selected': ''}}>Islam</option>
                                        <option value="Hindu" {{($editStuData['userrelation']['religion']) == 'Hindu'? 'selected': ''}}>Hindu</option>
                                        <option value="Cristian" {{($editStuData['userrelation']['religion']) == 'Cristian'? 'selected': ''}}>Cristian</option>
                                        <option value="Bhudda" {{($editStuData['userrelation']['religion']) == 'Bhudda'? 'selected': ''}}>Bhudda</option>                               
                                    </select>
                                    </div>
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Address<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="address" value="{{$editStuData['userrelation']['address']}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Discount</h5>
                                        <div class="controls">
                                    <input type="text" name="discount" value="{{$editStuData['discountRelation']['discount']}}" class="form-control"> 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-3 -->



                            <!-- /row-4 -->
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                    <h5>Class Name</h5>
                                    <div class="controls">
                                    <select name="class_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Class</option>                                    
                                        @foreach($classes as $class)
                                        <option value="{{$class->id}}" {{($editStuData->class_id) == $class->id? 'selected': ''}}>{{$class->class_name}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                    <h5>Group</h5>
                                    <div class="controls">
                                    <select name="group_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Group</option>                                    
                                        @foreach($groups as $group)
                                        <option value="{{$group->id}}" {{($editStuData->group_id) == $group->id? 'selected': ' '}}>{{$group->groups}}</option>
                                        @endforeach                                
                                    </select>
                                    </div>
                                    </div>

                                </div>



                                <div class="col-md-4">
        
                                    <div class="form_input form-group">
                                    <h5>Year</h5>
                                    <div class="controls">
                                    <select name="year_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Year</option>                                    
                                        @foreach($years as $year)
                                        <option value="{{$year->id}}" {{($editStuData->year_id) == $year->id? 'selected': ' '}}>{{$year->years}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-4 -->

                            <!-- /row-5 -->
                            <div class="row">
                                
                                

                                <div class="col-md-6">

                                    <div class="form_input form-group">
                                        <h5>Image</h5>
                                        <div class="controls">
                                        <input id="imgUpload" type="file" name="profile_photo" onchange="fileValidation()" class="form-control">
                                        <div id="img_error" style="color:red; font-size:12px;" class="d-none">Image extesion shuould be in (jpg,jpeg,png)</div>
                                        </div>
                                    </div>
                                    
                                </div> 

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <img id="imgShow" class="rounded avatar-md avatar-xl" src="{{ (!empty($editStuData['userrelation']['profile_photo']))? url($editStuData['userrelation']['profile_photo']): url('Upload/no_img.jpg')}}" alt="Card image cap">
                                    </div>
                                    
                                </div> 

                                
                            </div> <!-- end Row-5 -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('students.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>


<!--phone number custom validation-->
<script type="text/javascript">
    function validation(){
        var phone = document.getElementById('phoneNumCheck').value;

        var phonePattern = /^\+?(88)?0?1[3456789][0-9]{8}$/ ;

        if(phonePattern.test(phone)){
            document.getElementById('phone_error').classList.add('d-none');
        }else{
            document.getElementById('phone_error').classList.remove('d-none');
        }
    }
</script>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            name                    :{required : true},
            gender                 :{required : true},
            dob                     :{required : true},
            fatherName             :{required : true},  
            motherName             :{required : true},
            phone                  :{required : true, number : true},
            religion             :{required : true},  
            address             :{required : true},      
           },
           messages : {
            name                     : {required : 'Please Enter Student Name'},
            gender                : {required : 'Please Select Gender'},
            dob                 : {required : 'Please Select Date of Birth'},
            fatherName                 : {required : 'Please Enter Father Name'},
            motherName                 : {required : 'Please Enter Mother Name'},
            phone                 : {required : 'Please Enter Phone Number', number: 'Enter Valid Number'},
            religion                 : {required : 'Please Select Religion'},
            address                 : {required : 'Please Enter Student address'},
           
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>


<!-- image extension validation -->
<script>
    function fileValidation() {

        var fileInput = document.getElementById('imgUpload');
        var filePath = fileInput.value;
        
        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
            
        if (allowedExtensions.test(filePath)) {
            document.getElementById('img_error').classList.add('d-none');
            
        }else{
            document.getElementById('img_error').classList.remove('d-none');
            fileInput.value = '';
            return false;
        }
    }
</script>


<!--js script for load image-->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#imgUpload').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#imgShow').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>





@endsection
