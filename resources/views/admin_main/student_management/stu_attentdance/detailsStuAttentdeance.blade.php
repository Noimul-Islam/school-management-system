@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Student Attendance 
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Student Attendance Details</h3></br>

                  <h6 class="box-title mt-5" style="color:#fd7e14d4">
                <strong style="color:#6f42c1cf">Attendance Date: </strong> 
                {{date('d-m-Y',strtotime($detailData['0']['date']))}}
                </h6>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>Roll</th>                               
                                <th>Status</th>                              
							</tr>
						</thead>
						<tbody>
                        @foreach($detailData as $count => $data)

                            @php 
                                $assign_stu_data = App\Models\AssignStudent::where('class_id',$data->class_id)->where('group_id',$data->group_id)->where('year_id',$data->year_id)->where('student_id',$data->student_id)->get();
                            @endphp
							<tr>
								<td>{{$count+1}}</td>
                                <td>{{$data['userrelation']['id_no']}}</td>
                                <td>{{$data['userrelation']['name']}}</td>
                                @if($assign_stu_data['0']['roll'] < 10)
                                    <td>0{{$assign_stu_data['0']['roll']}}</td>
                                @else
                                    <td>{{$assign_stu_data['0']['roll']}}</td>
                                @endif

                                @if($data->attentdance_status == "Absent")
                                    <td style="color:#dc3545"><strong>{{$data->attentdance_status}}</strong></td>
                                @elseif($data->attentdance_status == "Leave")
                                    <td style="color:#fd7e14d1"><strong>{{$data->attentdance_status}}</strong></td>
                                @else
                                    <td style="color:#28a745"><strong>{{$data->attentdance_status}}</strong></td>
                                @endif
                                
							</tr>
						@endforeach
						</tbody>
                        
					  </table>
                      <hr>
                      <a href="{{route('stuAttendance.date',[$data->class_id,$data->group_id,$data->year_id])}}" style="float:right"class="btn btn-rounded btn-dark mt-10">Back</a>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection




