@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Student Attendance list
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Students Attendance list</h3>
                  <a href="{{route('stuAttendance.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD ATTENTDANCE</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
                                <th>Class Name</th>
                                <th>Group</th>   
                                <th>Year</th>                           
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($attentData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
                                <td>{{$data['classRelation']['class_name']}}</td>							
                                <td>{{$data['groupRelation']['groups']}}</td>     
                                <td>{{$data['yearRelation']['years']}}</td>                        
                                <td>
								    <a href="{{route('stuAttendance.date',[$data->class_id,$data->group_id,$data->year_id])}}" title="view date details" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection



