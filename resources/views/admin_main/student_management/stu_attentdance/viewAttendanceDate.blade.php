@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Students Attendance 
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Student Attend Date list</h3>
                  <!-- <a href="{{route('empAttendance.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD EMPLOYEE ATTENTDANCE</a> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
                                <th>Attendance Date</th>                              
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($attentData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
                                								
                                <td>{{date('d-m-Y',strtotime($data->date))}}</td>                             
                                <td>
								    <a href="{{route('stuAttendance.details',[$data->class_id, $data->group_id, $data->year_id, $data->date])}}" title="view details" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="{{route('stuAttendance.edit', [$data->class_id, $data->group_id, $data->year_id, $data->date])}}" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <!-- <a href="{{route('empLeave.delete', Crypt::encrypt($data->id))}}" id="delete" class="btn btn-danger mb-5"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
                      <hr>
                      <a href="{{route('stuAttendence.view')}}" style="float:right"class="btn btn-rounded btn-dark mt-10">Back</a>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection



