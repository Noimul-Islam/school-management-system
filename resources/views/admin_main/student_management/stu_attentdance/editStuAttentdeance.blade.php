@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit Employee Attendance
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Edit Student Attendance</h4></br></br>

                <h6 class="box-title mt-5" style="color:#fd7e14d4">
                <strong style="color:#6f42c1cf">Class: </strong> 
                {{$editableData['0']['classRelation']['class_name']}}
                </h6>&nbsp;&nbsp;&nbsp;&nbsp;

                <h6 class="box-title mt-5" style="color:#fd7e14d4">
                <strong style="color:#6f42c1cf">Group: </strong> 
                {{$editableData['0']['groupRelation']['groups']}}
                </h6>&nbsp;&nbsp;&nbsp;&nbsp;

                <h6 class="box-title mt-5" style="color:#fd7e14d4">
                <strong style="color:#6f42c1cf">Year: </strong> 
                {{$editableData['0']['yearRelation']['years']}}
                </h6>&nbsp;&nbsp;&nbsp;&nbsp;

                <h6 class="box-title mt-5" style="color:#fd7e14d4">
                <strong style="color:#6f42c1cf">Attendance Date: </strong> 
                {{date('d-m-Y',strtotime($editableData['0']['date']))}}
                </h6>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{route('stuAttendance.adding')}}">
                    @csrf
                        <div class="row">
                        <div class="col-12">

                            <!-- /row-1 -->
                            <div class="row">

                                <div class="col-md-3">

                                    <div class="form_input form-group" Style="display:none;">
                                        <h5>Class</h5>
                                        <div class="controls">
                                            <input type="text" name="class_id" value="{{$editableData['0']['class_id']}}" class="form-control" readonly> 
                                        </div>       
                                    </div>

                                </div>  


                                <div class="col-md-3">
                
                                    <div class="form_input form-group" Style="display:none;">
                                        <h5>Group</h5>
                                        <div class="controls">
                                            <input type="text" name="group_id" value="{{$editableData['0']['group_id']}}" class="form-control" readonly> 
                                        </div>       
                                    </div>

                                </div>

                                <div class="col-md-3">
            
                                    <div class="form_input form-group" Style="display:none;">
                                        <h5>Year</h5>
                                        <div class="controls">
                                            <input type="text" name="year_id" value="{{$editableData['0']['year_id']}}" class="form-control" readonly> 
                                        </div>       
                                    </div>

                                </div>

                                <div class="col-md-3">
                
                                    <div class="form_input form-group" Style="display:none;">
                                        <h5>Attendance Date</h5>
                                        <div class="controls">
                                    <input type="date" name="att_date" value="{{$editableData['0']['date']}}" id="blockOldDate1" class="form-control" readOnly> 
                                    </div>		 
                                    </div>

                                </div> 
                                
                            </div><!-- /end row-1 -->
                            </br>
                            
                            
                            <!-- /row-2 -->
                            <div class="row"> 

                                <div class="col-md-12">
            
                                <table class="table table-bordered table-striped" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Sl</th>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Student ID</th>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Student Name</th>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Roll</th>
                                            <th colspan="2" class="text-center" style="vertical-align: middle; width: 32%">Attendance Status</th>				
                                        </tr>

                                        <tr>
                                            <th class="text-center btn present_all" style="display: table-cell; background-color: #34465b">Present</th>
                                            <th class="text-center btn absent_all" style="display: table-cell; background-color: #34465b">Absent</th>                           
                                        </tr>   				
                                    </thead>
                                    <tbody>
                                        @foreach($editableData as $count => $data)	
                                        
                                        @php 
                                            $assign_stu_data = App\Models\AssignStudent::where('class_id',$data->class_id)->where('group_id',$data->group_id)->where('year_id',$data->year_id)->where('student_id',$data->student_id)->get();
                                        @endphp

                                        <tr id="div{{$data->id}}" class="text-center">
                                            <input type="hidden" name="student_id[]" value="{{$data->student_id}}">
                                            <td>{{ $count+1  }}</td>
                                            <td>{{ $data['userrelation']['id_no'] }}</td>
                                            <td>{{ $data['userrelation']['name'] }}</td>
                                            @if($assign_stu_data['0']['roll'] < 10)
                                                <td>0{{$assign_stu_data['0']['roll']}}</td>
                                            @else
                                                <td>{{$assign_stu_data['0']['roll']}}</td>
                                            @endif

                                            <td colspan="3">
                                                <div class="switch-toggle switch-3 switch-candy">

                                                    <input name="attentdance_status{{$count}}" type="radio" value="Present" id="present{{$count}}"
                                                    {{ ($data->attentdance_status == 'Present')?'checked':'' }} >
                                                    <label for="present{{$count}}">Present</label>

                                                    <input name="attentdance_status{{$count}}" type="radio" value="Absent" id="absent{{$count}}" 
                                                    {{ ($data->attentdance_status == 'Absent')?'checked':'' }} >
                                                    <label for="absent{{$count}}">Absent</label>
                                                    
                                                </div>			
                                            </td>
                                        </tr>			
                                    @endforeach
                                    </tbody>   			
                                </table>
                                </div>
                                
                            </div> <!-- end Row-2 -->
                            <h6 class="box-title mt-5" style="color:#fd7e14d4">
                            <strong style="color:#6f42c1cf">Attendance Date: </strong> 
                            {{date('d-m-Y',strtotime($editableData['0']['date']))}}
                            </h6>
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('stuAttendance.date',[$data->class_id,$data->group_id,$data->year_id])}}" style="float:right"class="btn btn-rounded btn-dark mt-10">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>



@endsection




