@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Student regiter fee
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		 

		<!-- Main content -->
		<section class="content">
		  <div class="row">

		
            <div class="col-12">
                <div class="box bl-3 border-primary">
                    <div class="box-header">
                        <h4 class="box-title">Student <strong>Registration Fee</strong></h4>
                    </div>

                    <div class="box-body">
                            <div class="row" >

                                <div class="col-md-3">

                                    <div class="form_input form-group">
                                    <h5>Class Name</h5>
                                    <div class="controls">
                                    <select name="class_id" id="class_id"class="form-control">
                                        <option value="" selected="" disabled="">Select Class</option>                                    
                                        @foreach($classes as $class)
                                        <option value="{{$class->id}}">{{$class->class_name}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>

                                </div> 

                                <div class="col-md-3">
    
                                    <div class="form-group">
                                    <h5>Select Group</h5>
                                    <div class="controls">
                                    <select name="group_id" id="group_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Group</option>                                    
                                                                    
                                    </select>
                                    </div>
                                    </div>

                                </div>


                                <div class="col-md-3">
            
                                    <div class="form_input form-group">
                                    <h5>Select Year</h5>
                                    <div class="controls">
                                    <select name="year_id" id="year_id"class="form-control">
                                        <option value="" selected="" disabled="">Select Year</option>                                    
                                        @foreach($years as $year)
                                        <option value="{{$year->id}}">{{$year->years}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>

                                </div>

                                <div class="col-md-3 mt-20">

                                    <a id="srchButton" name="search" class="btn btn-rounded btn-dark">Search</a>

                                </div>

                            </div><!--end search row --> 

                            </br>
                            <div class="row">
                                <div class="col-md-12">
                                    <script id="doc-template" type="text/x-handlebars-template">
                                        <table class="table table-bordered table-striped" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    @{{{thsource}}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    @{{#each this}}
                                                    <tr>
                                                        @{{{tdsource}}}
                                                    </tr>
                                                    @{{/each}}
                                                </tr>
                                            </tbody>
                                        </table>
                                    </script>

                                    <div id="document-ready">
                                        <script  type="text/x-handlebars-template">
                                        <table class="table table-bordered table-striped" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        @{{{thsource}}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        @{{#each this}}
                                                        <tr>
                                                            @{{{tdsource}}}
                                                        </tr>
                                                        @{{/each}}
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </script>
                                    </div>                              
                                </div>     
                            </div>                      
                        </div>
                    </div>		            
                </div>    
            </div>        
		  <!-- /.row -->
		</section>
		<!-- /.content -->  
    </div>
</div>



<!-- Javascript Load Group by class wise-->
<script type="text/javascript">
    $(function(){
        $(document).on('change','#class_id',function(){
            var class_id = $('#class_id').val();
            $.ajax({
            url:"{{ route('marks.getGroupData') }}", //from defualt controller
            type:"GET",
            data:{'class_id':class_id},
            success:function(data){
                var html = '<option value="" disabled="">Select Group</option>';
                $.each( data, function(key, v) {
                html += '<option value="'+v.group_id+'">'+v.group_relation.groups+'</option>';
                });
                $('#group_id').html(html);
            }
            });
        });
    });
</script>




<!-- Get Registration Fee -->
<script type="text/javascript">
  $(document).on('click','#srchButton',function(){
    var year_id = $('#year_id').val();
    var class_id = $('#class_id').val();
    var group_id = $('#group_id').val();
     $.ajax({
      url: "{{ route('getRegFee.data')}}",
      type: "get",
      data: {'year_id':year_id,'class_id':class_id,'group_id':group_id},
      beforeSend: function() {       
      },
      success: function (data) {
        var source = $("#doc-template").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#document-ready').html(html);
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
  });

</script>



@endsection
