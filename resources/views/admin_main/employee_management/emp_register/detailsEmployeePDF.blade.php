<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.i {
    font-size: 15px;
    float: right;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}

.templete{
    width: 100%;
    margin: 0 auto;
}     
.clear{
    overflow:hidden;
}
.header_section{
    height: 123px;
    background:#202340;
}

.logo{
    width: 50%;
    float: left;
}
.logo h2{
    color: #ffffff;
    font-size: 25px;
    margin-top: 20px;
    margin-left: 10px;
    text-shadow: 6px 1px 7px #ff0303;
}
.logo p {
    font-size: 13px;
    margin-top: 10px;
    margin-left: 10px;
    color: #ffd662;
}
.social{
    float: right;
    margin-top: 19px;
    width: 50%;
}
.social p {
    font-size: 10px;
    margin-left: 60%;
    color: #ffd662;
}

.heading2 {
    text-align: center; 
    text-decoration: underline;
}
</style>
</head>


<body>
<div class="header_section templete clear">
    <div class="logo">
        <h2>Future Hope Education School</h2> 
        <p> <b>Email:</b> futurehopeedu@gmail.com</p> 
    </div>
    <div class="social clear">
        <p><b>Phone 1: </b> 01737364749</p> 
        <p><b>Phone 2: </b> 01737366754</p>
        <p><b>Address: </b> Mirpur, Dhaka, Bangladesh</p> 
    </div>
</div>

<h2 class="heading2">Employee Details</h2>


<table id="customers">
  <tr>
    <th>SL</th>
    <th>Employee Details</th>
    <th>Data</th>
  </tr>

  <tr>
    <td>1</td>
    <td><b>Employee ID</b></td>
    <td>{{$details->id_no}}</td>
 </tr>

 <tr>
    <td>2</td>
    <td><b>Employee Name</b></td>
    <td>{{$details->name}}</td>
 </tr>


 <tr>
    <td>4</td>
    <td><b>Designation</b></td>
    <td>{{$details['designationRel']['dasignation']}}</td>
 </tr>
 <tr>
    <td>5</td>
    <td><b>Qualification</b></td>
    <td>{{$details['qualificationRel']['name']}}</td>
 </tr>
 <tr>
    <td>6</td>
    <td><b>Salary</b></td>
    <td>{{$details->salary}}</td>
 </tr>
 <tr>
    <td>7</td>
    <td><b>Join date</b></td>
    <td>{{ date('d-m-Y', strtotime($details->join_date))  }}</td>
 </tr>
 <tr>
    <td>8</td>
    <td><b>Gender</b></td>
    <td>{{$details->gender}}</td>
 </tr>
 <tr>
    <td>9</td>
    <td><b>Date of Birth</b></td>
    <td>{{ date('d-m-Y', strtotime($details->dob))  }}</td>
 </tr>
 <tr>
    <td>10</td>
    <td><b>Phone</b></td>
    <td>{{$details->phone}}</td>
 </tr>
 <tr>
    <td>11</td>
    <td><b>Father Name</b></td>
    <td>{{$details->fatherName}}</td>
 </tr>
 <tr>
    <td>12</td>
    <td><b>Mother Name</b></td>
    <td>{{$details->motherName}}</td>
 </tr>
 
 <tr>
    <td>13</td>
    <td><b>Address</b></td>
    <td>{{$details->address}}</td>
 </tr>
 <tr>
    <td>14</td>
    <td><b>Religion</b></td>
    <td>{{$details->religion}}</td>
 </tr>
 
  
</table>
</br>
<i class="i">Print date: {{date("d M Y")}}</i>

</body>
</html>



