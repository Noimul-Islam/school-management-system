@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - All Employees
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">All Employees</h3>
                  <a href="{{route('employee.register')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD EMPLOYEE</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th width="5%">Image</th>
                                <th>ID No.</th>
                                <th>Name</th>
								<th width="5%">Designation</th>
                                <th>Phone</th>
                                <th>Join Date</th>
                                <!-- <th>Salary</th> -->
                                @if(Auth::guard('user')->user()->role == 'Admin')
								<th>Code</th>
                                @endif
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allUserData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								<td>
									<img class="rounded avatar-md avatar-xl" 
									src="{{ (!empty($data->profile_photo))? url($data->profile_photo): url('Upload/no_img.jpg')}}" 
									alt="Card image cap">
								</td>
								<td>{{$data->id_no}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data['designationRel']['dasignation']}}</td>								
                                <td>{{$data->phone}}</td>
                                <td>{{$data->join_date}}</td>
                                <!-- <td>{{$data->salary}}/=</td> -->
                                <td>{{$data->code}}</td>
                                <td>
                                    <a href="{{route('employee.edit', Crypt::encrypt($data->id))}}" Title="Edit" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="{{route('employee.details', Crypt::encrypt($data->id))}}" target="_blank" Title="Details in Pdf"class="btn btn-light mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection

