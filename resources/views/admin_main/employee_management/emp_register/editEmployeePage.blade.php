@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit Employee
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Edit Employee</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('employee.editing') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                            <!-- /row-1 -->
                            <div class="row">

                            <input type="hidden" name="id" value="{{$editEmpData->id}}" class="form-control" > 
                                <div class="col-md-4">
                                    <div class="form_input form-group">
                                    <h5>Employee Full Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="text" name="name" value="{{$editEmpData->name}}" class="form-control" > 
                                    </div>		 
                                    </div> 
                                </div> 


                                <div class="col-md-4">
                                    <div class="form_input form-group">
                                        <h5>Email<span class="text-danger"></span></h5>
                                        <div class="controls">
                                        <input type="email" name="email" value="{{$editEmpData->email}}" class="form-control" readOnly> 
                                    </div>		 
                                    </div> 
                                </div>

                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                    <h5>Gender<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="gender" class="form-control">
                                        <option value="" selected="" disabled="">Select Gender</option>                                    
                                        <option value="Male" {{($editEmpData->gender == 'Male'? 'selected':'')}}>Male</option>
                                        <option value="Female" {{($editEmpData->gender == 'Female'? 'selected':'')}}>Female</option>                               
                                    </select>
                                    </div>
                                    </div> 

                                </div>

                                
                                
                            </div> <!-- end Row-1 -->


                            <!-- /row-2 -->
                            <div class="row">


                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Date of Birth<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="dob" value="{{$editEmpData->dob}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                        <h5>Father Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="fatherName" value="{{$editEmpData->fatherName}}" class="form-control" > 
                                    </div>		 
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Mother Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="motherName" value="{{$editEmpData->motherName}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                
                                
                            </div> <!-- end Row-2 -->


                            <!-- /row-3 -->
                            <div class="row">

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Phone<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="phone" id="phoneNumCheck" onKeyup="validation()" value="{{$editEmpData->phone}}" class="form-control" >
                                    <div id="phone_error" style="color:red; font-size:12px;" class="d-none">Please enter a valid phone number</div> 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                    <h5>Religion<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="religion" class="form-control">
                                        <option value="" selected="" disabled="">Select Religion</option>                                    
                                        <option value="Islam" {{($editEmpData->religion == 'Islam'? 'selected':'')}}>Islam</option>
                                        <option value="Hindu" {{($editEmpData->religion == 'Hindu'? 'selected':'')}}>Hindu</option>
                                        <option value="Cristian" {{($editEmpData->religion == 'Cristian'? 'selected':'')}}>Cristian</option>
                                        <option value="Bhudda" {{($editEmpData->religion == 'Bhudda'? 'selected':'')}}>Bhudda</option>                               
                                    </select>
                                    </div>
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Address<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="address" value="{{$editEmpData->address}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                                      
                            </div> <!-- end Row-3 -->



                            <!-- /row-4 -->
                            
                            <div class="row">
                                @if(!$editEmpData)
                                    <div class="col-md-4">
                                        <div class="form_input form-group">
                                            <h5>Salary<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                        <input type="text" name="salary" value="{{$editEmpData->salary}}" class="form-control" > 
                                        </div>		 
                                        </div>
                                                                        
                                    </div> 
                                @endif

                                @if(!$editEmpData)
                                    <div class="col-md-4">
                                        <div class="form_input form-group">
                                            <h5>Date of Join<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                        <input type="date" name="join_date" value="{{$editEmpData->join_date}}" class="form-control" > 
                                        </div>		 
                                        </div>           
                                        
                                    </div>
                                @endif

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                    <h5>Select Qualification<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="qualificationId" id="qualify" class="form-control">
                                        <option value="" selected="" disabled="">Select Qualification</option>                                    
                                        @foreach($qualification as $Q_data)
                                        <option value="{{$Q_data->id}}" {{($editEmpData->qualificationId == $Q_data->id? 'selected':'')}}>{{$Q_data->name}}</option>
                                        @endforeach   
                                        <option value="0">Add new</option>                             
                                    </select>
                                    </div>
                                        <input type="text" id="add_new" name="add_new" style="display:none;" class="form_input form-control mt-5" placeholder="Write qualification name">
                                    </div>

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                    <h5>Change Designation<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="designationId" class="form-control">
                                        <option value="" selected="" disabled="">Select Designation</option>                                    
                                        @foreach($dasignationData as $D_data)
                                        <option value="{{$D_data->id}}" {{($editEmpData->designationId == $D_data->id? 'selected':'')}}>{{$D_data->dasignation}}</option>
                                        @endforeach                               
                                    </select>
                                    </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                        <h5>Image</h5>
                                        <div class="controls">
                                        <input id="imgUpload" type="file" name="profile_photo" onchange="fileValidation()" class="form-control">
                                        <div id="img_error" style="color:red; font-size:12px;" class="d-none">Image extesion shuould be in (jpg,jpeg,png)</div>
                                        </div>
                                    </div>                                
  
                                </div>
                                
                            </div> <!-- end Row-4 -->

                            <!-- /row-5 -->
                            <div class="row">
                                
                                <div class="col-md-4"></div>

                                <div class="col-md-4"></div> 

                                <div class="col-md-4">

                                    <div class="form-group">
                                    <img id="imgShow" class="rounded avatar-md avatar-xl" src="{{ (!empty($editEmpData->profile_photo))? url($editEmpData->profile_photo): url('Upload/no_img.jpg')}}" alt="Card image cap">
                                    </div>  

                                </div> 
   
                            </div> <!-- end Row-5 -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('employees.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>



<!--add new qualification  js script-->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change','#qualify',function(){
			var qualify_id = $(this).val();
			if (qualify_id == '0') {
				$('#add_new').show();
			}else{
				$('#add_new').hide();
			}
		});
	});
</script>


<!--phone number custom validation-->
<script type="text/javascript">
    function validation(){
        var phone = document.getElementById('phoneNumCheck').value;

        var phonePattern = /^\+?(88)?0?1[3456789][0-9]{8}$/ ;

        if(phonePattern.test(phone)){
            document.getElementById('phone_error').classList.add('d-none');
        }else{
            document.getElementById('phone_error').classList.remove('d-none');
        }
    }
</script>




<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

        rules: {
            name                     :{required : true},
            email                   :{required : true, email : true},
            gender                     :{required : true},
            dob                     :{required : true},
            fatherName                :{required : true},
            motherName                :{required : true},
            phone                :{required : true},
            religion                :{required : true},
            address                :{required : true},
            qualificationId                :{required : true},
            designationId                :{required : true},
           },
           messages : {
            name                     : {required : 'Please Enter Employee Name'},
            email                     : {required : 'Please Enter Email Address'},
            gender                  : {required : 'Please Select Gender'},
            dob                     : {required : 'Please Enter Date of Birth'},
            fatherName                 : {required : 'Please Enter Father Name'},
            motherName                : {required : 'Please Enter Mother Name'},
            phone                   :{required : 'Please Enter Phone number'},
            religion                : {required : 'Please Select Religion'},
            address                : {required : 'Please Enter Address'},
            qualificationId                : {required : 'Please Select Qualification'},
            designationId                : {required : 'Please Select Designstion'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>


<!-- image extension validation -->
<script>
    function fileValidation() {

        var fileInput = document.getElementById('imgUpload');
        var filePath = fileInput.value;
        
        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
            
        if (allowedExtensions.test(filePath)) {
            document.getElementById('img_error').classList.add('d-none');
            
        }else{
            document.getElementById('img_error').classList.remove('d-none');
            fileInput.value = '';
            return false;
        }
    }
</script>


<!--js script for load image-->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#imgUpload').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#imgShow').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>





@endsection

