@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Employee Leaves 
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Employee Leaves list</h3>
                  <a href="{{route('empLeave.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD EMPLOYEE LEAVE</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>ID No.</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Leave Perpouse</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data['userrelation']['id_no']}}</td>
                                <td>{{$data['userrelation']['name']}}</td>
                                <td>{{$data['userrelation']['designationRel']['dasignation']}}</td>								
                                <td>{{$data['perposeRelation']['perposes']}}</td>
                                <td>{{$data->start_date}}</td>
                                <td>{{$data->end_date}}</td>
                                <td>
								<!-- <a href="{{route('empLeave.details', Crypt::encrypt($data->id))}}" title="view details in pdf" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                    <a href="{{route('empLeave.edit', Crypt::encrypt($data->id))}}" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="{{route('empLeave.delete', Crypt::encrypt($data->id))}}" id="delete" class="btn btn-danger mb-5"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection

