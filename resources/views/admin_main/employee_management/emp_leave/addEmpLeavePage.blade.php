@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add Employee leaves
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Add Employee Leaves</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{route('empLeave.adding')}}">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                            <!-- /row-1 -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form_input form-group">
                                    <h5>Employee Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="employee_id" class="form-control">
                                        <option value="" selected="" disabled="">Select</option>   
                                        @foreach($empData as $emp)                                 
                                        <option value="{{$emp->id}}">{{$emp->name}} -  ({{$emp['designationRel']['dasignation']}})</option>   
                                        @endforeach                            
                                    </select>
                                    </div>
                                    </div>
                                </div> 

                                <div class="col-md-6">

                                    <div class="form_input form-group">
                                    <h5>Leave Purpose<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <select name="leavePurpose_id" id="Purpose_id" class="form-control">
                                        <option value="" selected="" disabled="">Select </option>                                    
                                        @foreach($purposes as $purpose)                                 
                                        <option value="{{$purpose->id}}">{{$purpose->perposes}}</option>   
                                        @endforeach      
                                        <option value="0">New Purpose</option>
                                    </select>
                                    </div>
                                    
                                        <input type="text" id="add_purpose" name="add_purpose" style="display:none;" class="form_input form-control mt-5" placeholder="Write the purpose">
                                    </div> 

                                </div>

                                
                            </div> <!-- end Row-1 --> 
                            
                            
                            <!-- /row-2 -->
                            <div class="row"> 

                                <div class="col-md-12">
            
                                    <div class="form_input form-group">
                                        <h5>Description (reason)<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="leaveReason" class="form-control" > </textarea>
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-2 -->


                            <!-- /row-3 -->
                            <div class="row"> 

                                <div class="col-md-6">
            
                                    <div class="form_input form-group">
                                        <h5>Start Date<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="start_date" id="blockOldDate1" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-6">
            
                                    <div class="form_input form-group">
                                        <h5>End Date<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="end_date" id="blockOldDate2" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-3 -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Add Leave">
                            <a href="{{route('empLeave.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>



<!--add new purpose js script-->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change','#Purpose_id',function(){
			var Purpose_id = $(this).val();
			if (Purpose_id == '0') {
				$('#add_purpose').show();
			}else{
				$('#add_purpose').hide();
			}
		});
	});
</script>


<!--block previous/past date on the calander view-->
<script type="text/javascript">
    $(document).ready(function(){
        var date = new Date();
        var tDate = date.getDate();
        var month = date.getMonth() + 1;
        if(tDate <10){
            tDate = '0' + tDate;
        }
        if(month <10){
            month = '0' + month;
        }
        var year = date.getUTCFullYear();

        var minDate = year + "-" + month + "-" + tDate;
        document.getElementById("blockOldDate1").setAttribute('min', minDate)
        document.getElementById("blockOldDate2").setAttribute('min', minDate)
        console.log(minDate);
    });
</script>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            add_purpose                     :{required : true},
            employee_id                     :{required : true},
            leavePurpose_id                     :{required : true},
            leaveReason                     :{required : true},
            start_date                :{required : true},
            end_date                :{required : true},
           },
           messages : {
            employee_id                  : {required : 'Please Select Employee Name'},
            leavePurpose_id                : {required : 'Please Select Leave Purpose'},
            leavePurpose_id                : {required : 'Please Select Leave Purpose'},
            leaveReason                : {required : 'Please Enter the Reason of leaving'},
            start_date                 : {required : 'Please Input Start Date of Leaving'},
            end_date                : {required : 'Please Input End Date of Leaving'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>






@endsection


