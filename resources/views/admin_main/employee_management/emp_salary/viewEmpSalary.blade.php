@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - All Employees salary
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Employees Salary List</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>								
                                <th>ID No.</th>
                                <th>Name</th>
								<th width="5%">Designation</th>
                                <th>Phone</th>
                                <th>Join Date</th>
                                <th>Salary</th>                               
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allSalary as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								
								<td>{{$data->id_no}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data['designationRel']['dasignation']}}</td>								
                                <td>{{$data->phone}}</td>
                                <td>{{date('d-m-Y',strtotime($data->join_date))}}</td>
                                <td>{{$data->salary}}/=</td>                               
                                <td>
                                <a href="{{route('empSalary.increment', Crypt::encrypt($data->id))}}" Title="Salary Increment" class="btn btn-info mb-5"><i class="fa fa-line-chart" aria-hidden="true"></i></a>
                                <a href="{{route('empSalary.details', Crypt::encrypt($data->id))}}" title="Salary History Details" class="btn btn-light mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection

