@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Increment Salary History
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Salary History List</h3>
                    <hr>
                  <h5><strong style="color:#7a15f7;">Emp. Name: </strong> {{$userData->name}}</h5>
                  <h6><strong style="color:#7a15f7;">ID No:  </strong> {{$userData->id_no}}</h6>
                  <h6><strong style="color:#00bc8b;"> {{$userData['designationRel']['dasignation']}}</strong></h6>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table  class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>								
								<th>Previous Salary</th>
                                <th>Present Salary</th>
                                <th>Increment Amount</th>
                                <th>Increment date</th>                               
							</tr>
						</thead>
						<tbody>
                        @foreach($salaryData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								
								<td>{{$data->previous_salary}}/=</td>
                                <td>{{$data->present_salary}}/=</td>
                                <td>{{$data->increment_salary}}/=</td>								
                                <td>{{date('d-m-Y',strtotime($data->effected_salary))}}</td>                               
							</tr>
						@endforeach
						</tbody>
					  </table>
                      <a href="{{route('empSalarys.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection


