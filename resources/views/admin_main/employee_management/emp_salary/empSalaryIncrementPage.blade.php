@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Increment Salary
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Increment Salary</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('salary.incrementing') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                            <!-- /row-1 -->
                            <div class="row">
                            <input type="hidden" name="id" value="{{Crypt::encrypt($editData->id)}}" class="form-control" >
                                <div class="col-md-6">
                                    <div class="form_input form-group">
                                        <h5>Increment Amount<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="increment_salary" class="form-control" > 
                                    </div>		 
                                    </div> 
                                </div> 

                                <div class="col-md-6">
            
                                    <div class="form_input form-group">
                                        <h5>Effected Salary Date<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="effected_salary" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-1 -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Increment">
                            <a href="{{route('empSalarys.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>






<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            effected_salary                :{required : true},
            increment_salary                :{required : true, number: true,  range:[2000, 50000]},
           },
           messages : {
            effected_salary                : {required : 'Please input Date of salary Increment'},
            increment_salary                   : {required : 'Please Enter Salary Amount', number: 'Enter valid Amount', range:'Salary must be between {0} to {1}'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>


@endsection


