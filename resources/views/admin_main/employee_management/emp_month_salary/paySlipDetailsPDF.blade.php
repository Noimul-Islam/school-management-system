@php 
    $date = date('Y-m',strtotime($details['0']->date));
    if ($date !='') {
        $where[] = ['date','like',$date.'%'];
    }

    $totalAttend = App\Models\empAttentdance::with(['userrelation'])->where($where)->where('employee_id',$details['0']->employee_id)->get();
    $countAbsent = count($totalAttend->where('attentdance_status','Absent'));

    $originalSalary = (float)$details['0']['userrelation']['salary']; 
    $amountPerDay = (float)$originalSalary/30;             
    $minusAmount = (float)$amountPerDay*(float)$countAbsent; 
    $finalSalary = (float)$originalSalary-(float)$minusAmount;
@endphp

<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.i {
    font-size: 15px;
    float: right;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 7px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 8px;
  padding-bottom: 8px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}

.templete{
    width: 100%;
    margin: 0 auto;
}     
.clear{
    overflow:hidden;
}
.header_section{
    height: 123px;
    background:#202340;
}

.logo{
    width: 50%;
    float: left;
}
.logo h2{
    color: #ffffff;
    font-size: 25px;
    margin-top: 20px;
    margin-left: 10px;
    text-shadow: 6px 1px 7px #ff0303;
}
.logo p {
    font-size: 13px;
    margin-top: 10px;
    margin-left: 10px;
    color: #ffd662;
}
.social{
    float: right;
    margin-top: 19px;
    width: 50%;
}
.social p {
    font-size: 10px;
    margin-left: 60%;
    color: #ffd662;
}

.heading2 {
    text-align: center; 
    text-decoration: underline;
    padding-bottom: 5px;
}
</style>
</head>


<body>
<div class="header_section templete clear">
    <div class="logo">
        <h2>Future Hope Education School</h2> 
        <p> <b>Email:</b> futurehopeedu@gmail.com</p> 
    </div>
    <div class="social clear">
        <p><b>Phone 1: </b> 01737364749</p> 
        <p><b>Phone 2: </b> 01737366754</p>
        <p><b>Address: </b> Mirpur, Dhaka, Bangladesh</p> 
    </div>
</div>

<h3 class="heading2">Employee Monthly Salary</h3>
<table id="customers">
  <tr>
    <th>SL</th>
    <th>Employee Details</th>
    <th>Data</th>
  </tr>

  <tr>
    <td>1</td>
    <td><b>Emp. ID</b></td>
    <td>{{$details['0']['userrelation']['id_no']}}</td>
 </tr>

 <tr>
    <td>2</td>
    <td><b>Emp. Name</b></td>
    <td>{{$details['0']['userrelation']['name']}}</td>
 </tr>

 <tr>
    <td>3</td>
    <td><b>Designation</b></td>
    <td>{{$details['0']['userrelation']['designationRel']['dasignation']}}</td>
 </tr>

 <tr>
    <td>4</td>
    <td><b>Phone</b></td>
    <td>{{$details['0']['userrelation']['phone']}}</td>
 </tr>
 <tr>
    <th>SL</th>
    <th>Salary Details</th>
    <th>Data</th>
 </tr>
 <tr>
    <td>6</td>
    <td><b>Month</b></td>
    <td>{{date('M Y',strtotime($details['0']->date))}}</td>
 </tr>
 <tr>
    <td>7</td>
    <td><b>Bacis Salary</b></td>
    <td>{{$originalSalary}}/=</td>
 </tr>
 @if($countAbsent > 1)
 <tr>
    <td>8</td>
    <td><b>Total Absent for this Month</b></td>
    <td>{{$countAbsent}} Days</td>
 </tr>
 @elseif($countAbsent == 0)
 <tr>
    <td>8</td>
    <td><b>Total Absent for this Month</b></td>
    <td>{{$countAbsent}} Day</td>
 </tr>
 @else
 <tr>
    <td>8</td>
    <td><b>Total Absent for this Month</b></td>
    <td>{{$countAbsent}} Day</td>
 </tr>
 @endif
 <tr>
    <td>9</td>
    <td><b>Salary for this Month</b></td>
    <td>{{(int)$finalSalary}}/=</td>
 </tr> 
</table>
<i class="i">Print date: {{date("d M Y")}}</i>
</br>

<hr>
<h3 class="heading2">Employee Monthly Salary</h3>
<table id="customers">
  <tr>
    <th>SL</th>
    <th>Employee Details</th>
    <th>Data</th>
  </tr>

  <tr>
    <td>1</td>
    <td><b>Emp. ID</b></td>
    <td>{{$details['0']['userrelation']['id_no']}}</td>
 </tr>

 <tr>
    <td>2</td>
    <td><b>Emp. Name</b></td>
    <td>{{$details['0']['userrelation']['name']}}</td>
 </tr>

 <tr>
    <td>3</td>
    <td><b>Designation</b></td>
    <td>{{$details['0']['userrelation']['designationRel']['dasignation']}}</td>
 </tr>

 <tr>
    <td>4</td>
    <td><b>Phone</b></td>
    <td>{{$details['0']['userrelation']['phone']}}</td>
 </tr>
 <tr>
    <th>SL</th>
    <th>Salary Details</th>
    <th>Data</th>
 </tr>
 <tr>
    <td>6</td>
    <td><b>Month</b></td>
    <td>{{date('M Y',strtotime($details['0']->date))}}</td>
 </tr>
 <tr>
    <td>7</td>
    <td><b>Bacis Salary</b></td>
    <td>{{$originalSalary}}/=</td>
 </tr>
 @if($countAbsent > 1)
 <tr>
    <td>8</td>
    <td><b>Total Absent for this Month</b></td>
    <td>{{$countAbsent}} Days</td>
 </tr>
 @elseif($countAbsent == 0)
 <tr>
    <td>8</td>
    <td><b>Total Absent for this Month</b></td>
    <td>{{$countAbsent}} Day</td>
 </tr>
 @else
 <tr>
    <td>8</td>
    <td><b>Total Absent for this Month</b></td>
    <td>{{$countAbsent}} Day</td>
 </tr>
 @endif
 <tr>
    <td>9</td>
    <td><b>Salary for this Month</b></td>
    <td>{{(int)$finalSalary}}/=</td>
 </tr> 
</table>
<i class="i">Print date: {{date("d M Y")}}</i>

</body>
</html>



