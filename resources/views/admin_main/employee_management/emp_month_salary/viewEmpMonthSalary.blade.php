@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Employee monthly salary
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		 

		<!-- Main content -->
		<section class="content">
		  <div class="row">

		
            <div class="col-12">
                <div class="box bl-3 border-primary">
                    <div class="box-header">
                        <h4 class="box-title">Employee <strong>Monthly Salary</strong></h4>
                    </div>

                    <div class="box-body">
                            <div class="row" >

                                <div class="col-md-8">
            
                                    <div class="form_input form-group">
                                        <h5>Select Month<span class="text-danger">*</span></h5>
                                        <p style="font-size:10px;">(Data will be shown based on attendence month)</p>
                                        <div class="controls">
                                    <input type="month" name="date" id="date" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-2 mt-50">

                                    <a id="srchButton" name="search" class="btn btn-rounded btn-dark">Search</a>

                                </div>

                            </div><!--end search row --> 

                            </br>
                            <div class="row">
                                <div class="col-md-12">
                                    <script id="doc-template" type="text/x-handlebars-template">
                                        <table class="table table-bordered table-striped" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    @{{{thsource}}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    @{{#each this}}
                                                    <tr>
                                                        @{{{tdsource}}}
                                                    </tr>
                                                    @{{/each}}
                                                </tr>
                                            </tbody>
                                        </table>
                                    </script>

                                    <div id="document-ready">
                                        <script  type="text/x-handlebars-template">
                                        <table class="table table-bordered table-striped" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        @{{{thsource}}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        @{{#each this}}
                                                        <tr>
                                                            @{{{tdsource}}}
                                                        </tr>
                                                        @{{/each}}
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </script>
                                    </div>                              
                                </div>     
                            </div>                      
                        </div>
                    </div>		            
                </div>    
            </div>        
		  <!-- /.row -->
		</section>
		<!-- /.content -->  
    </div>
</div>



<!-- Get monthly Fee -->
<script type="text/javascript">
  $(document).on('click','#srchButton',function(){
    var date = $('#date').val();
     $.ajax({
      url: "{{ route('getMonthSalary.data')}}",
      type: "get",
      data: {'date':date},
      beforeSend: function() {       
      },
      success: function (data) {
        var source = $("#doc-template").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#document-ready').html(html);
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
  });

</script>



@endsection

