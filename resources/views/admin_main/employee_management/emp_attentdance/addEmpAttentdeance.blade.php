@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add Employee Attendance
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Add Employee AttenDance</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{route('empAttendance.adding')}}">
                    @csrf
                        <div class="row">
                        <div class="col-12">

                            <!-- /row-1 -->
                            <div class="row">

                                <div class="col-md-12">
                
                                    <div class="form_input form-group">
                                        <h5>Attendance Date<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="att_date" id="blockOldDate1" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div> 
                                
                            </div> <!-- end Row-1 --> 
                            </br>
                            
                            
                            <!-- /row-2 -->
                            <div class="row"> 

                                <div class="col-md-12">
            
                                <table class="table table-bordered table-striped" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Sl</th>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">ID No.</th>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Employee Name</th>
                                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Designation</th>
                                            <th colspan="3" class="text-center" style="vertical-align: middle; width: 32%">Attendance Status</th>				
                                        </tr>

                                        <tr>
                                            <th class="text-center btn present_all" style="display: table-cell; background-color: #34465b">Present</th>
                                            <th class="text-center btn absent_all" style="display: table-cell; background-color: #34465b">Absent</th>
                                            <th class="text-center btn leave_all" style="display: table-cell; background-color: #34465b">Leave</th>                           
                                        </tr>   				
                                    </thead>
                                    <tbody> 
                                        @foreach($empData as $count => $data)		

                                        <tr id="div{{$data->id}}" class="text-center">
                                            <input type="hidden" name="employee_id[]" value="{{$data->id}}">
                                            <td>{{ $count+1  }}</td>
                                            <td>{{ $data->id_no }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data['designationRel']['dasignation'] }}</td>

                                            <td colspan="3">
                                                <div class="switch-toggle switch-3 switch-candy">

                                                    <input name="attentdance_status{{$count}}" type="radio" value="Present" id="present{{$count}}" checked="checked">
                                                    <label for="present{{$count}}">Present</label>

                                                    <input name="attentdance_status{{$count}}" type="radio" value="Absent" id="absent{{$count}}"  >
                                                    <label for="absent{{$count}}">Absent</label>

                                                    <input name="attentdance_status{{$count}}" type="radio" value="Leave" id="leave{{$count}}"  >
                                                    <label for="leave{{$count}}">Leave</label>
                                                    
                                                </div>			
                                            </td>
                                        </tr>			
                                    @endforeach
                                    </tbody>   			
                                </table>
                                </div>
                                
                            </div> <!-- end Row-2 -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Submit">
                            <a href="{{route('empAttendance.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>



<!--block previous/past date on the calander view-->
<script type="text/javascript">
    $(document).ready(function(){
        var date = new Date();
        var tDate = date.getDate();
        var month = date.getMonth() + 1;
        if(tDate <10){
            tDate = '0' + tDate;
        }
        if(month <10){
            month = '0' + month;
        }
        var year = date.getUTCFullYear();

        var minDate = year + "-" + month + "-" + tDate;
        document.getElementById("blockOldDate1").setAttribute('min', minDate)
        console.log(minDate);
    });
</script>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {         
            att_date                     :{required : true},                    
           },
           messages : {           
            att_date                     : {required : 'Please Enter Date of Attendance'},          
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection



