<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Future Hope - Login page </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('backend/assets/images/favvicon.ico') }}">

        <!-- Bootstrap Css -->
        <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{ asset('backend/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('backend/assets/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
        <!-- toster msg Css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
    </head> 

    <body class="auth-body-bg">
        <div class="bg-overlay"></div>
        <div class="wrapper-page">
            <div class="container-fluid p-0">
                <div class="card">
                    <div class="card-body">

                    
    
                        <div class="text-center mt-4">
                            <div class="mb-3">
                                <a href="index.html" class="auth-logo">
                                    <img src="{{ asset('backend/assets/images/My_logo.png') }}" height="70" class="logo-dark mx-auto" alt="">
                                    <img src="{{ asset('backend/assets/images/My_logo.png') }}" height="70" class="logo-light mx-auto" alt="">
                                </a>
                            </div>
                        </div>
    
                        <h4 class="text-muted text-center font-size-18"><b>Account Log In</b></h4>
    
                        <div class="p-3">
                        <form id="myForm" class="form-horizontal mt-3" method="POST" action="{{ route('admin.logging') }}">
                        @csrf

                            <div class="form-group mb-3 row">
                                <div class="form_input col-12">
                                    <input class="form-control"  name="email" type="email" value="{{ old('email') }}" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group mb-3 row">
                                <div class="form_input col-12">
                                <input id="input-pass" class="form-control"  name="password"  type="password"  placeholder="Password">
                                </div>
                            </div>

                            <div class="form-group mb-3 row">
                                <div class="col-12">
                                    <select name="role" class="form-control">
                                        <option value=""  disabled="">Select Role</option>                                    
                                        <option value="Admin" selected="">Admin</option>
                                        <option value="Operator">Operator</option>                               
                                    </select>
                                </div>
                            </div>

                        
                            <div class="form-group text-center row mt-3 pt-1">
                                <div class="col-12">
                                    <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                            <!-- <div class="form-group mb-0 row mt-2">
                                <div class="col-sm-7 mt-3">
                                    <a href="{{ route('password.request') }}" class="text-muted"> Forgot your password?</a>
                                </div>
                                <div class="col-sm-5 mt-3">
                                    <a href="{{route('regist.form')}}" class="text-muted"> Create an account</a>
                                </div>
                            </div> -->
                        </form>
                            <!-- end form -->
                        </div>
                    </div>
                    <!-- end cardbody -->
                </div>
                <!-- end card -->
            </div>
            <!-- end container -->
        </div>
        <!-- end -->

        <!--Jquery library CDN link from w3 school (for js validation)-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

         <!--Javascript validation function-->
         <script type="text/javascript">

            $(document).ready(function(){

                $('#myForm').validate({

                    rules: {                      
                        email           :{required : true, email: true},
                        password        :{required : true, minlength: 6},                      
                    },
                    messages : {                       
                        email           : {required : 'Please Enter Email Address' },
                       password         : {required : 'Please Enter Password',
                                           minlength: 'Password Should be at least {0} characters long'
                                          },
                    },
                    // error ta kothai show korbe 
                    errorElement : 'span',
                    errorPlacement: function (error,element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form_input').append(error); //<div class="form_input"><input></div>
                    },
                    //highlight the error with proper design
                    highlight : function(element, errorClass, validClass){
                        $(element).addClass('is-invalid');
                    },
                    unhighlight : function(element, errorClass, validClass){
                        $(element).removeClass('is-invalid');
                    },
                });
            });
        </script>


        <!-- JAVASCRIPT -->
        <script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>
        <!--Javascript validate.min.js-->
        <script src="{{ asset('backend/assets/js/validate.min.js') }}"></script>

        <script src="{{ asset('backend/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('backend/assets/libs/metismenu/metisMenu.min.js') }}"></script>
        <script src="{{ asset('backend/assets/libs/simplebar/simplebar.min.js') }}"></script>
        <script src="{{ asset('backend/assets/libs/node-waves/waves.min.js') }}"></script>

        <script src="{{ asset('backend/assets/js/app.js') }}"></script>

        <!-- toster msg js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <!-- toster msg condition -->
        <script>
        @if(Session::has('message'))
        var type = "{{ Session::get('alert-type','info') }}"
        switch(type){
            case 'info':
            toastr.options =
            {
                "closeButton" : true,
                "progressBar" : true
            }
            toastr.info(" {{ Session::get('message') }} ");
            break;
            case 'success':
            toastr.options =
            {
                "closeButton" : true,
                "progressBar" : true
            }    
            toastr.success(" {{ Session::get('message') }} ");
            break;
            case 'warning':
            toastr.options =
            {
                "closeButton" : true,
                "progressBar" : true
            }
            toastr.warning(" {{ Session::get('message') }} ");
            break;
            case 'error':
            toastr.options =
            {
                "closeButton" : true,
                "progressBar" : true
            }
            toastr.error(" {{ Session::get('message') }} ");
            break; 
        }
        @endif 
        </script>
  

    </body>
</html>
