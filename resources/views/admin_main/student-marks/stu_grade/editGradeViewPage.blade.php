@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit Grade
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Edit Grade</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('MarksGrade.editing') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                            <!-- /row-1 -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form_input form-group">
                                        <h5>Grade Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="grade_name" value="{{$GradeData->grade_name}}" class="form-control" > 
                                    <input type="hidden" name="id" value="{{$GradeData->id}}" class="form-control" >
                                    </div>		 
                                    </div> 
                                </div> 

                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                        <h5>Grade Point<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="grade_point" value="{{$GradeData->grade_point}}" class="form-control" > 
                                    </div>		 
                                    </div> 

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Start Mark<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="start_mark" value="{{$GradeData->start_mark}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-1 -->


                            <!-- /row-2 -->
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form_input form-group">
                                        <h5>End Mark<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="end_mark" value="{{$GradeData->end_mark}}" class="form-control" > 
                                    </div>		 
                                    </div>
                                    
                                </div> 

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Start Point<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="start_point" value="{{$GradeData->start_point}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>End point<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="end_point" value="{{$GradeData->end_point}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-2 -->


                            <!-- /row-3 -->
                            <div class="row">
                    
                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Remarks<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="remarks" value="{{$GradeData->remarks}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>
                                
                            </div> <!-- end Row-3 -->
                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('marksGradeEntry.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>






<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            grade_name                     :{required : true},
            grade_point                     :{required : true , number: true, max : 5, min: 0},
            start_mark                     :{required : true , number: true, max : 100, min: 0},
            end_mark                :{required : true , number: true, max : 100, min: 30},
            start_point                :{required : true , number: true, max : 5, min: 0},
            end_point                :{required : true, number: true, max : 5, min: 0},
            remarks                    :{required : true},
           },
           messages : {
            grade_name                     : {required : 'Please Enter Grade Name'},
            grade_point                  : {required : 'Please Enter Grade Point', number: 'Enter in number', max: 'Maximum Acceptance Value: {0}', min: 'Minimum Acceptance value: {0}'},
            start_mark                     : {required : 'Please Enter Start Mark', number: 'Enter in number', max: 'Maximum Acceptance value: {0}', min: 'Minimum Acceptance value: {0}'},
            end_mark                 : {required : 'Please Enter End Mark', number: 'Enter in number', max: 'Maximum Acceptance value: {0}', min: 'Minimum Acceptance value: {0}'},
            start_point                : {required : 'Please Enter Start Point', number: 'Enter in number', max: 'Maximum Acceptance value: {0}', min: 'Minimum Acceptance value: {0}'},
            end_point                   :{required : 'Please Enter End Point', number: 'Enter in number', max: 'Maximum Acceptance value: {0}', min: 'Minimum Acceptance value: {0}'},
            remarks                 : {required : 'Please Enter Remark on this Grade'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection



