@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope -Edit Student Marks  
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

            <div class="box bl-3 border-primary">
				  <div class="box-header">
					<h4 class="box-title">Search Student (Marks edit)</h4>
				  </div>

				  <div class="box-body">

            <form  method="get" action="">
                @csrf
                <div class="row">

                    <div class="col-md-4">

                        <div class="form_input form-group">
                        <h5>Class Name</h5>
                        <div class="controls">
                        <select name="class_id" id="class_id"class="form-control">
                            <option value="" selected="" disabled="">Select Class</option>                                    
                            @foreach($classes as $class)
                            <option value="{{$class->id}}">{{$class->class_name}}</option>
                            @endforeach                               
                        </select>
                        </div>
                        </div>

                    </div>  

                    <div class="col-md-4">
                        <div class="form_input form-group">
                            <h5>Subject Name</h5>
                            <div class="controls">
                            <select name="assign_sub_id" id="assign_sub_id" class="form-control">
                                <option selected="">Select Subject</option>
                                                        
                            </select>
                            </div>
                        </div> 
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form_input form-group">
                        <h5>Exam Type</h5>
                        <div class="controls">
                        <select name="examType_id" id="examType_id"class="form-control">
                            <option value="" selected="" disabled="">Select Type</option>                                    
                            @foreach($examType as $exam)
                            <option value="{{$exam->id}}">{{$exam->examTypes}}</option>
                            @endforeach                               
                        </select>
                        </div>
                        </div>
                    </div>
                </div><!--end row -->


                <div class="row">
                    <div class="col-md-5">
    
                            <div class="form_input form-group">
                            <h5>Select Group</h5>
                            <div class="controls">
                            <select name="group_id" id="group_id" class="form-control">
                              <option value="" selected="" disabled="">Select Group</option>                                    
                                                                
                            </select>
                            </div>
                            </div>

                        </div>

                        <div class="col-md-5">
    
                            <div class="form_input form-group">
                            <h5>Select Year</h5>
                            <div class="controls">
                            <select name="year_id" id="year_id" class="form-control">
                                <option value="" selected="" disabled="">Select Year</option>                                    
                                @foreach($years as $year)
                                <option value="{{$year->id}}">{{$year->years}}</option>
                                @endforeach                               
                            </select>
                            </div>
                            </div>

                        </div>

                        
                        <div class="col-md-2 mt-20">
                            <a id="srchButton" name="search" class="btn btn-rounded btn-dark">Search</a>
                        </div>

                </div><!--end row 2 -->
            </form>
				  </div>
				</div>

            <form id="myForm" method="post" action="{{route('marks.editing')}}">
            @csrf    
              <div class="box d-none" id="marksEntry">
                <div class="box-header with-border">
                  <h3 class="box-title">All Students list</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table  class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="3%">SL</th>
                        <th width="3%">ID No</th>
                        <th width="7%">Student Name</th>
                        <!-- <th width="7%">Roll</th> -->
                        <th width="7%">Father Name</th>                                
                        <th width="7%">Mother Name</th>
                        <th width="5%">Gender</th>                               
                        <th width="10%">Marks</th>
                      </tr>
                    </thead>
                    <tbody id="markEntryTable">

                    </tbody>
                    </table>
                    </br>
                    <input type="submit" value="Update" class="btn btn-rounded btn-info d-none">
                  </div>
                </div>
                <!-- /.box-body -->
                </div>         
              </div>
            </form>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- <input type="submit" value="Roll Generate" class="btn btn-rounded btn-info d-none"> -->
  
  


<!-- Javascript Load subject by class wise-->
<script type="text/javascript">
    $(function(){
        $(document).on('change','#class_id',function(){
            var class_id = $('#class_id').val();
            $.ajax({
            url:"{{ route('marks.getSubjectData') }}",
            type:"GET",
            data:{'class_id':class_id},
            success:function(data){
                var html = '<option value="">Select Subject</option>';
                $.each( data, function(key, v) {
                html += '<option value="'+v.id+'">'+v.subject_relation.subject_name+'</option>';
                });
                $('#assign_sub_id').html(html);
            }
            });
        });
    });
</script>



<!-- Javascript Load Group by class wise-->
<script type="text/javascript">
    $(function(){
        $(document).on('change','#class_id',function(){
            var class_id = $('#class_id').val();
            $.ajax({
            url:"{{ route('marks.getGroupData') }}", //from defualt controller
            type:"GET",
            data:{'class_id':class_id},
            success:function(data){
                var html = '<option value="" disabled="">Select Group</option>';
                $.each( data, function(key, v) {
                html += '<option value="'+v.group_id+'">'+v.group_relation.groups+'</option>';
                });
                $('#group_id').html(html);
            }
            });
        });
    });
</script>


  <!--JS for Search student by class, group and year -->
  <script type="text/javascript">
  $(document).on('click','#srchButton',function(){
    var class_id = $('#class_id').val();
    var assign_sub_id = $('#assign_sub_id').val();
    var examType_id = $('#examType_id').val();
    var group_id = $('#group_id').val();
    var year_id = $('#year_id').val();
     $.ajax({
      url: "{{ route('editMarks.getStudentsData')}}",
      type: "GET",
      data: {'year_id':year_id,'class_id':class_id,'group_id':group_id,'examType_id':examType_id,'assign_sub_id':assign_sub_id},
      success: function (data) {
        $('#marksEntry').removeClass('d-none');
        
        var html = '';
        $.each( data, function(key, v){
          html +=
          '<tr>'+
          '<td>'+key+'</td>'+
          '<td>'+v.userrelation.id_no+'<input type="hidden" name="student_id[]" value="'+v.student_id+'"></td>'+
          '<td>'+v.userrelation.name+'<input type="hidden" name="id_no[]" value="'+v.userrelation.id_no+'"><input type="hidden" name="examType_id" value="'+v.examType_id+'"><input type="hidden" name="assign_sub_id" value="'+v.assign_sub_id+'"></td>'+
          '<td>'+v.userrelation.fatherName+'<input type="hidden" name="group_id" value="'+v.group_id+'"></td>'+
          '<td>'+v.userrelation.motherName+'<input type="hidden" name="year_id" value="'+v.year_id+'"></td>'+
          '<td>'+v.userrelation.gender+'<input type="hidden" name="class_id" value="'+v.class_id+'"></td>'+
          '<td><input type="text" class="form_input form-control form-control-sm mb-5" name="marks[]" value="'+v.marks+'"></td>'+
          '</tr>';

          if(data == null ){
            $('.btn-info').addClass('d-none');
          }else{
            $('.btn-info').removeClass('d-none');
          }
        });
        html = $('#markEntryTable').html(html);
        key++;
      }//end success
    });
  });

</script>


<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            "marks[]"                :{required : true, number: true, min: 0, max: 100},
           },
           messages : {
            "marks[]"                : {required : 'Please Enter Amount', number: 'Please Type in Number', min: 'minimum value id {0}', max: 'maximum value id {0}'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>



@endsection


