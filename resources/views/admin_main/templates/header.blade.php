<header class="main-header">
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top pl-30">
      <!-- Sidebar toggle button-->
	  <div>
		  <ul class="nav">
			<li class="btn-group nav-item">
				<a href="#" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="push-menu" role="button">
					<i class="nav-link-icon mdi mdi-menu"></i>
			    </a>
			</li>
			<li class="btn-group nav-item">
				<a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="Full Screen">
					<i class="nav-link-icon mdi mdi-crop-free"></i>
			    </a>
			</li>			
			
			
		  </ul>
	  </div>
		
      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav">
		  <!-- full Screen -->
	      <li class="search-bar">		  
			  <div class="lookup lookup-circle lookup-right">
			     <input type="text" name="s">
			  </div>
		  </li>			
		  @php
		  	$adminData = App\Models\allUsers::where('id',Auth::guard('user')->user()->id)->first();
		  @endphp
		  <!-- name -->
		  <li class="pt-20 pl-15 pr-10">		  
			  <div style="">{{$adminData->name}} </div>
		  </li>	
		  
		 
	      <!-- User Account-->
          <li class="dropdown user user-menu">	
			
			<a href="{{route('view.profile')}}" style="float:right" class="waves-effect waves-light rounded dropdown-toggle p-0" data-toggle="dropdown" title="User">
			<img src="{{ (!empty($adminData->profile_photo))? url($adminData->profile_photo): url('Upload/no_img.jpg')}}" alt="">
			</a>

			<ul class="dropdown-menu animated flipInX">
			  <li class="user-body">
				 <a class="dropdown-item" href="{{route('view.profile')}}"><i class="ti-user text-muted mr-2"></i> Profile</a>
				 <a class="dropdown-item" href="{{route('view.change.pass')}}"><i class="ti-wallet text-muted mr-2"></i> Change Password</a>
				 <div class="dropdown-divider"></div>
				 <a class="dropdown-item" href="{{route('admin.logOut')}}"><i class="ti-lock text-muted mr-2"></i> Logout</a>
			  </li>
			</ul>
          </li>	
		 
			
        </ul>
      </div>
    </nav>
  </header>