@php
$route = Route::current()->getName();
$prefix = Request::route()->getPrefix();
@endphp



<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">	
		
        <div class="user-profile">
			<div class="ulogo">
				 <a href="{{route('admin.dashbord')}}">
				  <!-- logo for regular state and mobile devices -->
					 <div class="d-flex align-items-center justify-content-center">					 	
						  <img src="{{ asset('backend/images/my_logo.jpg') }}" alt="">
						  
					 </div>
				</a>
			</div>
        </div>
      
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">  
		  
          <li class="{{ ($route == 'admin.dashbord')? 'active' : '' }}">
                <a href="{{route('admin.dashbord')}}">
                  <i data-feather="pie-chart"></i>
            <span>Dashboard</span>
                </a>
        </li>  
          @if(Auth::guard('user')->user()->role == 'Admin')
          <li class="header nav-small-cap">Users and Profile Management</li>
            <li class="treeview {{ ($route == 'Users.view')? 'active' : '' }}">
              <a href="#">
              <i class="fa fa-group" aria-hidden="true"></i>
                <span>Manage Users</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('Users.view')}}"><i class="ti-more"></i>All Users</a></li>
                <li class="{{ ($route == 'User.add')? 'active' : '' }}" >
                  <a href="{{route('User.add')}}"><i class="ti-more"></i>Add User</a>
                </li>
              </ul>
            </li> 
            @endif

            <li class="treeview {{ ($route == 'view.profile')? 'active' : '' }} ">
              <a href="#">
              <i class="fa fa-address-card" aria-hidden="true"></i>
               <span>Manage profile</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('view.profile')}}"><i class="ti-more"></i>Your Profile</a></li>
                <li class="{{ ($route == 'view.change.pass')? 'active' : '' }}">
                  <a href="{{route('view.change.pass')}}"><i class="ti-more"></i>Change Password</a>
                </li>
              </ul>
            </li>


        <li class="header nav-small-cap">Others Management</li>
        <li class="treeview {{ ($prefix == '/setup')?'active':'' }} ">
          <a href="#">
          <i class="fa fa-gears" aria-hidden="true"></i> <span>Setup Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ ($route == 'classes.view')? 'active' : '' }}"><a href="{{route('classes.view')}}"><i class="ti-more"></i>Student Class Setup</a></li>
            
            <li class="{{ ($route == 'years.view')? 'active' : '' }}">
              <a href="{{route('years.view')}}"><i class="ti-more"></i>Student Year Setup</a>
            </li>
            <li class="{{ ($route == 'groups.view')? 'active' : '' }}">
              <a href="{{route('groups.view')}}"><i class="ti-more"></i>Student Group Setup</a>
            </li>
            <li class="{{ ($route == 'feeCategory.view')? 'active' : '' }}">
              <a href="{{route('feeCategory.view')}}"><i class="ti-more"></i>Student Fee Category</a>
            </li>
            <li class="{{ ($route == 'feeAmount.view')? 'active' : '' }}">
              <a href="{{route('feeAmount.view')}}"><i class="ti-more"></i>Student Fee Amount</a>
            </li>
            <li class="{{ ($route == 'examType.view')? 'active' : '' }}">
              <a href="{{route('examType.view')}}"><i class="ti-more"></i>Exam Types Setup</a>
            </li>
            <li class="{{ ($route == 'subjects.view')? 'active' : '' }}">
              <a href="{{route('subjects.view')}}"><i class="ti-more"></i>Student Subjects Setup</a>
            </li>
            <li class="{{ ($route == 'assignSubs.view')? 'active' : '' }}">
              <a href="{{route('assignSubs.view')}}"><i class="ti-more"></i>Subjects Assign Setup</a>
            </li>
            <li class="{{ ($route == 'qualification.view')? 'active' : '' }}">
              <a href="{{route('qualification.view')}}"><i class="ti-more"></i>Qualification Setup</a>
            </li>
            <li class="{{ ($route == 'designations.view')? 'active' : '' }}">
              <a href="{{route('designations.view')}}"><i class="ti-more"></i>Dasignation Setup</a>
            </li>
            <li class="{{ ($route == 'purposes.view')? 'active' : '' }}">
              <a href="{{route('purposes.view')}}"><i class="ti-more"></i>Employee Leave purpose</a>
            </li>
          </ul>
        </li>

        
        <li class="treeview {{ ($prefix == '/students')?'active':'' }}">
          <a href="#">
          <i class="fa fa-mortar-board" aria-hidden="true"></i>
            <span>Student Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ ($route == 'students.view')? 'active' : '' }}">
              <a href="{{route('students.view')}}"><i class="ti-more"></i>All Students</a></li>
            <li class="{{ ($route == 'rollGenerate.view')? 'active' : '' }}">
              <a href="{{route('rollGenerate.view')}}"><i class="ti-more"></i>Student Roll Generate</a></li>
            <li class="{{ ($route == 'stuIdCard.view')? 'active' : '' }}">
              <a href="{{route('stuIdCard.view')}}"><i class="ti-more"></i>Student ID Card Generate</a></li>  
            <li class="{{ ($route == 'stuAttendence.view')? 'active' : '' }}">
              <a href="{{route('stuAttendence.view')}}"><i class="ti-more"></i>Student Attendence</a></li>  
            <li class="{{ ($route == 'monthlyFee.view')? 'active' : '' }}">
              <a href="{{route('monthlyFee.view')}}"><i class="ti-more"></i>Check Monthly Fees</a></li>
            <li class="{{ ($route == 'examFee.view')? 'active' : '' }}">
              <a href="{{route('examFee.view')}}"><i class="ti-more"></i>Check Exam Fees</a></li>
            <li class="{{ ($route == 'registerFee.view')? 'active' : '' }}">
              <a href="{{route('registerFee.view')}}"><i class="ti-more"></i>Check Registration Fees</a></li>
          </ul>
        </li>

        
        <li class="treeview {{ ($prefix == '/employee')?'active':'' }}">
          <a href="#">
          <i class="mdi mdi-account-multiple"></i>
            <span>Employee Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ ($route == 'employees.view')? 'active' : '' }}">
              <a href="{{route('employees.view')}}"><i class="ti-more"></i>All Employees</a></li>
            <li class="{{ ($route == 'empAttendance.view')? 'active' : '' }}">
              <a href="{{route('empAttendance.view')}}"><i class="ti-more"></i>Employee Attendance</a></li>      
            <li class="{{ ($route == 'empLeave.view')? 'active' : '' }}">
              <a href="{{route('empLeave.view')}}"><i class="ti-more"></i>Employee Leaves</a></li>
            <li class="{{ ($route == 'monthSalary.view')? 'active' : '' }}">
              <a href="{{route('monthSalary.view')}}"><i class="ti-more"></i>Employee Monthly Salary</a></li>
            <li class="{{ ($route == 'empSalarys.view')? 'active' : '' }}">
              <a href="{{route('empSalarys.view')}}"><i class="ti-more"></i>Employee Salary Increment</a></li>
            
          </ul>
        </li>


        
        <li class="treeview {{ ($prefix == '/marks')?'active':'' }}">
          <a href="#">
          <i class="mdi mdi-lead-pencil"></i>
            <span>Marks Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ ($route == 'marksGradeEntry.view')? 'active' : '' }}">
              <a href="{{route('marksGradeEntry.view')}}"><i class="ti-more"></i>Marks Grade Setup </a></li>
            <li class="{{ ($route == 'marksEntry.view')? 'active' : '' }}">
              <a href="{{route('marksEntry.view')}}"><i class="ti-more"></i>Marks Entry</a></li>
            <li class="{{ ($route == 'marksEdit.view')? 'active' : '' }}">
              <a href="{{route('marksEdit.view')}}"><i class="ti-more"></i>Marks Edit</a></li>
          </ul>
        </li>


        
        <li class="treeview {{ ($prefix == '/account')?'active':'' }}">
          <a href="#">
          <i class="fa fa-fw fa-money"></i>
            <span>Account Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ ($route == 'studentFee.view')? 'active' : '' }}">
              <a href="{{route('studentFee.view')}}"><i class="ti-more"></i>Student fee</a></li>
            <li class="{{ ($route == 'empSalary.view')? 'active' : '' }}">
              <a href="{{route('empSalary.view')}}"><i class="ti-more"></i>Employee Salary</a></li>
            <li class="{{ ($route == 'otherCost.view')? 'active' : '' }}">
              <a href="{{route('otherCost.view')}}"><i class="ti-more"></i>Other Cost</a></li>
            
          </ul>
        </li>


        
        <li class="treeview {{ ($prefix == '/report')?'active':'' }}">
          <a href="#">
          <i class="fa fa-fw fa-file-text"></i>
            <span>Report Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ ($route == 'monthlyProfit.view')? 'active' : '' }}">
              <a href="{{route('monthlyProfit.view')}}"><i class="ti-more"></i>Monthly-Yearly Profite</a></li>
            <li class="{{ ($route == 'stuResultReport.view')? 'active' : '' }}">
              <a href="{{route('stuResultReport.view')}}"><i class="ti-more"></i>Student Result Report</a></li>  
            
            <li class="{{ ($route == 'markSheetReport.view')? 'active' : '' }}">
              <a href="{{route('markSheetReport.view')}}"><i class="ti-more"></i>MarkSheet Report</a></li>
              
            <li class="{{ ($route == 'stuIdCard.view')? 'active' : '' }}">
              <a href="{{route('stuIdCard.view')}}"><i class="ti-more"></i>Student ID Card</a></li>
            <li class="{{ ($route == 'empAttendReport.view')? 'active' : '' }}">
              <a href="{{route('empAttendReport.view')}}"><i class="ti-more"></i>Employee Attendence Report</a></li>  
          </ul>
        </li>
        
      </ul>
    </section>
  </aside>