@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Dashboard 
@endsection

@php 
	$studentCount = App\Models\AssignStudent::select('student_id')->get()->count();
	$teacherCount = App\Models\allUsers::select('id')->where('userType','Employee')->where([['designationId','!=','4'], ['designationId','!=','5']])->get()->count();
	$employeeCount = App\Models\allUsers::select('id')->where('userType','!=','Student')->get()->count();
@endphp


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-3 col-6">
					<div class="box overflow-hidden pull-up">
						<div class="box-body">							
							<div class="icon bg-primary-light rounded w-60 h-60">
								<i class="text-primary mr-0 font-size-24 mdi mdi-account-multiple"></i>
							</div>
							<div>
								<p class="text-mute mt-20 mb-0 font-size-16">Total Students</p>
								@if($studentCount > 0)
								<h3 class="text-white mb-0 font-weight-500">{{$studentCount}}</h3>
								@else
								<h3 class="text-white mb-0 font-weight-500">0</h3>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-6">
					<div class="box overflow-hidden pull-up">
						<div class="box-body">							
							<div class="icon bg-success-light rounded w-60 h-60">
								<i class="text-primary mr-0 font-size-24 mdi mdi-account-check"></i>
							</div>
							<div>
							@php 
								$classId = App\Models\stuClasses::orderBy('id','asc')->first()->id;
								$yearId = App\Models\stuYears::orderBy('id','desc')->first()->id; //get latest year id
								$newAdmitCount = App\Models\AssignStudent::where('class_id',$classId)->where('year_id',$yearId)->get()->count();
							@endphp
								<p class="text-mute mt-20 mb-0 font-size-16">New Admitted</p>
								@if($newAdmitCount > 0)
								<h3 class="text-white mb-0 font-weight-500">{{$newAdmitCount}}</h3>
								@else
								<h3 class="text-white mb-0 font-weight-500">0</h3>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-6">
					<div class="box overflow-hidden pull-up">
						<div class="box-body">							
							<div class="icon bg-warning-light rounded w-60 h-60">
								<i class="text-primary mr-0 font-size-24 mdi mdi-account-multiple"></i>
							</div>
							<div>
								<p class="text-mute mt-20 mb-0 font-size-16">Total Teachers</p>
								@if($teacherCount > 0)
								<h3 class="text-white mb-0 font-weight-500">{{$teacherCount}}</h3>
								@else
								<h3 class="text-white mb-0 font-weight-500">0</h3>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-6">
					<div class="box overflow-hidden pull-up">
						<div class="box-body">							
							<div class="icon bg-danger-light rounded w-60 h-60">
								<i class="text-primary mr-0 font-size-24 mdi mdi-account-multiple"></i>
							</div>
							<div>
								<p class="text-mute mt-20 mb-0 font-size-16">Total Employeee</p>
								@if($employeeCount > 0)
								<h3 class="text-white mb-0 font-weight-500">{{$employeeCount}}</h3>
								@else
								<h3 class="text-white mb-0 font-weight-500">0</h3>
								@endif
							</div>
						</div>
					</div>
				</div>

	@php 
	$yearName = App\Models\stuYears::orderBy('id','desc')->first()->years;
	$classCount = App\Models\stuClasses::get()->count();
	$yearId = App\Models\stuYears::orderBy('id','desc')->first()->id;
	$classes = App\Models\AssignStudent::select('class_id','year_id','group_id')->where('year_id', $yearId)->groupBy('class_id')->groupBy('group_id')->groupBy('year_id')->get();
	$countAssingClass = $classes->count();
		$StuOne = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','1')->where('group_id','4')->where('year_id','8')->get();
		$countOne = $StuOne->count();

		$StuTwo = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','2')->where('group_id','4')->where('year_id','8')->get();
		$countTwo = $StuTwo->count();

		$StuThree = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','3')->where('group_id','4')->where('year_id','8')->get();
		$countThree = $StuThree->count();

		$StuFour = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','4')->where('group_id','4')->where('year_id','8')->get();
		$countFour = $StuFour->count();

		$StuFive = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','5')->where('group_id','4')->where('year_id','8')->get();
		$countFive = $StuFive->count();

		$StuSix = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','6')->where('group_id','4')->where('year_id','8')->get();
		$countSix = $StuSix->count();

		$StuSeven = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','7')->where('group_id','4')->where('year_id','8')->get();
		$countSeven = $StuSeven->count();

		$StuEight = App\Models\AssignStudent::select('student_id','class_id')->where('class_id','8')->where('group_id','4')->where('year_id','8')->get();
		$countEight = $StuEight->count();

	@endphp
				<div class="col-12">
					<div class="box">
						<div class="box-header">
							<h4 class="box-title align-items-start flex-column">
								@if($yearName == null)
								There are no Classes and Years
								@else
								Total Students in Every Classes of {{$yearName}}
								@endif

								@if($classCount > 0)
								<small class="subtitle">Total Classes: {{$classCount}}</small>
								@else
								<small class="subtitle">Total Classes: 0</small>
								@endif
							</h4>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table no-border">
									<thead>
										<tr class="text-uppercase bg-lightest">
											<th style="min-width: 25%"><span class="text-white">Class name</span></th>
											<th style="min-width: 25%"><span class="text-fade">Group</span></th>
											<th style="min-width: 25%"><span class="text-fade">Year</span></th>
											<th style="min-width: 25%"><span class="text-fade">Total Students</span></th>
										</tr>
									</thead>
									<tbody>
										
									@if($countAssingClass < 0)		

									<tr>							
										<td class="pl-30 py-8">
											<h4>Empty table</h4>
										</td>
									</tr>
									@else
										@foreach($classes as $data)		
										<tr>							
											<td class="pl-15 py-8">
												<div class="d-flex align-items-center">
													<div>
														<span class="text-fade d-block"><b>{{$data['classRelation']['class_name']}}</b></span>
													</div>
												</div>
											</td>

											<td class="pl-15 py-8">
												<div class="d-flex align-items-center">
													<div>
														<span class="text-fade d-block"><b>{{$data['group_relation']['groups']}}</b></span>
													</div>
												</div>
											</td>

											<td class="pl-15 py-8">
												<div class="d-flex align-items-center">
													<div>
														<span class="text-fade d-block"><b>{{$data['yearRelation']['years']}}</b></span>
													</div>
												</div>
											</td>
												@if($countOne > 0 )
													@php 
														$idOne = $StuOne['0']['class_id'];
													@endphp
													@if($data->class_id == $idOne)
														<td class="pl-15 py-8">
															<div class="d-flex align-items-center">
																<div>
																	<span class="text-fade d-block pl-50"><b>{{$countOne}}</b></span>
																</div>
															</div>
														</td>
													@endif
												@else 

												@endif

												@if($countTwo > 0 )
													@php 
														$idTwo = $StuTwo['0']['class_id'];
													@endphp
													@if($data->class_id == $idTwo)
														<td class="pl-15 py-8">
															<div class="d-flex align-items-center">
																<div>
																	<span class="text-fade d-block pl-50"><b>{{$countTwo}}</b></span>
																</div>
															</div>
														</td>
													@endif
												@else 

												@endif

												@if($countThree > 0 )
													@php 
														$idThree = $StuThree['0']['class_id'];
													@endphp
												
													@if($data->class_id == $idThree)
														<td class="pl-15 py-8">
															<div class="d-flex align-items-center">
																<div>
																	<span class="text-fade d-block pl-50"><b>{{$countThree}}</b></span>
																</div>
															</div>
														</td>
													@endif
												@else 

												@endif

												@if($countFour > 0 )
													@php 
														$idfour = $StuFour['0']['class_id'];
													@endphp
												
													@if($data->class_id == $idfour)
														<td class="pl-15 py-8">
															<div class="d-flex align-items-center">
																<div>
																	<span class="text-fade d-block pl-50"><b>{{$countFour}}</b></span>
																</div>
															</div>
														</td>
													@endif
												@else 

												@endif

												@if($countFive > 0 )
													@php 
														$idfive = $StuFive['0']['class_id'];
													@endphp

													@if($data->class_id == $idfive)
														<td class="pl-15 py-8">
															<div class="d-flex align-items-center">
																<div>
																	<span class="text-fade d-block pl-50"><b>{{$countFive}}</b></span>
																</div>
															</div>
														</td>
													@endif
												@else 

												@endif

												@if($countSix > 0 )
													@php 
														$idsix = $StuSix['0']['class_id'];
													@endphp

													@if($data->class_id == $idsix)
														<td class="pl-15 py-8">
															<div class="d-flex align-items-center">
																<div>
																	<span class="text-fade d-block pl-50"><b>{{$countSix}}</b></span>
																</div>
															</div>
														</td>
													@endif
												@else 

												@endif
										</tr>
										@endforeach
									@endif	
																
									</tbody>
								</table>
							</div>
						</div>
					</div>  
				</div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>

@endsection