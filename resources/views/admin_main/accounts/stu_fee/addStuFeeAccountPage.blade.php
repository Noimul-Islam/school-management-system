@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add Student Fee Payment 
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
		  <div class="row">

		
            <div class="col-12">
                <div class="box bl-3 border-primary">
				  <div class="box-header">
					<h4 class="box-title">Add <strong>Student Fee Payment</strong></h4>
				  </div>

				  <div class="box-body">
				
		 
			        <div class="row"> 

                        <div class="col-md-3">

                            <div class="form_input form-group">
                                <h5>Class Name</h5>
                                <div class="controls">
                                <select name="class_id" id="class_id"class="form-control">
                                    <option value="" selected="" disabled="">Select Class</option>                                    
                                    @foreach($classes as $class)
                                    <option value="{{$class->id}}">{{$class->class_name}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                            </div>  

 			            </div> 

                         <div class="col-md-3">
    
                            <div class="form-group">
                            <h5>Select Group</h5>
                                <div class="controls">
                                <select name="group_id" id="group_id" class="form-control">
                                <option value="" selected="" disabled="">Select Group</option>                                    
                                                        
                                </select>
                                </div>
                            </div>

                        </div>

 			
 		                <div class="col-md-3">

                            <div class="form_input form-group">
                              <h5>Select Year</h5>
                              <div class="controls">
                              <select name="year_id" id="year_id" class="form-control">
                                  <option value="" selected="" disabled="">Select Year</option>                                    
                                  @foreach($years as $year)
                                  <option value="{{$year->id}}">{{$year->years}}</option>
                                  @endforeach                               
                              </select>
                              </div>
                            </div>
	  
                        </div> 


                        <div class="col-md-3">

                            <div class="form_input form-group">
                                <h5>Select Fee Category</h5>
                                <div class="controls">
                                <select name="feeCatgry_id" id="feeCatgry_id" class="form-control">
                                    <option value="" selected="" disabled="">Select Category</option>                                    
                                    @foreach($feeCatgrys as $catgory)
                                    <option value="{{$catgory->id}}">{{$catgory->categories}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                            </div>
	  
 			            </div> 

                        <!-- <div class="col-md-3 ">

                            <div class="form_input form-group">
                                <h5>Select Exam Type</h5>
                                <div class="controls">
                                <select name="feeCatgry_id" id="feeCatgry_id" class="form-control">
                                    <option value="" selected="" disabled="">Select Category</option>                                    
                                    @foreach($feeCatgrys as $catgory)
                                    <option value="{{$catgory->id}}">{{$catgory->categories}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                            </div>

                        </div> -->

                    </div><!--  end row 1-->

                    <div class="row"><!-- row 2-->


                        <div class="col-md-6">
                            <div class="form-group">
                            <h5> Month </h5>
                            <div class="controls">
                                <input type="month" name="date" id="date" class="form-control" > 
                            </div>
	                        </div>
 			            </div>

 			            <div class="col-md-6" style="margin-top:25px;">
                         <a id="srchButton" name="search" class="btn btn-rounded btn-dark">Search</a>
 			            </div>

			        </div><!--  end row 2--> 



                    <div class="row mt-20">
                        <div class="col-md-12">
                            
                        <script id="doc-template"  type="text/x-handlebars-template">
                            <form id="myForm" method="post" action="{{route('studentFee.adding')}}">
                                @csrf
                                <table class="table table-bordered table-striped" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            @{{{thsource}}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @{{#each this}}
                                            <tr>
                                                @{{{tdsource}}}
                                            </tr>
                                            @{{/each}}
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary" style="margin-top: 10px">Submit</button>
                            </form>
                        </script>

                        <div id="document-ready">
                            <script  type="text/x-handlebars-template">
                            <form id="myForm" method="post" action="{{route('studentFee.adding')}}">
                                @csrf
                                <table class="table table-bordered table-striped" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            @{{{thsource}}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @{{#each this}}
                                            <tr>
                                                @{{{tdsource}}}
                                            </tr>
                                            @{{/each}}
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary" style="margin-top: 10px">Submit</button>
                            </form>
                            </script> 		
                        </div> 	
                    </div>  <!-- end Row 2 -->
                </div>
            </div>
            <!-- /main row -->
            </section>   
        </div>
    </div>
  

    

<!-- Javascript Load Group by class wise-->
<script type="text/javascript">
    $(function(){
        $(document).on('change','#class_id',function(){
            var class_id = $('#class_id').val();
            $.ajax({
            url:"{{ route('marks.getGroupData') }}", //from defualt controller
            type:"GET",
            data:{'class_id':class_id},
            success:function(data){
                var html = '<option value="" disabled="">Select Group</option>';
                $.each( data, function(key, v) {
                html += '<option value="'+v.group_id+'">'+v.group_relation.groups+'</option>';
                });
                $('#group_id').html(html);
            }
            });
        });
    });
</script>



  <!-- Get students Fee account -->
<script type="text/javascript">
  $(document).on('click','#srchButton',function(){
    var class_id = $('#class_id').val();
    var year_id = $('#year_id').val();
    var feeCatgry_id = $('#feeCatgry_id').val();
    var group_id = $('#group_id').val();
    var date = $('#date').val();

    if(date.length==0){
    $('#document-ready').empty();
      toastr.error('Please Select Month');
    }else{
     $.ajax({
      url: "{{ route('studentFeeData.get')}}",
      type: "get",
      data: {'class_id':class_id,'year_id':year_id,'group_id':group_id,'feeCatgry_id':feeCatgry_id,'date':date},
      beforeSend: function() {       
      },
      success: function (data) {
        var source = $("#doc-template").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#document-ready').html(html);
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    }
  });

</script>


@endsection

