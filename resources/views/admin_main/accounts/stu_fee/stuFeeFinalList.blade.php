
@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Students fee account
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Students {{$allStuFee['0']['feeCatgryRelation']['categories']}} List</h3>
                    </br>
                  <h6 class="box-title mt-15" style="color:#fd7e14d4">
                    <strong style="color:#6f42c1cf">Class: </strong> 
                    {{$allStuFee['0']['classRelation']['class_name']}}
                  </h6>&nbsp;&nbsp;&nbsp;&nbsp;

                  <h6 class="box-title mt-5" style="color:#fd7e14d4">
                   <strong style="color:#6f42c1cf">Year: </strong> 
                  {{$allStuFee['0']['yearRelation']['years']}}
                  </h6>&nbsp;&nbsp;&nbsp;&nbsp;

                  <h6 class="box-title mt-5" style="color:#fd7e14d4">
                   <strong style="color:#6f42c1cf">Group: </strong> 
                   {{$allStuFee['0']['groupRelation']['groups']}}
                  </h6>&nbsp;&nbsp;&nbsp;&nbsp;

                  <a href="{{route('studentFee.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD NEW / EDIT</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
                                <th>ID no.</th>
								<th>Student Name</th>
                                <th>Father name</th>
                                <th>Phone</th>
								<th>Amount</th>
                                <th>Month</th>
                                <th>Status</th>
								<th>Action</th>
                                
							</tr>
						</thead>
						<tbody>
                        @foreach($allStuFee as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>								
								<td>{{$data['userrelation']['id_no']}} 
                                <td>{{$data['userrelation']['name']}}</td>
                                <td>{{$data['userrelation']['fatherName']}}</td>								
                                <td>{{$data['userrelation']['phone']}}</td>
                                <td>{{$data->amount}}/=</td>  
                                <td>{{date('M Y', strtotime($data->date))}}</td>   
								<td><img src="{{ asset('Upload/paid-stamp-icon.png') }}" alt="" style="height:45px; width:55px"></td>
                                   
								@if($data->feeCatgry_id == '3')
								<td><a href="{{route('paidSlipMonthlyFee.data', $data->id)}}" target="blank" title="paid slep" class="btn btn-info" style="font-size: 10px;">Paid Slip</a></td>
								@elseif($data->feeCatgry_id == '1')
								<td><a href="{{route('paidSlipExamFee.data', $data->id)}}" title="paid slep" class="btn btn-info" style="font-size: 10px;">Exam Slip</a></td>
								@else
								<td><a href="#" title="paid slep" class="btn btn-info" style="font-size: 10px;">Reg Slip</a></td>
								@endif
							</tr>
						@endforeach
						</tbody>
					  </table>
                      <a href="{{route('studentClass.view',[$data->feeCatgry_id,$data->year_id])}}" style="float:right"class="btn btn-rounded btn-dark mt-20">Back</a>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection



