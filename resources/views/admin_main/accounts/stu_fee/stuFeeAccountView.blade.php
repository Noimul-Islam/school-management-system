@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Students fee account
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Fee Type List</h3>
                  <a href="{{route('studentFee.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD NEW / EDIT</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>Fee category</th>   
								<th>Year</th>     
								<th width="15%">Action</th>                           
							</tr>
						</thead>
						<tbody>
                        @foreach($allStuFee as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>								
                                <td>{{$data['feeCatgryRelation']['categories']}}</td>
								<td>{{$data['yearRelation']['years']}}</td>    
								<td>                        
                                <a href="{{route('studentClass.view',[$data->feeCatgry_id,$data->year_id])}}" title="view date details" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection



