@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Students fee account
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Class List</h3>
                  <a href="{{route('studentFee.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD NEW / EDIT</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>Class Name</th> 
                                <th>Group</th>      
								<th width="15%">Action</th>                           
							</tr>
						</thead>
						<tbody>
                        @foreach($classData as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>								
								<td>{{$data['classRelation']['class_name']}}</td>   
                                <td>{{$data['groupRelation']['groups']}}<td>                        
                                <a href="{{route('studentFeeList.view',[$data->feeCatgry_id,$data->year_id,$data->class_id,$data->group_id])}}" title="view date details" class="btn btn-dark mb-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					  <a href="{{route('studentFee.view')}}" style="float:right"class="btn btn-rounded btn-dark mt-20">Back</a>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection



