@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - other cost account
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Other Cost Account List</h3>
                  <a href="{{route('otherCost.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD OTHER COST</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
                                <th>Image</th>
								<th>Description</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Action</th>
                                
							</tr>
						</thead>
						<tbody>
                        @foreach($allCost as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>								
								<td>
                                <img class="rounded avatar-md avatar-xl" 
									src="{{ (!empty($data->image))? url($data->image): url('Upload/no_img.jpg')}}" 
									alt="Card image cap">
                                </td>
                                <td>{{$data->desc}}</td>
                                <td>{{$data->amount}}/=</td>	
                                <td>{{date('d-m-Y', strtotime($data->date))}}</td>							
                                <td><a href="{{route('otherCost.edit', Crypt::encrypt($data->id))}}" class="btn btn-info"> Edit</a>
								<a href="{{route('otherCost.delete', Crypt::encrypt($data->id))}}" id="delete" class="btn btn-danger"> Delete</a></td> 
                                                             
                                
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection





