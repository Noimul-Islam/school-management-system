@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add other cost
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
	

		<section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Edit Other Cost</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('otherCost.editing') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more"> 

                            <!-- /row-1 -->
                            <div class="row">
                                
                                <div class="col-md-6">

                                <input type="hidden" name="id" value="{{$editCostData->id}}" class="form-control" >
            
                                    <div class="form_input form-group">
                                        <h5>Date<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="date" name="date" value="{{$editCostData->date}}" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form_input form-group">
                                        <h5>Amount<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="amount" value="{{$editCostData->amount}}" class="form-control" > 
                                    </div>		 
                                    </div> 
                                </div> 
                                
                            </div> <!-- end Row-1 -->


                            <!-- /row-2 -->
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
		 				                <h5>Description <span class="text-danger">*</span></h5>
						                <div class="controls">
						 	                <textarea name="desc" id="desc" class="form-control" required="" placeholder="Textarea text" aria-invalid="false">{{$editCostData->desc}}</textarea>
						                <div class="help-block"></div></div>
					                </div>
                                    
                                </div> 
                                
                            </div> <!-- end Row-2 -->



                            <!-- /row-3 -->
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form_input form-group">
                                        <h5>Image</h5>
                                        <div class="controls">
                                        <input id="imgUpload" type="file" name="image" onchange="fileValidation()" class="form-control">
                                        <div id="img_error" style="color:red; font-size:12px;" class="d-none">Image extesion shuould be in (jpg,jpeg,png)</div>
                                        </div>
                                    </div> 
                                                                                                          
                                </div> 

                                

                                <div class="col-md-6">

                                <div class="form-group">
                                        <img id="imgShow" class="rounded avatar-md avatar-xl" src="{{ (!empty($editCostData->image))? url($editCostData->image): url('Upload/no_images.jpg')}}" alt="Card image cap">
                                    </div>
                                                                   
  
                                </div>
                                
                            </div> <!-- end Row-3 -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('otherCost.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
  
	</div>
</div>






<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            date                     :{required : true},
            amount                     :{required : true, number: true, min : 1},
            desc                     :{required : true},
            
           },
           messages : {
            date                     : {required : 'Please Select Date'},
            desc                  : {required : 'Please Enter Description'},
            
            amount                   :{required : 'Please Enter Cost Amount', number: 'Enter in number', min: 'Minimum acceptable value should be at least {0}'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>



<!-- image extension validation -->
<script>
    function fileValidation() {

        var fileInput = document.getElementById('imgUpload');
        var filePath = fileInput.value;
        
        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
            
        if (allowedExtensions.test(filePath)) {
            document.getElementById('img_error').classList.add('d-none');
            
        }else{
            document.getElementById('img_error').classList.remove('d-none');
            fileInput.value = '';
            return false;
        }
    }
</script>


<!--js script for load image-->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#imgUpload').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#imgShow').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>





@endsection



