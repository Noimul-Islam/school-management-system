@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Employee Salary account
@endsection


<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Paid Employee Salary List</h3>
                  <a href="{{route('empSalary.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD NEW / EDIT</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
                                <th>ID no.</th>
								<th>Employee Name</th>
                                <th>Designation</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th>Month</th>
                                
							</tr>
						</thead>
						<tbody>
                        @foreach($allEmpSalary as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>								
								<td>{{$data['userrelation']['id_no']}}</td>
                                <td>{{$data['userrelation']['name']}}</td>
                                <td>{{$data['userrelation']['designationRel']['dasignation']}}</td>	
                                <td>{{$data['userrelation']['phone']}}</td>							
                                <td>{{$data->amount}}/=</td>  
                                <td>{{date('M Y', strtotime($data->date))}}</td>                             
                                
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection




