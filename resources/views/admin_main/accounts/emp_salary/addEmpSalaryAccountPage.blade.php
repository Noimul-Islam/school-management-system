@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add employee salary Payment
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		 

		<!-- Main content -->
		<section class="content">
		  <div class="row">

		
            <div class="col-12">
                <div class="box bl-3 border-primary">
                    <div class="box-header">
                        <h4 class="box-title">Add Employee <strong>Salary Payment</strong></h4>
                    </div>

                    <div class="box-body">
                            <div class="row" >

                                <div class="col-md-8">
            
                                    <div class="form_input form-group">
                                        <h5>Select Month<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="month" name="date" id="date" class="form-control" required> 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-2 mt-25">

                                    <a id="srchButton" name="search" class="btn btn-rounded btn-dark">Search</a>

                                </div>

                            </div><!--end search row --> 

                            </br>
                            <div class="row">
                                <div class="col-md-12">
                                <script id="doc-template" type="text/x-handlebars-template">
                            <form method="post" action="{{route('empSalary.adding')}}">
                                @csrf
                                <table class="table table-bordered table-striped" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            @{{{thsource}}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @{{#each this}}
                                            <tr>
                                                @{{{tdsource}}}
                                            </tr>
                                            @{{/each}}
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary" style="margin-top: 10px">Submit</button>
                            </form>
                        </script>

                        <div id="document-ready">
                            <script  type="text/x-handlebars-template">
                            <form method="post" action="{{route('empSalary.adding')}}">
                                @csrf
                                <table class="table table-bordered table-striped" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            @{{{thsource}}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @{{#each this}}
                                            <tr>
                                                @{{{tdsource}}}
                                            </tr>
                                            @{{/each}}
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary" style="margin-top: 10px">Submit</button>
                            </form>
                            </script>
                                    </div>                              
                                </div>     
                            </div>                      
                        </div>
                    </div>		            
                </div>    
            </div>        
		  <!-- /.row -->
		</section>
		<!-- /.content -->  
    </div>
</div>



<!-- Get monthly Fee -->
<script type="text/javascript">
  $(document).on('click','#srchButton',function(){
    var date = $('#date').val();
    if(date.length==0){
    $('#document-ready').empty();
      toastr.error('Please Select Month');
    }else{
     $.ajax({
      url: "{{ route('empSalaryData.get')}}",
      type: "get",
      data: {'date':date},
      beforeSend: function() {       
      },
      success: function (data) {
        var source = $("#doc-template").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#document-ready').html(html);
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    }
  });

</script>



@endsection


