<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.i {
    font-size: 15px;
    float: right;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}

.templete{
    width: 100%;
    margin: 0 auto;
}     
.clear{
    overflow:hidden;
}
.header_section{
    height: 123px;
    background:#202340;
}

.logo{
    width: 50%;
    float: left;
}
.logo h2{
    color: #ffffff;
    font-size: 25px;
    margin-top: 20px;
    margin-left: 10px;
    text-shadow: 6px 1px 7px #ff0303;
}
.logo p {
    font-size: 13px;
    margin-top: 10px;
    margin-left: 10px;
    color: #ffd662;
}
.social{
    float: right;
    margin-top: 19px;
    width: 50%;
}
.social p {
    font-size: 10px;
    margin-left: 60%;
    color: #ffd662;
}

.heading2 {
    text-align: center; 
    text-decoration: underline;
}
</style>
</head>


<body>
<div class="header_section templete clear">
    <div class="logo">
        <h2>Future Hope Education School</h2> 
        <p> <b>Email:</b> futurehopeedu@gmail.com</p> 
    </div>
    <div class="social clear">
        <p><b>Phone 1: </b> 01737364749</p> 
        <p><b>Phone 2: </b> 01737366754</p>
        <p><b>Address: </b> Mirpur, Dhaka, Bangladesh</p> 
    </div>
</div>

<h2 class="heading2">Employee Attendance Report</h2>


<table id="customers">

 <tr>
    <td colspan="3" style="text-align: left; background-color:#C0C5F6;">
      <strong><i>Employee Name: </i></strong> {{$allData['0']['userrelation']['name']}} &nbsp;&nbsp;&nbsp;&nbsp; <strong><i>Employee ID: </i></strong> {{$allData['0']['userrelation']['id_no']}} <br>
      <hr>
      <strong><i>Dasignation: </i></strong> {{$allData['0']['userrelation']['designationRel']['dasignation']}} &nbsp;&nbsp;&nbsp;&nbsp; <strong><i>Attendance Month:</i></strong>  {{date('M Y', strtotime($month))}}
    </td>
  </tr>
  <tr>
    <th width="10%">SL</th>
    <th width="45%">Date</th>
    <th width="45%">Attendance</th>
  </tr>

  @foreach($allData as $count=>$data)
  <tr>
    <td>{{$count+1}}</td>
    <td>{{ date('d-m-Y', strtotime($data->date))}}</td>

    @if($data->attentdance_status == 'Absent')
    <td style="background-color:#DB2727;">{{$data->attentdance_status}}</td>
    @elseif($data->attentdance_status == 'Leave')
    <td style="background-color:#DFB145;">{{$data->attentdance_status}}</td>
    @else
    <td style="background-color:#27DB50;">{{$data->attentdance_status}}</td>
    @endif
 </tr>
 @endforeach

 <tr>
    <td colspan="3" style="text-align: left; background-color:#C0C5F6;">
      <strong><i>Total Absent: </i></strong> {{$absents}} &nbsp;&nbsp;&nbsp;&nbsp;
      <strong><i>Total Leave: </i></strong> {{$leaves}} 
    </td>
  </tr>

</table>
</br>
<i class="i">Print date: {{date("d M Y")}}</i>

</body>
</html>





