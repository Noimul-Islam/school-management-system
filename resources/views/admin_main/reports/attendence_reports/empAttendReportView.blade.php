@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Employee attendance report 
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

            <div class="box bl-3 border-primary">
				  <div class="box-header">
					<h4 class="box-title">Search Employee (Attendance Report)</h4>
				  </div>

				  <div class="box-body">

                    <form  target="_blank" method="GET" action="{{route('getempAttendReport.data')}}">
                        @csrf
                        <div class="row">

                            <div class="col-md-4">

                                <div class="form_input form-group">
                                <h5>Employee Name</h5>
                                <div class="controls">
                                <select name="emp_id" class="form-control">
                                    <option value="" selected="" disabled="">Select Name</option>                                    
                                    @foreach($empData as $emp)
                                    <option value="{{$emp->id}}">{{$emp->name}} - ({{$emp['designationRel']['dasignation']}})</option>
                                    @endforeach                               
                                </select>
                                </div>
                                </div>

                            </div>  

                            <div class="col-md-5">

                                <div class="form_input form-group">
                                    <h5>Month<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                <input type="month" name="date" class="form-control"> 
                                </div>		 
                                </div>                            

                            </div>
       
                            <div class="col-md-2 mt-20">
                                <input type="submit" value="Search" class="btn btn-rounded btn-dark">
                            </div>
                        </div><!--end row -->
                    </form>
				  </div>
				</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>


@endsection


