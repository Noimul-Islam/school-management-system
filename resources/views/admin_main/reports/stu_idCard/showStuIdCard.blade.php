@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Students Id card
@endsection
<script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>


<div class="content-wrapper">
	  <div class="container-full">

        <div class="px-30 my-15">		  
			<a class="btn btn-app btn-success" style="float:right; position: static;" href="{{route('idCard.print',[$allData['0']['class_id'], $allData['0']['year_id'], $allData['0']['group_id']])}}" id="btnprn">
            <i class="fa fa-print" aria-hidden="true"></i> Print </a>
		  </div>
		</div>

        <script type="text/javascript">
        $(document).ready(function(){
        $('#btnprn').printPage();
        });
        </script>


    <div class="container_set">
    @foreach($allData as $data)
        <div class="card_box">
            <div class="title">
                <h3>Future Hope Education School</h3>
                <p>Dhaka,Bangladesh</p>
            </div>
            <div class="image">
                <div class="outer">
                    <div class="inner">
                        <img src="{{ (!empty($data['userrelation']['profile_photo']))? url($data['userrelation']['profile_photo']): url('Upload/no_img.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="name">
                Student ID Card
            </div>
            <div class="details">
                <div class="col">
                    <ul type="none">
                        <li>ID: {{$data['userrelation']['id_no']}}</li>
                        <li>Name: {{$data['userrelation']['name']}}</li>
                        <li>Class: {{$data['classrelation']['class_name']}}</li>

                        @if($data->roll == null)
                        <li>Empty</li>
                        @elseif($data->roll < 10)
                        <li>Roll: 0{{$data->roll}}</li>			
                        @else
                        <li>Roll: {{$data->roll}}</li>
                        @endif

                        <li>Group: {{$data['group_relation']['groups']}}</li>
                        <li>Session: {{$data['yearRelation']['years']}}</li>
                        <li>Contact: {{$data['userrelation']['phone']}}</li>
                    </ul>
                </div>
                <div class="col">
                    <img src="{{asset('Upload/My_logo.png')}}" alt="">
                </div>
            </div>

        </div>
		@endforeach
        
        
    </div>
</div>   
	
@endsection