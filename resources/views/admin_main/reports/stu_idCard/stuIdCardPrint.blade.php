<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Future Hope - Id card </title>
    <link rel="icon" href="{{ asset('backend/images/favvicon.ico') }}">
    <style>
*{
    margin: 0;
    padding: 0;
}

body{
    background-color: #ffff;
}

.container{
    display: flex;
	flex-wrap: wrap;
	width: 100%;
    justify-content: center;
    align-items: center;
	margin: 50px 0;
    
}

.card{
    width: 20%;
    margin: 15px;
	box-sizing: border-box;
    border-radius: 12px;
    background-color: #A084CA;
    overflow: hidden;
	float: left;
	padding-top: 10px;
	cursor:pointer;
}

.card .title h3{
    font-family: sans-serif;
    color: white;
    text-align: center;
	margin-top: 10px;
}

.card .title p{
	
	font-family:sans-serif;
	color: white;
	text-align: center;
}

.card .image{
    display: flex;
    justify-content: center;
    margin-top: 5%;
}

.card .image .outer{
    background-color: #EBC7E8;
    height: 110px;
    width: 110px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
}

.card .image .inner{
    height: 100px;
    width: 100px;
    overflow: hidden;
    border-radius: 50%;
}

.card .image .inner img{
    width: 100%;
}

.card .name{
    color: white;
    text-align: center;
    font-family: sans-serif;
    font-size: 1.2rem;
    font-weight: bold;
    margin-top: 2%;
}

.card .details{	
	background-color: #645CAA;
    height: 39%;
    margin-top: 4%;
    border-top-left-radius: 50px;
    padding-bottom: 5%;
    color: white;
    font-family: sans-serif;
    display: flex;
    align-items: center;
    justify-content: center;
	border-top: 4px solid white;
}

.card .details .col{
    width: 50%;
}

.card .details .col img{
    width: 80%;
	opacity: 0.5;
	padding-top: 30px;
}

.card .details .col:last-child{
    display: flex;
    justify-content: center;
    align-items: center;
}

.card .details li{
    margin-top: 2%;
	margin-left: 40px;
    width: 100%;
	font-size:0.8rem;
	padding: 1%;
}

@media(max-width:1000px){
	.card{
		width: 45%;
	}
}

@media(max-width:750px){
	.card{
		width: 45%;
	}
}


    </style>
    
    
</head>
<body>

    @foreach($allData as $data)
        <div class="card">
            <div class="title">
                <h3>Future Hope Education School</h3>
                <p>Dhaka,Bangladesh</p>
            </div>
            <div class="image">
                <div class="outer">
                    <div class="inner">
                        <img src="{{ (!empty($data['userrelation']['profile_photo']))? url($data['userrelation']['profile_photo']): url('Upload/no_img.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="name">
                Student ID Card
            </div>
            <div class="details">
                <div class="col">
                    <ul type="none">
                        <li>ID: {{$data['userrelation']['id_no']}}</li>
                        <li>Name: {{$data['userrelation']['name']}}</li>
                        <li>Class: {{$data['classrelation']['class_name']}}</li>
                        @if($data->roll == null)
                        <li></li>
                        @elseif($data->roll < 10)
                        <li>Roll: 0{{$data->roll}}</li>			
                        @else
                        <li>Roll: {{$data->roll}}</li>
                        @endif
                        <li>Group: {{$data['group_relation']['groups']}}</li>
                        <li>Session: {{$data['yearRelation']['years']}}</li>
                        <li>Contact: {{$data['userrelation']['phone']}}</li>
                    </ul>
                </div>
                <div class="col">
                    <img src="{{asset('Upload/My_logo.png')}}" alt="">
                </div>
            </div>

        </div>
		@endforeach
    </div>
</body>
</html>

