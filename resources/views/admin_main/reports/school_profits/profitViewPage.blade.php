@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - monthly profit
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		 

		<!-- Main content -->
		<section class="content">
		  <div class="row">

		
            <div class="col-12">
                <div class="box bl-3 border-primary">
                    <div class="box-header">
                        <h4 class="box-title">View <strong>Monthly or Yearly Profit</strong></h4>
                    </div>

                    <div class="box-body">
                            <div class="row" >

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>Start date</h5>
                                        <div class="controls">
                                    <input type="date" name="start_date" id="start_date" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4">
            
                                    <div class="form_input form-group">
                                        <h5>End date</h5>
                                        <div class="controls">
                                    <input type="date" name="end_date" id="end_date" class="form-control" > 
                                    </div>		 
                                    </div>

                                </div>

                                <div class="col-md-4 mt-25">

                                    <a id="srchButton" name="search" class="btn btn-rounded btn-dark">Search</a>

                                </div>

                            </div><!--end search row --> 

                            </br>
                            <div class="row">
                                <div class="col-md-12">
                                    <script id="doc-template" type="text/x-handlebars-template">
                                        <table class="table table-bordered table-striped" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    @{{{thsource}}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    
                                                    <tr>
                                                        @{{{tdsource}}}
                                                    </tr>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </script>

                                    <div id="document-ready">
                                        <script  type="text/x-handlebars-template">
                                        <table class="table table-bordered table-striped" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        @{{{thsource}}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        
                                                        <tr>
                                                            @{{{tdsource}}}
                                                        </tr>
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </script>
                                    </div>                              
                                </div>     
                            </div>                      
                        </div>
                    </div>		            
                </div>    
            </div>        
		  <!-- /.row -->
		</section>
		<!-- /.content -->  
    </div>
</div>



<!-- Get monthly Fee -->
<script type="text/javascript">
  $(document).on('click','#srchButton',function(){
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
     $.ajax({
      url: "{{ route('getProfitReport.data')}}",
      type: "get",
      data: {'start_date':start_date,'end_date':end_date},
      beforeSend: function() {       
      },
      success: function (data) {
        var source = $("#doc-template").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#document-ready').html(html);
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
  });

</script>



@endsection

