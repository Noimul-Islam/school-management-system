@php 

    $stuFees = App\Models\stuFeeAccount::whereBetween('date',[$start_date,$end_date])->sum('amount');

    $otherCost = App\Models\otherCostAccount::whereBetween('date',[$S_date,$E_date])->sum('amount'); 

    $empSalaries = App\Models\empSalaryAccount::whereBetween('date',[$start_date,$end_date])->sum('amount');

    $totalCost = $otherCost+$empSalaries;
    $profit = $stuFees-$totalCost;

@endphp


<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.i {
    font-size: 15px;
    float: right;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}

.templete{
    width: 100%;
    margin: 0 auto;
}     
.clear{
    overflow:hidden;
}
.header_section{
    height: 123px;
    background:#202340;
}

.logo{
    width: 50%;
    float: left;
}
.logo h2{
    color: #ffffff;
    font-size: 25px;
    margin-top: 20px;
    margin-left: 10px;
    text-shadow: 6px 1px 7px #ff0303;
}
.logo p {
    font-size: 13px;
    margin-top: 10px;
    margin-left: 10px;
    color: #ffd662;
}
.social{
    float: right;
    margin-top: 19px;
    width: 50%;
}
.social p {
    font-size: 10px;
    margin-left: 60%;
    color: #ffd662;
}

.heading2 {
    text-align: center; 
    text-decoration: underline;
}
</style>
</head>


<body>
<div class="header_section templete clear">
    <div class="logo">
        <h2>Future Hope Education School</h2> 
        <p> <b>Email:</b> futurehopeedu@gmail.com</p> 
    </div>
    <div class="social clear">
        <p><b>Phone 1: </b> 01737364749</p> 
        <p><b>Phone 2: </b> 01737366754</p>
        <p><b>Address: </b> Mirpur, Dhaka, Bangladesh</p> 
    </div>
</div>

<h2 class="heading2">Monthly & yearly profit report</h2>


<table id="customers">

 <tr>
    <td colspan="3" style="text-align: center; background-color:#C0C5F6;">
      <h4>Reporting Date: {{ date('d M Y', strtotime($S_date)) }} - {{ date('d M Y', strtotime($E_date)) }}</h4>

    </td>
  </tr>
  <tr>
    <th width="10%">SL</th>
    <th width="45%">Perpuse</th>
    <th width="45%">Amount</th>
  </tr>

  <tr>
    <td>1</td>
    <td><b>Student Fees</b></td>
    <td>{{$stuFees}}/=</td>
 </tr>

 <tr>
    <td>2</td>
    <td><b>Employee Salary Cost</b></td>
    <td>{{$empSalaries}}/=</td>
 </tr>


 <tr>
    <td>3</td>
    <td><b>Other Cost</b></td>
    <td>{{$otherCost}}/=</td>
 </tr>
 <tr>
    <td>4</td>
    <td><b>Total Cost</b></td>
    <td>{{$totalCost}}/=</td>
 </tr>
 @if($profit < $totalCost)
 <tr style="background-color:#DB2727;">
    <td>5</td>
    <td><b style="color:#EEF5EF;">Loss Amount</b></td>
    <td style="color:#EEF5EF;">{{$profit}}/=</td>
 </tr>
 @else
 <tr style="background-color:#27DB50;" >
    <td>5</td>
    <td><b style="color:#EEF5EF;">Profit Amount</b></td>
    <td style="color:#EEF5EF;">{{$profit}}/=</td>
 </tr>
 @endif
  
</table>
</br>
<i class="i">Print date: {{date("d M Y")}}</i>

</body>
</html>




