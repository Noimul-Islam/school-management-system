<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
	<!-- Vendors Style-->
	<link rel="stylesheet" href="{{ asset('backend/css/vendors_css.css') }}">
	<!-- Style-->  
	<link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/print_skin_color.css') }}">
     
  </head>

<body class="hold-transition dark-skin theme-primary">
	
<div class="wrapper">

	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">
        
            <div class="box bl-3 border-primary">
                <div class="box-body" style="border: solid 1px; padding: 15px;">

                <div class="row"> <!--row 1-->

                    <div style="float: right; width:33%;" class="col-md-2 text-center mt-15">
                        <img src="{{ url('backend/images/My_logo.png') }}" style="width: 110px; height: 110px;">			
                    </div>

                    <div class="col-md-2 text-center" style="float: right; width:1%;">
  			
  		            </div>

                    <div class="col-md-4 text-center" style="float: left; width:33%;" >
                        <h3><strong>Future Hope Education School</strong></h3>
                        <h6><strong>Dhaka, Bangladesh</strong></h6>
                        <h4><strong><u><i>Academic Transcript</i></u></strong></h4>
                        <h5><strong>{{ $marksData['0']['examType_relation']['examTypes'] }}</strong></h5>                       
                    </div>

                    <div class="col-md-4 text-center">
                    <p style="text-align: right;"><u><i>Issue Date: </i>{{ date('d M Y') }} </u></p>
  		            </div>
                    <div class="col-md-12">
                        <hr style="border: solid 1px; width: 100%; color: #ddd; margin-bottom: 0px;">                       
  		            </div>

                </div><!--end row 1-->

                <br><br>
                <div class="row"> <!--row 2-->

                    <div class="col-md-6" style="width: 50%;">

                        <table border="1" style="border-color: #ffffff;" width="100%" cellpadding="8" cellspacing="2">

                        <tr>
                            <td width="50%"><b>Student Id</b></td>
                            <td width="50%">{{ $marksData['0']['id_no'] }}</td>
                        </tr>

                        <tr>
                            <td width="50%"><b>Student Name </b></td>
                            <td width="50%">{{ $marksData['0']['userrelation']['name'] }}</td>
                        </tr>

                        <!-- for showing student roll -->
                        @php 
                            $assign_stu_data = App\Models\AssignStudent::where('class_id',$marksData['0']->class_id)->where('group_id',$marksData['0']->group_id)->where('year_id',$marksData['0']->year_id)->where('student_id',$marksData['0']->student_id)->first();
                        @endphp
                        <tr>
                            <td width="50%"><b>Roll No</b></td>
                            @if($assign_stu_data->roll < 10)
                            <td width="50%">0{{ $assign_stu_data->roll }}</td>
                            @else
                            <td width="50%">{{ $assign_stu_data->roll }}</td>
                            @endif
                        </tr>

                        <tr>
                            <td width="50%"><b>Father Name </b></td>
                            <td width="50%">{{ $marksData['0']['userrelation']['fatherName'] }}</td>
                        </tr>

                        <tr>
                            <td width="50%"><b>Mother Name </b></td>
                            <td width="50%">{{ $marksData['0']['userrelation']['motherName'] }}</td>
                        </tr>

                        <tr>
                            <td width="50%"><b>Class</b></td>
                            <td width="50%">{{ $marksData['0']['class_relation']['class_name'] }}</td>
                        </tr>

                        <tr>
                            <td width="50%"><b>Group</b></td>
                            <td width="50%">{{ $marksData['0']['group_relation']['groups'] }}</td>
                        </tr>


                        <tr>
                            <td width="50%"><b>Session</b></td>
                            <td width="50%">{{ $marksData['0']['year_relation']['years'] }}</td>
                        </tr>

                        </table>
                    </div>


                    <div class="col-md-6" style="width: 50%;">

                        <table border="1" style="border-color: #ffffff;" width="100%" cellpadding="8" cellspacing="2">

                        <thead>
                            <tr>
                                <th>Letter Grade</th>
                                <th>Marks Interval</th>
                                <th>Grade Point</th>
                            </tr>
                        </thead>

                        <tbody>

                        @foreach($marksGrades as $grade)
                            <tr>
                                <td>{{$grade->grade_name}}</td>
                                <td>{{$grade->start_mark}} - {{$grade->end_mark}}</td>
                                <td>{{number_format((float)$grade->grade_point,2)}} - {{ ($grade->grade_point == 5)?(number_format((float)$grade->grade_point,2)):(number_format((float)$grade->grade_point+1,2) - (float)0.01) }}</td>
                            </tr>
                        @endforeach

                        </tbody>



                        </table>
                    </div>


                </div><!--end row 2-->


                <br><br>

                <div class="row"> <!-- row 3 -->
                    <div class="col-md-12">

                        <table border="1" style="border-color: #ffffff;" width="100%" cellpadding="8" cellspacing="2">
                            <thead>
                            <tr>
                                <th class="text-center">SL</th>
                                <th class="text-center">Subjects</th>
                                <th class="text-center">Get Marks</th>
                                <th class="text-center">Letter Grade</th>
                                <th class="text-center">Grade Point</th>    
                            </tr>
                            </thead>

                            <tbody>
                            @php
                                $total_marks = 0;
                                $total_point = 0;
                            @endphp

                            @foreach($marksData as $count => $mark)
                                @php
                                $get_mark = $mark->marks;
                                $total_marks = (float)$total_marks+(float)$get_mark;
                                $total_subject = App\Models\studentMarks::where('group_id',$mark->group_id)->where('year_id',$mark->year_id)->where('class_id',$mark->class_id)->where('examType_id',$mark->examType_id)->where('student_id',$mark->student_id)->get()->count();
                                @endphp
                                <tr>
                                    <td class="text-center">{{ $count+1 }}</td>

                                    @php
                                    $subject =  App\Models\subsAssign::where('id',$mark->assign_sub_id)->get();
                                    @endphp
 
                                    <td class="text-center">{{ $subject['0']['subject_relation']['subject_name'] }}</td>
                                    <td class="text-center">{{ $get_mark }}</td>

                                    @php
                                    $grade_marks = App\Models\marksGrade::where([['start_mark','<=', (int)$get_mark],['end_mark', '>=',(int)$get_mark ]])->first();
                                    $grade_name = $grade_marks->grade_name;
                                    $grade_point = number_format((float)$grade_marks->grade_point,2);
                                    $total_point = (float)$total_point+(float)$grade_point;
                                    @endphp

                                    @if($get_mark < 33)
                                    <td class="text-center" style="color: red;">{{ $grade_name }}</td>
                                    @else
                                    <td class="text-center">{{ $grade_name }}</td>
                                    @endif
                                    <td class="text-center">{{ $grade_point }}</td>

                                </tr>
                            @endforeach     
                                <tr>
                                    <td colspan="4"><strong style="padding-left: 50%;">Total Marks</strong></td>
                                    <td colspan="4"><strong style="padding-left: 38px;">{{ $total_marks }}</strong></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>      
                </div> <!-- end row 3 -->
                <br><br>


                <div class="row">  <!-- row 4 -->
                    <div class="col-md-12">

                        <table border="1" style="border-color: #ffffff;" width="100%" cellpadding="8" cellspacing="2">
                            @php
                            $total_grade = 0;
                            $point_for_letter_grade = (float)$total_point/(float)$total_subject;
                            $total_grade = App\Models\marksGrade::where([['start_point','<=',$point_for_letter_grade],['end_point','>=',$point_for_letter_grade]])->first();

                            $grade_point_avg = (float)$total_point/(float)$total_subject;

                            @endphp
                            <tr>
                                <td class="text-center" width="50%"><strong>Grade Point Average</strong></td>
                                <td class="text-center" width="50%"> 
                                @if($failCount > 0)
                                0.00
                                @else
                                {{number_format((float)$grade_point_avg,2)}}
                                @endif
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center" width="50%"><strong>Letter Grade </strong></td>
                                <td class="text-center" width="50%"> 
                                    @if($failCount > 0)
                                    <strong style="color:red;">Fail</strong>
                                    @else
                                    {{ $total_grade->grade_name }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" width="50%">Total Marks with Fraction</td>
                                <td class="text-center" width="50%"><strong>{{ $total_marks }}</strong></td>
                            </tr>
                        </table>        
                    </div>        
                </div>   <!--  end row 4 -->
                <br><br>

                <div class="row">  <!--  row 5 -->
                    <div class="col-md-12">

                    <table border="1" style="border-color: #ffffff;" width="100%" cellpadding="8" cellspacing="2">
                        <tbody>
                            <tr>
                                <td style="text-align: center;"><strong>Remrks:</strong>
                                    @if($failCount > 0)
                                    Fail
                                    @else
                                    {{ $total_grade->remarks }}
                                    @endif
                                </td>
                            </tr> 
                        </tbody>
                    </table>        
                </div>        
            </div>   <!--  end row 5 -->
            <br><br><br><br>
 
            <div class="row"> <!--  row 6 -->
                <div class="col-md-4" style="width: 33%;">
                    <hr style="border: solid 1px; widows: 60%; color: #ffffff; margin-bottom: -3px;">
                    <div class="text-center mt-2" >Teacher</div>
                </div>

                <div class="col-md-4" style="width: 33%;">
                    <hr style="border: solid 1px; widows: 60%; color: #ffffff; margin-bottom: -3px;">
                    <div class="text-center mt-2">Parents / Guardian </div>
                </div>

                <div class="col-md-4" style="width: 33%;">
                    <hr style="border: solid 1px; widows: 60%; color: #ffffff; margin-bottom: -3px;">
                    <div class="text-center mt-2">Principal / Headmaster</div>
                </div>
  
            </div>  <!-- end row 6 -->
            <br><br>            
                </div>
            </div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>

  
  </div>
  <!-- ./wrapper -->
        
       
  <!-- Vendor JS -->
  <script src="{{ asset('backend/js/vendors.min.js') }}"></script>
    <script src="{{ asset('../assets/icons/feather-icons/feather.min.js') }}"></script>	

  
    <!-- Sunny Admin App -->
      <script src="{{ asset('backend/js/template.js') }}"></script>

    <!-- JAVASCRIPT print data-->
    <!--<script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('js/jquery.printPage.js') }}"></script>
      
  </body>
  </html>