@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Results list
@endsection


<script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>


<div class="content-wrapper">
	  <div class="container-full">

      <!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Student Results List</h3>
					<div class="d-inline-block align-items-center">
					</div>
				</div>
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">

          <div class="col-12">

                <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $marksData['0']['examType_relation']['examTypes'] }}</h3></br></br>
                    <h6 class="box-title mt-5" style="color:#fd7e14d4">
                        <strong style="color:#6f42c1cf">Class Name: </strong> 
                        {{$marksData['0']['class_relation']['class_name']}}
                        &nbsp;&nbsp;&nbsp;&nbsp; <strong style="color:#6f42c1cf"><i>Year: </i></strong> {{$marksData['0']['year_relation']['years']}} &nbsp;&nbsp;&nbsp;&nbsp; 
                        <strong style="color:#6f42c1cf"><i>Group:</i></strong>  {{$marksData['0']['group_relation']['groups']}}
                    </h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table  class="table table-bordered table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th width="10%">SL</th>
                                <th>Student ID</th>
                                <th>Student name</th>
                                <th>Grade Point Average</th>
                                <th>Letter Grade</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($marksData as $count => $data)

                                @php 
                                $all_result = App\Models\studentMarks::where('class_id',$data->class_id)->where('group_id',$data->group_id)->where('year_id',$data->year_id)->where('examType_id',$data->examType_id)->where('student_id',$data->student_id)->get();
                                $total_marks = 0;
                                $total_point = 0;

                                foreach($all_result as $vlu){
                                    $failCount = App\Models\studentMarks::where('class_id',$vlu->class_id)->where('group_id',$vlu->group_id)->where('year_id',$vlu->year_id)->where('examType_id',$vlu->examType_id)->where('student_id',$vlu->student_id)->where('marks','<','33')->get()->count();
                                    $getMark = $vlu->marks;
                                    $marksGrade = App\Models\marksGrade::where([['start_mark','<=', (int)$getMark],['end_mark', '>=',(int)$getMark ]])->first();
                                    $gradeName = $marksGrade->grade_name;
                                    $grade_point = number_format((float)$marksGrade->grade_point,2);
                                    $total_point = (float)$total_point+(float)$grade_point;
                                }
                                @endphp

                                <tr>
                                <td class="text-center">{{ $count+1 }}</td>
                                <td class="text-center">{{$data['userrelation']['id_no']}}</td>
                                <td class="text-center">{{$data['userrelation']['name']}}</td>
                                @php 

                                $total_subject = App\Models\studentMarks::where('group_id',$data->group_id)->where('year_id',$data->year_id)->where('class_id',$data->class_id)->where('examType_id',$data->examType_id)->where('student_id',$data->student_id)->get()->count();
                                $total_grade = 0;
                                $point_for_letter_grade = (float)$total_point/(float)$total_subject;
                                $total_grade = App\Models\marksGrade::where([['start_point','<=',$point_for_letter_grade],['end_point','>=',$point_for_letter_grade]])->first();
                                $grade_point_avg = (float)$total_point/(float)$total_subject;

                                @endphp
                                

                                <td class="text-center">
                                    @if($failCount > 0)
                                        0.00
                                    @else
                                        {{number_format((float)$grade_point_avg,2)}}
                                    @endif 
                                </td>


                                <td class="text-center">
                                    @if($failCount > 0)
                                        <strong style="color:red;">Fail</strong>
                                    @else
                                        {{ $total_grade->grade_name }}
                                    @endif
                                </td>

                                <td class="text-center">

                                    @if($failCount > 0)
                                    <strong style="color:red;">Fail</strong>
                                    @else
                                    {{ $total_grade->remarks }}
                                    @endif
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->      
                </div>
		  </div>
		  <!-- /.row -->
		</section>
        <div class="px-30 my-15 m">		  
            <a class="btn btn-app btn-success" style="float:right; position: static;" href="{{route('sturesultReport.print', [$marksData['0']['class_id'], $marksData['0']['year_id'], $marksData['0']['group_id'], $marksData['0']['examType_id']])}}" id="btnprn">
            <i class="fa fa-print" aria-hidden="true"></i> Print </a>
        </div>
		<!-- /.content -->
	  </div>
  </div>


<script type="text/javascript">
    $(document).ready(function(){
    $('#btnprn').printPage();
    });
</script>





@endsection