<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.i {
    font-size: 15px;
    float: right;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}

.templete{
    width: 100%;
    margin: 0 auto;
}     
.clear{
    overflow:hidden;
}
.header_section{
    height: 123px;
    background:#202340;
}

.logo{
    width: 50%;
    float: left;
}
.logo h2{
    color: #ffffff;
    font-size: 25px;
    margin-top: 20px;
    margin-left: 10px;
    text-shadow: 6px 1px 7px #ff0303;
}
.logo p {
    font-size: 13px;
    margin-top: 10px;
    margin-left: 10px;
    color: #ffd662;
}
.social{
    float: right;
    margin-top: 19px;
    width: 50%;
}
.social p {
    font-size: 10px;
    margin-left: 60%;
    color: #ffd662;
}

.heading2 {
    text-align: center; 
    text-decoration: underline;
    color: #04AA6D;
}
</style>
</head>


<body>
<div class="header_section templete clear">
    <div class="logo">
        <h2>Future Hope Education School</h2> 
        <p> <b>Email:</b> futurehopeedu@gmail.com</p> 
    </div>
    <div class="social clear">
        <p><b>Phone 1: </b> 01737364749</p> 
        <p><b>Phone 2: </b> 01737366754</p>
        <p><b>Address: </b> Mirpur, Dhaka, Bangladesh</p> 
    </div>
</div>

<h2 class="heading2"><strong>Student Results of </strong> {{ $marksData['0']['examType_relation']['examTypes'] }} </h2>


<table id="customers">
    <tr>
    <td colspan="6" style="text-align: left; background-color:#C0C5F6;">
        <strong><i>Class name: </i></strong> {{$marksData['0']['class_relation']['class_name']}} &nbsp;&nbsp;&nbsp;&nbsp; <strong><i>Year: </i></strong> {{$marksData['0']['year_relation']['years']}} &nbsp;&nbsp;&nbsp;&nbsp; <strong><i>Group:</i></strong>  {{$marksData['0']['group_relation']['groups']}}
        <hr>
    </td>
  </tr>
                      
  <tr>
    <th width="10%">SL</th>
    <th>Student ID</th>
    <th>Student name</th>
    <th>Grade Point Average</th>
    <th>Letter Grade</th>
    <th>Remarks</th>
  </tr>
  @foreach($marksData as $count => $data)


    @php 
     $all_result = App\Models\studentMarks::where('class_id',$data->class_id)->where('group_id',$data->group_id)->where('year_id',$data->year_id)->where('examType_id',$data->examType_id)->where('student_id',$data->student_id)->get();
     $total_marks = 0;
     $total_point = 0;

     foreach($all_result as $vlu){
        $failCount = App\Models\studentMarks::where('class_id',$vlu->class_id)->where('group_id',$vlu->group_id)->where('year_id',$vlu->year_id)->where('examType_id',$vlu->examType_id)->where('student_id',$vlu->student_id)->where('marks','<','33')->get()->count();
        $getMark = $vlu->marks;
        $marksGrade = App\Models\marksGrade::where([['start_mark','<=', (int)$getMark],['end_mark', '>=',(int)$getMark ]])->first();
        $gradeName = $marksGrade->grade_name;
        $grade_point = number_format((float)$marksGrade->grade_point,2);
        $total_point = (float)$total_point+(float)$grade_point;
     }
    @endphp

  <tr>
    <td class="text-center">{{ $count+1 }}</td>
    <td class="text-center">{{$data['userrelation']['id_no']}}</td>
    <td class="text-center">{{$data['userrelation']['name']}}</td>
     @php 

     $total_subject = App\Models\studentMarks::where('group_id',$data->group_id)->where('year_id',$data->year_id)->where('class_id',$data->class_id)->where('examType_id',$data->examType_id)->where('student_id',$data->student_id)->get()->count();
     $total_grade = 0;
     $point_for_letter_grade = (float)$total_point/(float)$total_subject;
     $total_grade = App\Models\marksGrade::where([['start_point','<=',$point_for_letter_grade],['end_point','>=',$point_for_letter_grade]])->first();
     $grade_point_avg = (float)$total_point/(float)$total_subject;

     @endphp
       

    <td class="text-center">
        @if($failCount > 0)
            0.00
        @else
            {{number_format((float)$grade_point_avg,2)}}
        @endif 
    </td>

    
    <td class="text-center">
        @if($failCount > 0)
            <strong style="color:red;">Fail</strong>
        @else
            {{ $total_grade->grade_name }}
        @endif
    </td>

    <td class="text-center">

        @if($failCount > 0)
        <strong style="color:red;">Fail</strong>
        @else
        {{ $total_grade->remarks }}
        @endif
    </td>
 </tr>
 @endforeach
  
</table>
</br>
<i class="i">Print date: {{date("d M Y")}}</i>

</br></br>
    <div style="text-decoration:overline; margine-top: 50px;">Principal / Headmaster</div>


</body>
</html>


