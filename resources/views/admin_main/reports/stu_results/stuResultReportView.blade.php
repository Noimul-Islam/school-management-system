@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Results report 
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

            <div class="box bl-3 border-primary">
				  <div class="box-header">
					<h4 class="box-title">Search Student (Exam Result Report)</h4>
				  </div>

				  <div class="box-body">

                    <form method="GET" action="{{route('getStuResultReport.data')}}">
                        @csrf
                        <div class="row">

                            <div class="col-md-3">

                                <div class="form_input form-group">
                                <h5>Class Name</h5>
                                <div class="controls">
                                <select name="class_id" id="class_id"class="form-control">
                                    <option value="" selected="" disabled="">Select Class</option>                                    
                                    @foreach($classes as $class)
                                    <option value="{{$class->id}}">{{$class->class_name}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                                </div>

                            </div>  

                            <div class="col-md-3">
                                <div class="form_input form-group">
                                <h5>Select Year</h5>
                                <div class="controls">
                                <select name="year_id" id="year_id" class="form-control">
                                    <option value="" selected="" disabled="">Select Year</option>                                    
                                    @foreach($years as $year)
                                    <option value="{{$year->id}}">{{$year->years}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                                </div> 
                            </div>
                            
                            <div class="col-md-3">                                
                                <div class="form_input form-group">
                                    <h5>Select Group</h5>
                                    <div class="controls">
                                    <select name="group_id" id="group_id" class="form-control">
                                        <option value="" selected="" disabled="">Select Group</option>                                    
                                                             
                                    </select>
                                    </div>
                                    </div>
                            </div>

                            <div class="col-md-3">

                                <div class="form_input form-group">
                                <h5>Exam Type</h5>
                                <div class="controls">
                                <select name="examType_id" id="examType_id"class="form-control">
                                    <option value="" selected="" disabled="">Select Type</option>                                    
                                    @foreach($examType as $exam)
                                    <option value="{{$exam->id}}">{{$exam->examTypes}}</option>
                                    @endforeach                               
                                </select>
                                </div>
                                </div>

                            </div>
                        </div><!--end row -->


                        <div class="row">      

                                <div class="col-md-5 mt-20"> </div>
                                <div class="col-md-5 mt-20"> </div>     

                                <div class="col-md-2 mt-20">
                                    <input type="submit" value="Search" class="btn btn-rounded btn-dark">
                                </div>

                        </div><!--end row 2 -->
                    </form>
				  </div>
				</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>




  <!-- Javascript Load Group by class wise-->
<script type="text/javascript">
    $(function(){
        $(document).on('change','#class_id',function(){
            var class_id = $('#class_id').val();
            $.ajax({
            url:"{{ route('marks.getGroupData') }}", //from defualt controller
            type:"GET",
            data:{'class_id':class_id},
            success:function(data){
                var html = '<option value="" disabled="">Select Group</option>';
                $.each( data, function(key, v) {
                html += '<option value="'+v.group_id+'">'+v.group_relation.groups+'</option>';
                });
                $('#group_id').html(html);
            }
            });
        });
    });
</script>


@endsection


