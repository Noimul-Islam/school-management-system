@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit User
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

      <section class="content">

		 <!-- Basic Forms -->
		  <div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title">Edit User</h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  <div class="row">
				<div class="col">


					<form id="myForm" method="POST" action="{{route('User.editing')}}">
                    @csrf    
					  <div class="row">
						<div class="col-12">	
                        <div class="row">

                            <input type="hidden" name="id" value="{{$userData->id}}">


                            <div class="col-md-12">

                                <div class="form_input form-group">
                                    <h5>User Name <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="text" name="name" value="{{$userData->name}}" class="form-control">  
                                    </div>                               
                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form_input form-group">
                                    <h5>User Email <span class="text-danger"></span></h5>
                                    <div class="controls">
                                    <input type="email" name="email" value="{{$userData->email}}" class="form-control" readOnly>
                                    </div>
                                </div>

                            </div>



                            <div class="col-md-12">
                                <div class="form_input form-group">
                                <h5>User role <span class="text-danger">*</span></h5>
                                
                                <select name="role" id="role" class="form-control">
                                    <option value="" selected="" >Select role</option>
                                    <option value="Admin" {{ ($userData->role == 'Admin') ? 'selected': '' }}> Admin</option>
                                    <option value="Operator" {{ ($userData->role == 'Operator') ? 'selected': '' }}> Operator</option>
                                </select>
                                
                            </div>

                            <div class="form-group">
							<input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('Users.view')}}" style="float:right"class="btn btn-rounded btn-dark" >Back</a>
						    </div>
                            </div>
                        </div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->

		</section>

    </div>
</div>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
               name            :{required : true},
               email           :{required : true, email: true},
               
               userType        :{required : true},
           },
           messages : {
               name            : {required : 'Please Enter Name' },
               email           : {required : 'Please Enter Email Id' },
              
              userType         : {required : 'Please Select a Roll'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection