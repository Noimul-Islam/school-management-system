@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Add User
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

      <section class="content">

            <!-- Basic Forms -->
            <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Add New User</h4>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <div class="col">

                    <form id="myForm" method="post" action="{{ route('User.adding') }}">
                    @csrf
                        <div class="row">
                        <div class="col-12">
                        <div class="add_more">
                                                            
                            <div class="form_input form-group">
                                <h5>User role <span class="text-danger">*</span></h5>
                                <div class="controls">
                                <select name="role" id="role" class="form-control">
                                    <option value="" selected="" disabled="">Select role</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Operator">Operator</option>
                                </select>
                                </div>
                            </div> 

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form_input form-group">
                                        <h5>User Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                        <input type="text" name="name" value="{{ old('name') }}" class="form-control"> 
                                        </div>                                
                                    </div>
                                </div> 

                                <div class="col-md-6">
            
                                    <div class="form_input form-group">
                                        <h5>User Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                        <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                
                            </div> <!-- end Row -->

                        </div>	<!-- // End add_more -->
                        <hr>
                                
                        <div class="text-xs-right mt-10">
                            <input type="submit" class="btn btn-rounded btn-info" value="Add User">
                            <a href="{{route('Users.view')}}" style="float:right"class="btn btn-rounded btn-dark">Back</a>
                        </div>
                    </form>

                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>

    </div>
</div>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
               name            :{required : true},
               email           :{required : true, email: true},
               role             :{required : true},
           },
           messages : {
               name            : {required : 'Please Enter Name' },
               email           : {required : 'Please Enter Email Address' },
              role         : {required : 'Please Select Roll'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection