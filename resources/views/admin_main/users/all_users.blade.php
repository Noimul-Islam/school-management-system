@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - All Users 
@endsection

<div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">All Users</h3>
                  <a href="{{route('User.add')}}" class="btn btn-success btn-flat mb-5" style="float:right">ADD USER</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%">SL</th>
								<th>Name</th>
								<th>Email</th>
								<th>Role</th>
								<th>Code</th>
                                <th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
                        @foreach($allUsers as $count=>$data)
							<tr>
								<td>{{$count+1}}</td>
								<td>{{$data->name}}</td>
								<td>{{$data->email}}</td>
								<td>{{$data->role}}</td>	
								@if($data->code == null)
								<td>Empty</td>
								@else	
								<td>{{$data->code}}</td>
								@endif						
                                <td>
                                    <a href="{{route('User.edit', Crypt::encrypt($data->id))}}" class="btn btn-info mb-5"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="{{route('User.delete', Crypt::encrypt($data->id))}}" id="delete" class="btn btn-danger mb-5"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
							</tr>
						@endforeach
						</tbody>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>         
			</div>
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>


@endsection