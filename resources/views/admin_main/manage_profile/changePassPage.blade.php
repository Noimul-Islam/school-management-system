@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit User
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

      <section class="content">

		 <!-- Basic Forms -->
		  <div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title">Change Password</h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  <div class="row">
				<div class="col">


					<form id="myForm" method="POST" action="{{route('updationg.pass')}}">
                    @csrf    
					  <div class="row">
						<div class="col-12">	
                        <div class="row">

                            <input type="hidden" name="id" value="">


                            <div class="col-md-12">

                                <div class="form_input form-group">
                                    <h5>Current Password <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="password" name="currPass" class="form-control">  
                                    </div>                               
                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form_input form-group">
                                    <h5>New Password <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="password" id="new-pass" name="newPass" class="form-control">
                                    </div>
                                </div>

                            </div>



                            <div class="col-md-12">
                                <div class="form_input form-group">
                                    <h5>Confirm Password <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="password" name="confPass" class="form-control">
                                    </div>
                                </div>
                                
                            </div>

						    </div>

                        </div>
                        <div class="text-xs-right pl-5">
							<input type="submit" class="btn btn-rounded btn-info" value="Change">
						</div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->

		</section>

    </div>
</div>



<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
            currPass             :{required : true, minlength: 6},
            newPass              :{required : true, minlength: 6},
            confPass             :{required : true, minlength: 6, equalTo: "#new-pass"},
           },
           messages : {
            currPass            : {required : 'Please Enter Current Password',
                                  minlength: 'Password Should be at least {0} characters long'
                                 },
            newPass             : {required : 'Please Enter New Password',
                                  minlength: 'Password Should be at least {0} characters long'
                                 },
            confPass            : {required : 'Please Enter Confirm Password',
                                  minlength: 'Password Should be at least {0} characters long',
                                  equalTo:  'New Password and Confirm Password not Matched'
                                 },
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>





@endsection