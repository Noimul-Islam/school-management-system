@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Profile Page 
@endsection

<div class="content-wrapper">
    <div class="container-full">

        <section class="content">
            <div class="row">

                <div class="col-12">

				  <!-- /.box -->
				 <div class="box box-widget widget-user">
					<!-- Add the bg color to the header using any of the bg-* classes -->
					<div class="widget-user-header bg-black"  center center;">
					  <h3 class="widget-user-username">{{ Str::of($adminData->name)->upper() }}</h3>

                      <a href="{{route('edit.profile')}}" class="btn btn-success btn-flat mb-5" style="float:right">Edit Profile</a>

					  <h6 class="widget-user-desc">{{$adminData->role}}</h6>
                      <h6 class="widget-user-desc">Email: {{$adminData->email}}</h6>
					</div>

     
					<div class="widget-user-image">
					  <img id="imgShow" class="rounded-circle" src="{{ (!empty($adminData->profile_photo))? url($adminData->profile_photo): url('Upload/no_img.jpg')}}" alt="Card image cap">
					</div>
					<div class="box-footer">
					  <div class="row">
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">Gender</h5>
							<span class="description-text">{{$adminData->gender}}</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 br-1 bl-1">
						  <div class="description-block">
							<h5 class="description-header">Phone Number</h5>
							<span class="description-text">{{$adminData->phone}}</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">Address</h5>
							<span class="description-text">{{$adminData->address}}</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
					  </div>
					  <!-- /.row -->
					</div>
				  </div>


                </div>
            </div>
        </section>
    </div>
	</div>
<div>
@endsection