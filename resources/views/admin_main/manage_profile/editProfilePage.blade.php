@extends('admin_main.admin_master')
@section('adminContent')
@section('title')
 Future Hope - Edit Profile 
@endsection

<!--Jquery library CDN link from w3 school (for js validation)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="content-wrapper">
	  <div class="container-full">

      <section class="content">

		 <!-- Basic Forms -->
		  <div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title">Edit Profile</h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
			  <div class="row">
				<div class="col">


					<form id="myForm" method="POST" action="{{route('updating.profile')}}" enctype="multipart/form-data">
                    @csrf    
					  <div class="row">
						<div class="col-12">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form_input form-group">
                                    <h5>User Name <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="text" name="name" value="{{$adminData->name}}" class="form-control"> 
                                    </div>                                
                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="form_input form-group">
                                    <h5>User Email <span class="text-danger"></span></h5>
                                    <div class="controls">
                                    <input type="email" name="email" value="{{$adminData->email}}" class="form-control" readOnly>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-6">

                                <div class="form_input form-group">
                                <h5>Gender <span class="text-danger">*</span></h5>
                                <div class="controls">
                                <select name="gender" id="gender" class="form-control">
                                            <option value="">Select Gender</option>
                                            <option value="Male" {{ ($adminData->gender == 'Male') ? 'selected': '' }}>Male</option>
                                            <option value="Female" {{ ($adminData->gender == 'Female') ? 'selected': '' }}>Female</option>
                                        </select>
                                </div>
                            </div>

                            </div>

                            <div class="col-md-6">
            
                                    <div class="form_input form-group">
                                        <h5>Phone<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                    <input type="text" name="phone" id="phoneNumCheck" onKeyup="validation()" value="{{$adminData->phone}}" class="form-control" >
                                    <div id="phone_error" style="color:red; font-size:12px;" class="d-none">Please enter a valid phone number</div> 
                                    </div>		 
                                    </div>

                                </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="form_input form-group">
                                    <h5>User Address <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <input type="text" name="address" value="{{$adminData->address}}" class="form-control"> 
                                    </div>                                
                                </div>

                            </div>

                            <div class="col-md-6">

                                    <div class="form_input form-group">
                                        <h5>Profile Photo</h5>
                                        <div class="controls">
                                        <input id="imgUpload" type="file" name="profile_photo" onchange="fileValidation()" class="form-control">
                                        <div id="img_error" style="color:red; font-size:12px;" class="d-none">Image extesion shuould be in (jpg,jpeg,png)</div>
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <img id="imgShow" class="rounded avatar-md avatar-xl" src="{{ (!empty($adminData->profile_photo))? url($adminData->profile_photo): url('Upload/no_img.jpg')}}" alt="Card image cap">
                                </div>

                            </div>
                        </div>
                        
						<div class="text-xs-right pl-5">
							<input type="submit" class="btn btn-rounded btn-info" value="Update">
                            <a href="{{route('view.profile')}}" style="float:right"class="btn btn-rounded btn-dark" >Back</a>
						</div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->

		</section>

    </div>
</div>

<!--phone number custom validation-->
<script type="text/javascript">
    function validation(){
        var phone = document.getElementById('phoneNumCheck').value;

        var phonePattern = /^\+?(88)?0?1[3456789][0-9]{8}$/ ;

        if(phonePattern.test(phone)){
            document.getElementById('phone_error').classList.add('d-none');
        }else{
            document.getElementById('phone_error').classList.remove('d-none');
        }
    }
</script>

<!--Javascript validation function-->
<script type="text/javascript">

   $(document).ready(function(){

       $('#myForm').validate({

           rules: {
               name            :{required : true},
               email           :{required : true, email: true},
               phone           :{required : true},
               gender          :{required : true},
               address          :{required : true},
           },
           messages : {
               name            : {required : 'Please Enter Name' },
               email           : {required : 'Please Enter Email Id' },
               phone           : {required : 'Please Enter Phone No' },
               gender          : {required : 'Please Select Gender'},
               address         : {required : 'Please Enter Address'},
           },
           // error ta kothai show korbe 
           errorElement : 'span',
           errorPlacement: function (error,element) {
               error.addClass('invalid-feedback');
               element.closest('.form_input').append(error); //<div class="form_input"><input></div>
           },
           //highlight the error with proper design
           highlight : function(element, errorClass, validClass){
               $(element).addClass('is-invalid');
           },
           unhighlight : function(element, errorClass, validClass){
               $(element).removeClass('is-invalid');
           },
       });
   });
</script>


<!-- image extension validation -->
<script>
    function fileValidation() {

        var fileInput = document.getElementById('imgUpload');
        var filePath = fileInput.value;
        
        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
            
        if (allowedExtensions.test(filePath)) {
            document.getElementById('img_error').classList.add('d-none');
            
        }else{
            document.getElementById('img_error').classList.remove('d-none');
            fileInput.value = '';
            return false;
        }
    }
</script>


<!--js script for load image-->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#imgUpload').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#imgShow').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>

@endsection