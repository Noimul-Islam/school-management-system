<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignStudent extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function userrelation(){  //making relation betwen allUser and assign Student table

        return $this->belongsTo(allUsers::class, 'student_id', 'id');
    }

    public function classRelation(){  

        return $this->belongsTo(stuClasses::class, 'class_id', 'id');
    }

    public function yearRelation(){  

        return $this->belongsTo(stuYears::class, 'year_id', 'id');
    }

    public function group_relation(){  

        return $this->belongsTo(stuGroups::class, 'group_id', 'id');
    }

    

    public function shiftRelation(){  

        return $this->belongsTo(stuShifts::class, 'shift_id', 'id');
    }

    public function discountRelation(){  

        return $this->belongsTo(StuDiscount::class, 'id', 'assign_stu_id'); //reverse the id cz, assignStuId is the FK of the discount table
    }
}
