<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class empSalaryAccount extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function userrelation(){  //making relation 

        return $this->belongsTo(allUsers::class, 'employee_id', 'id');
    }
}
