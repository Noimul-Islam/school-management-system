<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class stu_attendence extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function userrelation(){  //making relation betwen allUser and assign Student table

        return $this->belongsTo(allUsers::class, 'student_id', 'id');
    }

    public function classRelation(){  

        return $this->belongsTo(stuClasses::class, 'class_id', 'id');
    }

    public function yearRelation(){  

        return $this->belongsTo(stuYears::class, 'year_id', 'id');
    }

    public function groupRelation(){  

        return $this->belongsTo(stuGroups::class, 'group_id', 'id');
    }
}
