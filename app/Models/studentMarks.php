<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class studentMarks extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function userrelation(){  //making relation 

        return $this->belongsTo(allUsers::class, 'student_id', 'id');
    }

    public function assing_sub_relation(){  //making relation 

        return $this->belongsTo(subsAssign::class, 'assign_sub_id', 'id');
    }

    public function class_relation(){  //making relation 

        return $this->belongsTo(stuClasses::class, 'class_id', 'id');
    }

    public function group_relation(){  //making relation 

        return $this->belongsTo(stuGroups::class, 'group_id', 'id');
    }

    public function year_relation(){  //making relation 

        return $this->belongsTo(stuYears::class, 'year_id', 'id');
    }

    public function examType_relation(){  //making relation 

        return $this->belongsTo(examTypes::class, 'examType_id', 'id');
    }
}
