<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employeeLeave extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function userrelation(){  //making relation betwen allUser and emp leave table

        return $this->belongsTo(allUsers::class, 'employee_id', 'id');
    }

    public function perposeRelation(){  //making relation betwen perpose and emp leave table

        return $this->belongsTo(empLeavePerpose::class, 'leavePurpose_id', 'id');
    }
}
