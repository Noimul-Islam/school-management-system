<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class stuFeeAmounts extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function feeCatgryRelation(){  //making relation betwen fee categories and fee amount table

        return $this->belongsTo(stuFeeCategorys::class, 'feeCatgryId', 'id');
    }

    public function stuClassRelation(){  //making relation betwen Classes and fee amount table 

        return $this->belongsTo(stuClasses::class, 'classId', 'id');
    }
}
