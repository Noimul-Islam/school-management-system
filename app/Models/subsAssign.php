<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subsAssign extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function stuClassRelation(){  //making relation betwen Classes and assign sub table 

        return $this->belongsTo(stuClasses::class, 'classId', 'id');
    }

    public function subject_relation(){  //making relation betwen subjects and assign sub table 

        return $this->belongsTo(subjects::class, 'subjectId', 'id');
    }

}
