<?php

namespace App\Http\Controllers\backend_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\allUsers;
use Auth;
use Carbon\Carbon;
use Image;


class profileController extends Controller
{
    //profile view
    public function viewProfile(){
   
        $id = Auth::guard('user')->user()->id;
        $adminData= allUsers::find($id);

        return view('admin_main.manage_profile.profilePage',compact('adminData'));
    }




    //edit profile view
    public function viewEditProfile(){
   
        $id = Auth::guard('user')->user()->id;
        $adminData= allUsers::find($id);

        return view('admin_main.manage_profile.editProfilePage',compact('adminData'));
    }



    //updating profile 
    public function updateProfile(Request $request){
   
        $id = Auth::guard('user')->user()->id;
        $values= allUsers::find($id);

        $values->name = $request->name; 
        $values->email = $request->email;
        $values->gender = $request->gender;
        $values->phone = $request->phone;
        $values->address = $request->address;

        if($request->file('profile_photo')){

            $image = $request->file('profile_photo');
            $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension(); //store the file's name as unique code
            image::make($image)->resize(128,128)->save('Upload/profileImage/'.$imageName);//file store path

            $imageUrl =  'Upload/profileImage/'.$imageName;
            $values->profile_photo = $imageUrl;
        }

        $values->save();

        $msg= array(                        
            'message' => 'Profile updated successfully',
            'alert-type' => 'success' 
        );

        return redirect()->route('view.profile')->with($msg);
    }



    //change password view  
    public function viewChangePass(){

        return view('admin_main.manage_profile.changePassPage');
    }




    //updating password 
    public function updatingPass(Request $request){

        $hashedPassword = Auth::guard('user')->user()->password; //get current hashed password

        if(Hash::check($request->currPass,$hashedPassword)){

            $userdata = allUsers::find(Auth::guard('user')->user()->id); //get all data of the logged in user. 

            $userdata->password = bcrypt($request->newPass); //inserting requested data as hashing

            $userdata->save();
            Auth::guard('user')->logout();

            $msg= array(                        
                'message' => 'Password Changed. Please login again',
                'alert-type' => 'success' 
            );
            return redirect()->route('login.form')->with($msg);
        }else{

            $msg= array(                        
                'message' => 'Current Password is Not Matched',
                'alert-type' => 'error' 
            );
            return redirect()->back()->with($msg);

        }
    }
}
