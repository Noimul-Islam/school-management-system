<?php

namespace App\Http\Controllers\backend_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\stuClasses;
use App\Models\stuYears;
use Auth;
use Carbon\Carbon;

class usersController extends Controller
{


    //admin login form view
    public function indexLogin(){

        return view('admin_main.adminLoginPage'); 
    }



    //admin login process
    public function loginProcess(Request $request){

        //dd($request->all());
        $check= $request->all();

        if(Auth::guard('user')->attempt(['role' => $check['role'],'email' => $check['email'], 'password' => $check['password']])){

            $msg = array(
                'message' => "You are Successfully Logged In",
                'alert-type' => 'success'
            );
            return redirect()->route('admin.dashbord')->with($msg);

        }else{

            $msg = array(
                'message' => "Invalid email or password!!!",
                'alert-type' => 'error'
            );

            return back()->withInput()->with($msg);

        }
        
    }


    
    //admin dashbord view
    public function dashbord(){
   
        $id = Auth::guard('user')->user()->id;  //get auth admin id
        $adminData= allUsers::find($id);

        return view('admin_main.index',compact('adminData'));
    }



    //admin logout
    public function logOut(){

        Auth::guard('user')->logout();

        $msg = array(
            'message' => "Sussessfully Logged Out",
            'alert-type' => 'success'
        );

        return redirect()->route('login.form')->with($msg);
    }
















    //admin register form view
    public function indexRegister(){

        return view('admin_main.adminRegisterPage');
    }
    
    
    
    //admin register process
    public function registProcess(Request $request){


        $email = allUsers::where('email', $request->email)->get();

    
        # check if email is more than 1
        if(sizeof($email) > 0){ 
            
            $msg = array(
                'message' => "This email is already exist",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);

        }else{

            allUsers::insert([

                'name' => $request->name, 
                'userType' => 'Admin',
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Admin Created success",
                'alert-type' => 'success'
            );
            return redirect()->route('login.form')->with($msg);

        }

    }
}
