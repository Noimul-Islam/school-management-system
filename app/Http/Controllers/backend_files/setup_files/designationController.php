<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\dasignation;
use Carbon\Carbon;
use \Crypt;

class designationController extends Controller
{
    //all designation view
    public function viewDesignatios(){

        $allData = dasignation::latest()->get();
        return view('admin_main.setup_management.designation.viewDasignations',compact('allData'));
    }


    //add designation view
    public function addDesignation(){

        return view('admin_main.setup_management.designation.addDesignationPage');
    }



    //adding designation
    public function addingDesignation(Request $request){

        $desiData = dasignation::where('dasignation', $request->dasignation)->get();

        if(sizeof($desiData) > 0 ){  # check if group name is more than 1
            $msg = array(
                'message' => "This Designation is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            dasignation::insert([

                'dasignation' => $request->dasignation,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Dasignation Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('designations.view')->with($msg);

        }
    }


    //edit designation Page view
    public function editDesignation($id){

        $dcry_id = Crypt::decrypt($id);
        $desiData = dasignation::findOrFail($dcry_id);

        return view('admin_main.setup_management.designation.editDasignationPage',compact('desiData'));
    }



    //editing designation
    public function editingDesignation(Request $request){

        $id= $request->id;

        $desiData = dasignation::where('dasignation', $request->dasignation)->get();

        if(sizeof($desiData) > 0 ){  # check if group name is more than 1
            $msg = array(
                'message' => "This Designation is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            dasignation::where('id',$id)->update([

                'dasignation' => $request->dasignation,
            ]);
    
            $msg = array(
                'message' => "Dasignation Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('designations.view')->with($msg);

        }
        
    }


    //delete designation 
    public function deleteDesignation($id){

        $dcry_id = Crypt::decrypt($id);
        $Data = dasignation::findOrFail($dcry_id);
        $Data->delete();

        return redirect()->route('designations.view');
    }

}
