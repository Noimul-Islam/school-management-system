<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuGroups;
use Carbon\Carbon;
use \Crypt;

class stuGroupController extends Controller
{

    //all groups view
    public function viewGroups(){

        $allData = stuGroups::latest()->get();
        return view('admin_main.setup_management.student_group.groupsViewPage',compact('allData'));
    }


    //add Group view
    public function addGroup(){

        return view('admin_main.setup_management.student_group.addGroupsPage');
    }


    //adding Group
    public function addingGroup(Request $request){

        $group = stuGroups::where('groups', $request->group)->get();

        if(sizeof($group) > 0 ){  # check if group name is more than 1
            $msg = array(
                'message' => "This Group Name is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            stuGroups::insert([

                'groups' => $request->group,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Group Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('groups.view')->with($msg);

        }
    }


    //editGroupPage view
    public function editGroup($id){

        $dcry_id = Crypt::decrypt($id);
        $groupData = stuGroups::findOrFail($dcry_id);

        return view('admin_main.setup_management.student_group.editGroupPage',compact('groupData'));
    }



    //editing Group
    public function editingGroup(Request $request){

        $id= $request->id;

        $group = stuGroups::where('groups', $request->group)->get();

        if(sizeof($group) > 0 ){  # check if group name is more than 1
            $msg = array(
                'message' => "This Group Name is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            stuGroups::where('id',$id)->update([

                'groups' => $request->group,
            ]);
    
            $msg = array(
                'message' => "Group Name Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('groups.view')->with($msg);

        }
        
    }


    //delete year 
    public function deleteGroup($id){

        $dcry_id = Crypt::decrypt($id);
        $groupData = stuGroups::findOrFail($dcry_id);
        $groupData->delete();

        return redirect()->route('groups.view');
    }
}
