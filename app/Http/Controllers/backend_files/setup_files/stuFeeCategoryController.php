<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuFeeCategorys;
use Carbon\Carbon;
use \Crypt;

class stuFeeCategoryController extends Controller
{
    
    //all feeCat view
    public function viewFeeCatgry(){

        $allData = stuFeeCategorys::latest()->get();
        return view('admin_main.setup_management.fee_category.feeCatgryView',compact('allData'));
    }


    //add feeCat view
    public function addFeeCatgry(){

        return view('admin_main.setup_management.fee_category.addFeeCatgryPage');
    }


    //adding feeCat
    public function addingFeeCatgry(Request $request){

        $feeCat = stuFeeCategorys::where('categories', $request->category)->get();

        if(sizeof($feeCat) > 0 ){  
            $msg = array(
                'message' => "This Category is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            stuFeeCategorys::insert([

                'categories' => $request->category,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Category Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('feeCategory.view')->with($msg);

        }
    }


    //edit feeCat view
    public function editFeeCatgry($id){

        $dcry_id = Crypt::decrypt($id);
        $feeCatData = stuFeeCategorys::findOrFail($dcry_id);

        return view('admin_main.setup_management.fee_category.editFeeCatgryPage',compact('feeCatData'));
    }


    //editing feeCat
    public function editingFeeCatgry(Request $request){

        $id= $request->id;

        $feeCat = stuFeeCategorys::where('categories', $request->category)->get();

        if(sizeof($feeCat) > 0 ){  
            $msg = array(
                'message' => "This Category is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            stuFeeCategorys::where('id',$id)->update([

                'categories' => $request->category,
            ]);
    
            $msg = array(
                'message' => "Fee Category Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('feeCategory.view')->with($msg);

        }
        
    }


    //delete feeCat 
    public function deleteFeeCatgry($id){

        $dcry_id = Crypt::decrypt($id);
        $feeCatData = stuFeeCategorys::findOrFail($dcry_id);
        $feeCatData->delete();

        return redirect()->route('feeCategory.view');
    }
}
