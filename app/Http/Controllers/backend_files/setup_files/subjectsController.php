<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\subjects;
use Carbon\Carbon;
use \Crypt;

class subjectsController extends Controller
{
    
    //all subject view
    public function viewSubjects(){

        $allData = subjects::all();
        return view('admin_main.setup_management.subjects.allSubjectView',compact('allData'));
    }



    //add subject view
    public function addSubject(){

        return view('admin_main.setup_management.subjects.addSubjectPage');
    }



    //adding subject
    public function addingSubjecte(Request $request){

        $sub = subjects::where('subject_name', $request->subject_name)->get();

        if(sizeof($sub) > 0 ){  # check if the type is more than 1
            $msg = array(
                'message' => "This Subject is already taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            subjects::insert([

                'subject_name' => $request->subject_name,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Subject Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('subjects.view')->with($msg);

        }
    }




    //edit subject view
    public function editSubject($id){

        $dcry_id = Crypt::decrypt($id);
        $subData = subjects::findOrFail($dcry_id);

        return view('admin_main.setup_management.subjects.editSubjectPage',compact('subData'));
    }



    //editing subject
    public function editingSubject(Request $request){

        $id= $request->id;

        $sub = subjects::where('subject_name', $request->subject_name)->get();

        if(sizeof($sub) > 0 ){  # check if the type is more than 1
            $msg = array(
                'message' => "This Subject is already taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            subjects::where('id',$id)->update([

                'subject_name' => $request->subject_name,
            ]);
    
            $msg = array(
                'message' => "Subject Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('subjects.view')->with($msg);

        }
        
    }



    //delete subject 
    public function deleteSubject($id){

        $dcry_id = Crypt::decrypt($id);
        $Data = subjects::findOrFail($dcry_id);
        $Data->delete();

        return redirect()->route('subjects.view');
    }

}
