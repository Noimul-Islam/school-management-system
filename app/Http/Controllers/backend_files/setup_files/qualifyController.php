<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\emp_qualification;
use Carbon\Carbon;
use \Crypt;

class qualifyController extends Controller
{
    //all employee qualify list view
    public function viewQualify(){

        $allData = emp_qualification::latest()->get();
        return view('admin_main.setup_management.emp_qualify.qualifyViewPage',compact('allData'));
    }


    //add Qualify form view
    public function addQualify(){

        return view('admin_main.setup_management.emp_qualify.addQualifyPage');
    }



    //adding Qualify
    public function addingQualify(Request $request){

        $Qualify = emp_qualification::where('name', $request->name)->get();

        if(sizeof($Qualify) > 0 ){  # check if group name is more than 1
            $msg = array(
                'message' => "This Qualification Name is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            emp_qualification::insert([

                'name' => $request->name,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Qualification Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('qualification.view')->with($msg);

        }
    }


    //edit Qualify Page view
    public function editQualify($id){

        $dcry_id = Crypt::decrypt($id);
        $editData = emp_qualification::findOrFail($dcry_id);

        return view('admin_main.setup_management.emp_qualify.editQualifyPage',compact('editData'));
    }



    //editing Qualify
    public function editingQualify(Request $request){

        $id= $request->id;

        $qualify = emp_qualification::where('name', $request->name)->get();

        if(sizeof($qualify) > 0 ){  # check if group name is more than 1
            $msg = array(
                'message' => "This Qualification Name is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->with($msg);
        
        }else{

            emp_qualification::where('id',$id)->update([

                'name' => $request->name,
            ]);
    
            $msg = array(
                'message' => "Qualification Name Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('qualification.view')->with($msg);

        }
        
    }


    //delete Qualify 
    public function deleteQualify($id){

        $dcry_id = Crypt::decrypt($id);
        $Data = emp_qualification::findOrFail($dcry_id);
        $Data->delete();

        return redirect()->route('qualification.view');
    }
}
