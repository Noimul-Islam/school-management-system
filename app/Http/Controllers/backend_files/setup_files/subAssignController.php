<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\subsAssign;
use App\Models\stuClasses;
use App\Models\subjects;
use Carbon\Carbon;
use \Crypt;

class subAssignController extends Controller
{
    
    //all sub Assign view
    public function viewAssignSub(){

        $allData = subsAssign::select('classId')->groupBy('classId')->get();  //get group by data
        //$allData = subsAssign::all();

        return view('admin_main.setup_management.subjects_assign.allAssignSubView',compact('allData'));
    }


    //add sub Assign view
    public function addSubAssign(){

        $subData = subjects::all();
        $classData = stuClasses::all();

        return view('admin_main.setup_management.subjects_assign.addAssignSubPage',compact('subData','classData'));
    }

    //adding sub Assign 
    public function addingSubAssign(Request $req){

        $subCount = count($req->subjectId);

        if($subCount != NULL){

            for($i=0; $i < $subCount; $i++){

                subsAssign::insert([

                    'classId'   => $req->classId,
                    'subjectId'       => $req->subjectId[$i],
                    'fullMark'        => $req->fullMark[$i],
                    'passMark'        => $req->passMark[$i],
                    'subjectiveMark'  => $req->subjectiveMark[$i],
                    'created_at'      => Carbon::now()
                ]);
            }
        } //end if
        $msg = array(
            'message' => "New Subject Assigned Successfully",
            'alert-type' => 'success'
        );
        return redirect()->route('assignSubs.view')->with($msg);
    }



    //edit sub Assign  view
    public function editAssignSub($id){

        $dcry_id = Crypt::decrypt($id);
        $allEditData = subsAssign::where('classId', $dcry_id)->orderBy('subjectId','asc')->get();

        $subData = subjects::all();
        $classData = stuClasses::all();

        return view('admin_main.setup_management.subjects_assign.editAssignSubPage',compact('allEditData','classData','subData'));
    }


    //editing  Assign sub
    public function editingAssignSub(Request $req){

        $cid = $req->clsID;
        
        if($req->subjectId == NULL){

            $msg = array(
                'message' => "Sorry you did not select any Subject",
                'alert-type' => 'error'
            );
            return redirect()->route('AssignSub.edit',Crypt::encrypt($cid))->with($msg);

        }else{

            $subCount = count($req->subjectId);

            subsAssign::where('classId',$cid)->delete(); 
            
    		for ($i=0; $i <$subCount ; $i++) { 
    			$assigned = new subsAssign();
    			$assigned->classId = $req->classId;
                $assigned->subjectId = $req->subjectId[$i];
                $assigned->fullMark = $req->fullMark[$i];
                $assigned->passMark = $req->passMark[$i];
    			$assigned->subjectiveMark = $req->subjectiveMark[$i];
    			$assigned->save();

    		}
        } // end else
        $msg = array(
            'message' => "Data Updated Successfully",
            'alert-type' => 'success'
        );
        return redirect()->route('assignSubs.view')->with($msg);
    }



    //add feeCat Amount details view
    public function detailsAssignSub($cid){

        $dcry_id = Crypt::decrypt($cid);
        $allData = subsAssign::where('classId', $dcry_id)->orderBy('subjectId','asc')->get();

        return view('admin_main.setup_management.subjects_assign.detailsAssignSubPage',compact('allData'));
    }

}
