<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\empLeavePerpose;
use Carbon\Carbon;
use \Crypt;

class empLeavePerposeSetupController extends Controller
{
    //all leave purposes view
    public function viewPurposes(){

        $allData = empLeavePerpose::latest()->get();
        return view('admin_main.setup_management.leave_purpose.empleavePurposeView',compact('allData'));
    }


    //add leave purpose view
    public function addPurpose(){

        return view('admin_main.setup_management.leave_purpose.addleavePurposePage');
    }


    //adding leave purpose
    public function addingPurpose(Request $request){

        $purpose = empLeavePerpose::where('perposes', $request->perposes)->get();

        if(sizeof($purpose) > 0 ){  
            $msg = array(
                'message' => "This Purpose Type is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            empLeavePerpose::insert([

                'perposes' => $request->perposes,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Purpose Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('purposes.view')->with($msg);

        }
    }


    //edit leave purpose view
    public function editPurpose($id){

        $dcry_id = Crypt::decrypt($id);
        $purposeData = empLeavePerpose::findOrFail($dcry_id);

        return view('admin_main.setup_management.leave_purpose.editleavePurposePage',compact('purposeData'));
    }


    //editing leave purpose
    public function editingPurpose(Request $request){

        $id= $request->id;

        $purpose = empLeavePerpose::where('perposes', $request->perposes)->get();

        if(sizeof($purpose) > 0 ){  
            $msg = array(
                'message' => "This Purpose Type is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->with($msg);
        
        }else{

            empLeavePerpose::where('id',$id)->update([

                'perposes' => $request->perposes
            ]);
    
            $msg = array(
                'message' => "Leaving Purpose Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('purposes.view')->with($msg);

        }
        
    }


    //delete leave purpose 
    public function deletePurpose($id){

        $dcry_id = Crypt::decrypt($id);
        $Data = empLeavePerpose::findOrFail($dcry_id);
        $Data->delete();

        return redirect()->route('purposes.view');
    }

}
