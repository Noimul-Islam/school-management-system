<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuClasses;
use Carbon\Carbon;
use \Crypt;

class stuClassesController extends Controller
{
    
    //all classes view
    public function viewClasses(){

        $classData = stuClasses::all();
        return view('admin_main.setup_management.student_classes.classesViewPage',compact('classData'));
    }



    //add classes view
    public function addClasses(){

        return view('admin_main.setup_management.student_classes.addClassPage');
    }



    //adding class
    public function addingClass(Request $request){

        $class = stuClasses::where('class_name', $request->class_name)->get();

        if(sizeof($class) > 0 ){  # check if class name is more than 1
            $msg = array(
                'message' => "This name is already taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            stuClasses::insert([

                'class_name' => $request->class_name,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Class Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('classes.view')->with($msg);

        }
    }




    //edit class view
    public function editClass($id){

        $dcry_id = Crypt::decrypt($id);
        $classData = stuClasses::findOrFail($dcry_id);

        return view('admin_main.setup_management.student_classes.classEditPage',compact('classData'));
    }



    //editing class
    public function editingClass(Request $request){

        $id= $request->id;

        $class = stuClasses::where('class_name', $request->class_name)->get();

        if(sizeof($class) > 0 ){  # check if class name is more than 1
            $msg = array(
                'message' => "This name is already taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->with($msg);
        
        }else{

            stuClasses::where('id',$id)->update([

                'class_name' => $request->class_name,
            ]);
    
            $msg = array(
                'message' => "Class Name Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('classes.view')->with($msg);

        }
        
    }



    //delete class 
    public function deleteClass($id){

        $dcry_id = Crypt::decrypt($id);
        $classData = stuClasses::findOrFail($dcry_id);
        $classData->delete();

        return redirect()->route('classes.view');
    }
}
