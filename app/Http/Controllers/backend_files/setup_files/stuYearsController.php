<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuYears;
use Carbon\Carbon;
use \Crypt;

class stuYearsController extends Controller
{

    //all years view
    public function viewYears(){

        $allData = stuYears::latest()->get();
        return view('admin_main.setup_management.student_years.yearsViewPage',compact('allData'));
    }



    //add year view
    public function addYear(){

        return view('admin_main.setup_management.student_years.addYearPage');
    }



    //adding year
    public function addingyear(Request $request){

        $year = stuYears::where('years', $request->year)->get();

        if(sizeof($year) > 0 ){  # check if class name is more than 1
            $msg = array(
                'message' => "This Year is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            stuYears::insert([

                'years' => $request->year,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Year Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('years.view')->with($msg);

        }
    }



    //edit year view
    public function editYear($id){

        $dcry_id = Crypt::decrypt($id);
        $yearData = stuYears::findOrFail($dcry_id);

        return view('admin_main.setup_management.student_years.editYearPage',compact('yearData'));
    }



    //editing year
    public function editingYear(Request $request){

        $id= $request->id;

        $year = stuYears::where('years', $request->year)->get();

        if(sizeof($year) > 0 ){  # check if class name is more than 1
            $msg = array(
                'message' => "This Year is Already Taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->with($msg);
        
        }else{

            stuYears::where('id',$id)->update([

                'years' => $request->year,
            ]);
    
            $msg = array(
                'message' => "Year Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('years.view')->with($msg);

        }
        
    }


     //delete year 
     public function deleteYear($id){

        $dcry_id = Crypt::decrypt($id);
        $yearData = stuYears::findOrFail($dcry_id);
        $yearData->delete();

        return redirect()->route('years.view');
    }
}
