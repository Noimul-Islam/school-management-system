<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuFeeAmounts;
use App\Models\stuClasses;
use App\Models\stuFeeCategorys;
use Carbon\Carbon;
use \Crypt;

class stuFeeAmountController extends Controller
{

    //all feeCatAmount view
    public function viewFeeAmount(){

        $allData = stuFeeAmounts::select('feeCatgryId')->groupBy('feeCatgryId')->get();  //get group by data

        return view('admin_main.setup_management.fee_amount.feeAmountView',compact('allData'));
    }


    //add feeCatAmount view
    public function addFeeAmount(){

        $catgryData = stuFeeCategorys::latest()->get();
        $classData = stuClasses::all();

        return view('admin_main.setup_management.fee_amount.addFeeAmountPage',compact('catgryData','classData'));
    }



    //adding fee Cat Amount 
    public function addingFeeAmount(Request $req){

        $classCount = count($req->classId);

        if($classCount!= NULL){

            for($i=0; $i < $classCount; $i++){

                stuFeeAmounts::insert([

                    'feeCatgryId'   => $req->feeCatgryId,
                    'classId'       => $req->classId[$i],
                    'amount'        => $req->amount[$i],
                    'created_at'    => Carbon::now()
                ]);
            }
        } //end if
        $msg = array(
            'message' => "New Amount Added Successfully",
            'alert-type' => 'success'
        );
        return redirect()->route('feeAmount.view')->with($msg);
    }



    //edit feeAmount view
    public function editFeeAmount($FeeCatid){

        $dcry_id = Crypt::decrypt($FeeCatid);
        $allEditData = stuFeeAmounts::where('feeCatgryId', $dcry_id)->orderBy('classId','asc')->get();

        $catgryData = stuFeeCategorys::latest()->get();
        $classData = stuClasses::all();

        return view('admin_main.setup_management.fee_amount.editFeeAmountPage',compact('allEditData','classData','catgryData'));
    }



    //editing feeAmount 
    public function editingFeeAmount(Request $req){

        $id = $req->catID;
        
        if($req->classId == NULL){

            $msg = array(
                'message' => "Sorry you did not select any class amount",
                'alert-type' => 'error'
            );
            return redirect()->route('feeAmount.edit',Crypt::encrypt($id))->with($msg);

        }else{

            $classCount = count($req->classId);

            stuFeeAmounts::where('feeCatgryId',$id)->delete(); 
    		for ($i=0; $i <$classCount ; $i++) { 
    			$fee_amount = new stuFeeAmounts();
    			$fee_amount->feeCatgryId = $req->feeCatgryId;
    			$fee_amount->classId = $req->classId[$i];
    			$fee_amount->amount = $req->amount[$i];
    			$fee_amount->save();

    		}
        } // end else
        $msg = array(
            'message' => "Amount Updated Successfully",
            'alert-type' => 'success'
        );
        return redirect()->route('feeAmount.view')->with($msg);
    }



    //add feeCat Amount details view
    public function detailsFeeAmount($FeeCatid){

        $dcry_id = Crypt::decrypt($FeeCatid);
        $allData = stuFeeAmounts::where('feeCatgryId', $dcry_id)->orderBy('classId','asc')->get();

        return view('admin_main.setup_management.fee_amount.detailsFeeAmountPage',compact('allData'));
    }
}
