<?php

namespace App\Http\Controllers\backend_files\setup_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\examTypes;
use Carbon\Carbon;
use \Crypt;

class examTypeController extends Controller
{
    
    //all exam types view
    public function viewExamType(){

        $allData = examTypes::all();
        return view('admin_main.setup_management.exam_type.viewExamType',compact('allData'));
    }



    //add exam type view
    public function addExamType(){

        return view('admin_main.setup_management.exam_type.addExamTypePage');
    }



    //adding exam type
    public function addingExamType(Request $request){

        $exam = examTypes::where('examTypes', $request->examType)->get();

        if(sizeof($exam) > 0 ){  # check if the type is more than 1
            $msg = array(
                'message' => "This type of exam is already taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            examTypes::insert([

                'examTypes' => $request->examType,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New Type Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('examType.view')->with($msg);

        }
    }




    //edit exam type view
    public function editExamType($id){

        $dcry_id = Crypt::decrypt($id);
        $examData = examTypes::findOrFail($dcry_id);

        return view('admin_main.setup_management.exam_type.editExamTypePage',compact('examData'));
    }



    //editing exam type
    public function editingExamType(Request $request){

        $id= $request->id;

        $exam = examTypes::where('examTypes', $request->examType)->get();

        if(sizeof($exam) > 0 ){  # check if the type is more than 1
            $msg = array(
                'message' => "This type of exam is already taken",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
        
        }else{

            examTypes::where('id',$id)->update([

                'examTypes' => $request->examType,
            ]);
    
            $msg = array(
                'message' => "Data Updated Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('examType.view')->with($msg);

        }
        
    }



    //delete exam type 
    public function deleteExamType($id){

        $dcry_id = Crypt::decrypt($id);
        $Data = examTypes::findOrFail($dcry_id);
        $Data->delete();

        return redirect()->route('examType.view');
    }

}
