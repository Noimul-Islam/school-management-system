<?php

namespace App\Http\Controllers\backend_files\report_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\marksGrade;
use App\Models\studentMarks;
use App\Models\examTypes;

class marksheetReportController extends Controller
{

    //marksheet report view
    public function viewMarksheetReport(){

        $classes = stuClasses::all();
        $groups = stuGroups::all();
        $years = stuYears::orderBy('id','desc')->get();
        $examType = examTypes::all();
        return view('admin_main.reports.stu_marksheets.marksheetReportView', compact('classes','groups','years','examType'));
    }



    //student marksheet report data get
    public function getMarksheetReportData(Request $req){

        $class = $req->class_id;
        $group = $req->group_id;
        $year = $req->year_id;
        $examType = $req->examType_id;
        $id_no = $req->id_no;

        //counting failed students
        $failCount = studentMarks::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('examType_id',$examType)->where('id_no',$id_no)->where('marks','<','33')->get()->count();
        
        //get individual student data
        $individual_Stu = studentMarks::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('examType_id',$examType)->where('id_no',$id_no)->first();
        
        if($individual_Stu == true){

            $marksData = studentMarks::with(['assing_sub_relation', 'year_relation'])->where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('examType_id',$examType)->where('id_no',$id_no)->get();
            $marksGrades = marksGrade::all();

            return view('admin_main.reports.stu_marksheets.marksheetReport_show', compact('marksData','marksGrades','failCount'));
       
        }else{

            $msg = array(
                'message' => "Sorry this criteria does not match with our record",
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }
    }


    //print marksheet report 
    public function printMarksheetReport($id){

        //counting failed students
        $failCount = studentMarks::where('student_id',$id)->where('marks','<','33')->get()->count();
        

        $marksData = studentMarks::with(['assing_sub_relation', 'year_relation'])->where('student_id',$id)->get();
        //dd($marksData->toArray());
        $marksGrades = marksGrade::all();

        return view('admin_main.reports.stu_marksheets.marksheetReport_Print', compact('marksData','marksGrades','failCount'));

    }


}
