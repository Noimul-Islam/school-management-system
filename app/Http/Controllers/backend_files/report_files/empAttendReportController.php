<?php

namespace App\Http\Controllers\backend_files\report_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\empAttentdance;
use PDF;

class empAttendReportController extends Controller
{
    
    
    //employee attandence report view
    public function viewEmpAttendReport(){

        $empData = allUsers::where('userType','Employee')->get();

        return view('admin_main.reports.attendence_reports.empAttendReportView',compact('empData'));
    }


    //employee attandence data get
    public function getEmpAttendData(Request $req){

        $emp_id = $req->emp_id;
        if($emp_id != ''){

            $where[] = ['employee_id', $emp_id];
        }else{
            $msg = array(
                'message' => "Please select Employee",
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }


        $date = date('Y-m', strtotime($req->date));
        if($date != ''){

            $where[] = ['date','like',$date.'%'];
        }else{
            $msg = array(
                'message' => "Please select month",
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }

        $individualAttendance = empAttentdance::with(['userrelation'])->where($where)->get();
        if($individualAttendance == true){

            $data['allData'] = empAttentdance::with(['userrelation'])->where($where)->get();
            //counting Leaves
            $data['leaves'] = empAttentdance::with(['userrelation'])->where($where)->where('attentdance_status','Leave')->get()->count();
            //counting Absents
            $data['absents'] = empAttentdance::with(['userrelation'])->where($where)->where('attentdance_status','Absent')->get()->count();
            $data['month'] = $req->date;

            $pdf = Pdf::loadView('admin_main.reports.attendence_reports.empAttendReportPdf',$data);
            return $pdf->stream('employee_attendance_report.pdf');
        }else{

            $msg = array(
                'message' => "Sorry this criteria does not match with our record",
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }
    }
}
