<?php

namespace App\Http\Controllers\backend_files\report_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\marksGrade;
use App\Models\studentMarks;
use App\Models\examTypes;
use App\Models\AssignStudent;
use PDF;

class stuResultReportController extends Controller
{
    
    // students result repot view
    public function viewStuResultReport(){

        $classes = stuClasses::all();
        $groups = stuGroups::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();
        $examType = examTypes::all();
        return view('admin_main.reports.stu_results.stuResultReportView', compact('classes','groups','years','examType'));
    }



    //student results report data get
    public function getStuResultData(Request $req){

        $class = $req->class_id;
        $group = $req->group_id;
        $year = $req->year_id;
        $examType = $req->examType_id;

        
        $single_result = studentMarks::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('examType_id',$examType)->first();
        
        if($single_result == true){

            $data['marksData'] = studentMarks::select('student_id','class_id','group_id','year_id','examType_id')->where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('examType_id',$examType)->groupBy('student_id')->groupBy('class_id')->groupBy('group_id')->groupBy('year_id')->groupBy('examType_id')->get();
           
            return view('admin_main.reports.stu_results.stuResultShow', $data);
           
        }else{

            $msg = array(
                'message' => "Sorry this criteria does not match with our record",
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }
    }




    //student result print
    public function printResultsReport($classID, $yearId, $groupId, $examId){

        $data['marksData'] = studentMarks::select('student_id','class_id','group_id','year_id','examType_id')->where('class_id',$classID)->where('group_id',$groupId)->where('year_id',$yearId)->where('examType_id',$examId)->groupBy('student_id')->groupBy('class_id')->groupBy('group_id')->groupBy('year_id')->groupBy('examType_id')->get();
        return view('admin_main.reports.stu_results.stuResultReportPrint',$data);
    }






                        //Student ID card generator controller//





    //student id card generate
    public function viewStuIdCard(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();
        
        return view('admin_main.reports.stu_idCard.stuIdCardView', compact('classes','years'));

    }




    //student Id card data get
    public function getStuIdCardData(Request $req){

        $class = $req->class_id;
        $group = $req->group_id;
        $year = $req->year_id;

        $dataCheck = AssignStudent::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->first();

        if($dataCheck == true){

            $data['allData'] = AssignStudent::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->get();
            //dd($data['allData']->toArray());
            return view('admin_main.reports.stu_idCard.showStuIdCard',$data);
        }else{

            $msg = array(
                'message' => "Sorry this criteria does not match with our record",
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }
    }


    //student Id card print
    public function idCardPrint($classID, $yearId, $groupId){

        $allData = AssignStudent::where('class_id',$classID)->where('year_id',$yearId)->where('group_id',$groupId)->get();
        return view('admin_main.reports.stu_idCard.stuIdCardPrint',compact('allData'));
    }
}
