<?php

namespace App\Http\Controllers\backend_files\report_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\otherCostAccount;
use App\Models\empSalaryAccount;
use App\Models\stuFeeAccount;
use \Crypt;
use PDF;
use DB;

class profitReportController extends Controller
{
    
    //monthly profite view
    public function viewMonthlyProfit(){

        return view('admin_main.reports.school_profits.profitViewPage');
    }


    //get profit report data
    public function getProfitReportData(Request $req){

        
        $start_date = date('Y-m',strtotime($req->start_date));
		$end_date = date('Y-m',strtotime($req->end_date));

        //another (Start date and End date) variable with [Date] for other cost account
    	$S_date = date('Y-m-d',strtotime($req->start_date));
    	$E_date = date('Y-m-d',strtotime($req->end_date));
    	 
    	$stuFees = stuFeeAccount::whereBetween('date',[$start_date,$end_date])->sum('amount');

    	$otherCost = otherCostAccount::whereBetween('date',[$S_date,$E_date])->sum('amount'); 

    	$empSalaries = empSalaryAccount::whereBetween('date',[$start_date,$end_date])->sum('amount');

        $totalCost = $otherCost+$empSalaries;
    	$profit = $stuFees-$totalCost;
        $color = 'info';
    	

    	$html['thsource']  = '<th>Student Fees</th>';
        $html['thsource'] .= '<th>Employee Salary</th>';
    	$html['thsource'] .= '<th>Other Cost</th>';
    	$html['thsource'] .= '<th>Total Cost</th>';
        if($profit < $totalCost){
    	$html['thsource'] .= '<th>Loss Amount</th>';
        }else{
        $html['thsource'] .= '<th>Profit Amount</th>';
        }
    	$html['thsource'] .= '<th>Details</th>';
	
    	$html['tdsource']   = '<td>'.$stuFees.'</td>';
        $html['tdsource']  .= '<td>'.$empSalaries.'</td>';
    	$html['tdsource']  .= '<td>'.$otherCost.'</td>';
    	$html['tdsource']  .= '<td>'.$totalCost.'</td>';
        $html['tdsource']  .= '<td>'.$profit.'</td>';   
    	$html['tdsource'] .='<td>';
    	$html['tdsource'] .='<a class="btn btn-sm btn-'.$color.'" title="PDF File" target="_blanks" href="'.route("ProfitReportPdf.details").'?start_date='.$S_date.'&end_date='.$E_date.'">PDF Slip</a>';
    	$html['tdsource'] .= '</td>';

    	
    	return response()->json(@$html);
    }



    //profit details pdf view
    public function detailsProfitReport(Request $req){

        $data['start_date'] = date('Y-m',strtotime($req->start_date));
		$data['end_date'] = date('Y-m',strtotime($req->end_date));

    	$data['S_date'] = date('Y-m-d',strtotime($req->start_date));
    	$data['E_date'] = date('Y-m-d',strtotime($req->end_date));

        $pdf = Pdf::loadView('admin_main.reports.school_profits.profitDetailsPdf',$data);
        return $pdf->stream('school_profit_report.pdf');
    }
}
