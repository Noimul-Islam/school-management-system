<?php

namespace App\Http\Controllers\backend_files\employee_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\allUsers;
use App\Models\empLeavePerpose;
use App\Models\employeeLeave;
use Carbon\Carbon;
use \Crypt;
use PDF;

class empLeaveController extends Controller
{
    //employee leave view
    public function viewEmpLeave(){

        $allData= employeeLeave::orderBy('id','desc')->get();
        return view('admin_main.employee_management.emp_leave.viewEmployeeLeaves',compact('allData'));

    }


    //add employee leave view
    public function addEmpLeave(){ 

        $empData= allUsers::where('userType','Employee')->get();
        $purposes = empLeavePerpose::orderBy('id','desc')->get();
        return view('admin_main.employee_management.emp_leave.addEmpLeavePage',compact('empData','purposes'));

    }


    //adding employee leave
    public function addingEmpLeave(Request $req){

        if ($req->leavePurpose_id == "0") {

            $leavepurpose = new empLeavePerpose();
    		$leavepurpose->perposes = $req->add_purpose;
    		$leavepurpose->save();
    		$leavePurpose_id = $leavepurpose->id;
    	}else{
            $leavePurpose_id = $req->leavePurpose_id;        
        }

            $data = new employeeLeave();
            $data->employee_id = $req->employee_id;
            $data->leavePurpose_id = $leavePurpose_id;
            $data->leaveReason = $req->leaveReason;
            $data->start_date = date('Y-m-d',strtotime($req->start_date));
            $data->end_date = date('Y-m-d',strtotime($req->end_date));
            $data->save();

    	$msg = array(
    		'message' => 'Employee Leave Data Inserted Successfully',
    		'alert-type' => 'success'
    	);

    	return redirect()->route('empLeave.view')->with($msg);
    }



    //emp leave edit view
    public function editEmpLeave($id){

        $dcry_id = Crypt::decrypt($id);
    	$editableData = employeeLeave::find($dcry_id);
        //dd($editableData->toArray());
    	$empData = allUsers::where('userType','Employee')->get();
    	$purposes = empLeavePerpose::all();
    	return view('admin_main.employee_management.emp_leave.editEmpLeavePurposePage',compact('editableData','empData','purposes'));

    }




    //update emp leave
    public function editingEmpLeave(Request $req){

        $id = $req->id;
    	if ($req->leavePurpose_id == "0") {

            $leavepurpose = new empLeavePerpose();
    		$leavepurpose->perposes = $req->add_purpose;
    		$leavepurpose->save();
    		$leavePurpose_id = $leavepurpose->id;
    	}else{
            $leavePurpose_id = $req->leavePurpose_id;       
        }

            $data = employeeLeave::find($id);
            $data->employee_id = $req->employee_id;
            $data->leavePurpose_id = $leavePurpose_id;
            $data->leaveReason = $req->leaveReason;
            $data->start_date = date('Y-m-d',strtotime($req->start_date));
            $data->end_date = date('Y-m-d',strtotime($req->end_date));
            $data->save();

    	$msg = array(
    		'message' => 'Employee Leave Data Updated Successfully',
    		'alert-type' => 'success'
    	);

    	return redirect()->route('empLeave.view')->with($msg);

    }




    //delete emp leave 
    public function deleteEmpLeave($id){

        $dcry_id = Crypt::decrypt($id);
        $Data = employeeLeave::findOrFail($dcry_id);
        $Data->delete();

        return redirect()->route('empLeave.view');
    }


}
