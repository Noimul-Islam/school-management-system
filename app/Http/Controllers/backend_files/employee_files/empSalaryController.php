<?php

namespace App\Http\Controllers\backend_files\employee_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\dasignation;
use App\Models\employeeSalaryLog;
use Carbon\Carbon;
use \Crypt;

class empSalaryController extends Controller
{
    
    //view employee salary
    public function viewEmpSalarys(){

        $allSalary = allUsers::where('userType', 'Employee')->get();

        return view('admin_main.employee_management.emp_salary.viewEmpSalary',compact('allSalary'));
    }


    //view employee salary increment page
    public function empSalaryIncrement($empId){

        $dcry_id = Crypt::decrypt($empId);
        $editData = allUsers::find($dcry_id);
        return view('admin_main.employee_management.emp_salary.empSalaryIncrementPage',compact('editData'));
    }


    //employee salary incrementing
    public function salaryIncrementing(Request $req){

        $empId = $req->id;
        $dcry_id = Crypt::decrypt($empId);

        $userData = allUsers::find($dcry_id);
        $oldSalary = $userData->salary;
        $newSalary = (float)$oldSalary + (float)$req->increment_salary;
        $userData->salary = $newSalary;
        $userData->save();

        // employeeSalaryLog::where('employee_id',$dcry_id)->update([

        //     'previous_salary' => $oldSalary,
        //     'present_salary' => $newSalary,
        //     'increment_salary' => $req->increment_salary,
        //     'effected_salary' => date('Y-m-d',strtotime($req->effected_salary))

        // ]);

        $salaryData = new employeeSalaryLog();
        $salaryData->employee_id = $dcry_id;
        $salaryData->previous_salary = $oldSalary;
        $salaryData->present_salary = $newSalary;
        $salaryData->increment_salary = $req->increment_salary;
        $salaryData->effected_salary = $req->effected_salary;
        $salaryData->save();

        $msg= array(                        
            'message' => 'Employee Salary Incremented successfully',
            'alert-type' => 'success' 
            );
    
            return redirect()->route('empSalarys.view')->with($msg);


    }



    //employee salary ditails salary history
    public function detailsSalaryHistory($empId){

        $dcry_id = Crypt::decrypt($empId);

        $userData = allUsers::find($dcry_id);
        $salaryData = employeeSalaryLog::where('employee_id', $userData->id)->get();
        
        return view('admin_main.employee_management.emp_salary.empSalaryHistoryPage',compact('salaryData','userData'));
    }

}
