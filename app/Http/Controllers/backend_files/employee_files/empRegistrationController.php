<?php

namespace App\Http\Controllers\backend_files\employee_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;
//use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\dasignation;
use App\Models\emp_qualification;
use App\Models\employeeSalaryLog;
use Carbon\Carbon;
use \Crypt;
use Image;
use PDF;
use DB;

class empRegistrationController extends Controller
{
    
    //employee view page
    public function viewRegisterEmployees(){

        $allUserData = allUsers::where('userType','Employee')->get();
        return view('admin_main.employee_management.emp_register.viewEmployees',compact('allUserData'));
    }


    //add employee view page
    public function registerEmployee(){

        $dasignationData = dasignation::all();
        $qualification = emp_qualification::all();
        return view('admin_main.employee_management.emp_register.addEmployee',compact('dasignationData','qualification'));
    }


    //adding new employee 
    public function addingEmployee(Request $req){

        $email = allUsers::where('email', $req->email)->get();
        $qualifi_name = emp_qualification::where('name', $req->add_new)->get();

        # check if email is more than 1
        if(sizeof($email) > 0){ 
            
            $msg = array(
                'message' => "This email is already exist",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);

        }elseif(sizeof($qualifi_name) > 0){ 
            
            $msg = array(
                'message' => "This Qualification is Already Exist",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);
    
        }else{

            DB::transaction(function() use($req){

            $chkYear = date('Ym',strtotime($req->join_date));
            $employee = allUsers::where('userType','Employee')->orderBy('id','DESC')->first();

            if($employee == null){    //if there are no employee data into the database then

                $fstReg = 0;
                $empId = $fstReg+1;

                if($empId < 10){

                    $id_no = '000'.$empId;

                }elseif($empId < 100){

                    $id_no = '00'.$empId;

                }elseif($empId < 1000){

                    $id_no = '0'.$empId;
                }
            }else{

                $employee = allUsers::where('userType','Employee')->orderBy('id','DESC')->first()->id;

                $empId = $employee+1;

                if($empId < 10){

                    $id_no = '000'.$empId;

                }elseif($empId < 100){

                    $id_no = '00'.$empId;

                }elseif($empId < 1000){

                    $id_no = '0'.$empId;
                }
            }//end else

                $finalId = $chkYear.$id_no; //final unique id like 20221
                $code= rand(100000, 999999);

                if ($req->qualificationId == "0") {

                    $qualification = new emp_qualification();
                    $qualification->name = $req->add_new;
                    $qualification->save();
                    $qualificationId = $qualification->id;
                }else{
                    $qualificationId = $req->qualificationId;       
                }

                //allusers table data insert
                $userData = new allUsers();
                $userData->name = $req->name;
                $userData->email = $req->email;
                $userData->userType = 'Employee';
                $userData->password = Hash::make($code);
                $userData->gender = $req->gender;
                $userData->address = $req->address;
                $userData->phone = $req->phone;
                $userData->fatherName = $req->fatherName;
                $userData->motherName = $req->motherName;
                $userData->religion = $req->religion;
                $userData->id_no = $finalId;
                $userData->qualificationId = $qualificationId;
                $userData->designationId = $req->designationId;
                $userData->salary = $req->salary;
                $userData->dob = date('Y-m-d',strtotime($req->dob));
                $userData->join_date = date('Y-m-d',strtotime($req->join_date));
                $userData->code = $code;

                if($req->file('profile_photo')){
                    $image= $req->file('profile_photo');
                    $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                    Image::make($image)->resize(128,128)->save('Upload/employeeImage/'.$imageName);
                    $stor_url= 'Upload/employeeImage/'.$imageName;
                    $userData->profile_photo = $stor_url;
                }//end if
                $userData->save();

                //employee salary log table data insert
                $empSalary = new employeeSalaryLog();
                $empSalary->employee_id = $userData->id;
                $empSalary->previous_salary = $req->salary;
                $empSalary->present_salary = $req->salary;
                $empSalary->increment_salary = '0';
                $empSalary->effected_salary = date('Y-m-d',strtotime($req->join_date));
                $empSalary->save();
                

            });//end DB method

            $msg= array(                        
                'message' => 'New Employee Registred successfully',
                'alert-type' => 'success' 
            );

            return redirect()->route('employees.view')->with($msg);
        }
    }



    //edit employee page view
    public function editEmployee($empId){

        $dcry_id = Crypt::decrypt($empId);
        $dasignationData = dasignation::all();
        $qualification = emp_qualification::all();
        $editEmpData = allUsers::findorFail($dcry_id);

        return view('admin_main.employee_management.emp_register.editEmployeePage',compact('editEmpData','dasignationData','qualification'));

    }


    //editing employee
    public function editingEmployee(Request $req){

        $empid = $req->id;
        
        if ($req->qualificationId == "0") {

            $qualification = new emp_qualification();
            $qualification->name = $req->add_new;
            $qualification->save();
            $qualificationId = $qualification->id;
        }else{
            $qualificationId = $req->qualificationId;       
        }

        $userData = allUsers::find($empid);
        $userData->name = $req->name;
        $userData->email = $req->email;
        $userData->gender = $req->gender;
        $userData->address = $req->address;
        $userData->phone = $req->phone;
        $userData->fatherName = $req->fatherName;
        $userData->motherName = $req->motherName;
        $userData->religion = $req->religion;
        $userData->qualificationId = $qualificationId;
        $userData->designationId = $req->designationId;
        $userData->dob = date('Y-m-d',strtotime($req->dob));

        if($req->file('profile_photo')){
            $image= $req->file('profile_photo');
            if($userData->profile_photo){
                unlink($userData->profile_photo);
            }
            $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(128,128)->save('Upload/employeeImage/'.$imageName);
            $stor_url= 'Upload/employeeImage/'.$imageName;
            $userData->profile_photo = $stor_url;
        }

        $userData->save();

        $msg= array(                        
        'message' => 'Employee Data Updated successfully',
        'alert-type' => 'success' 
        );

        return redirect()->route('employees.view')->with($msg);
        
    }




    //employee pdf details view
    public function detailsEmployee($empId){

        $dcry_id = Crypt::decrypt($empId);

        $data['details'] = allUsers::find($dcry_id);

        $pdf = Pdf::loadView('admin_main.employee_management.emp_register.detailsEmployeePDF',$data);
        return $pdf->stream('employee_details.pdf');
    }

    
}
