<?php

namespace App\Http\Controllers\backend_files\employee_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\empLeavePerpose;
use App\Models\employeeLeave;
use App\Models\empAttentdance;
use Carbon\Carbon;
use \Crypt;


class empAttentdanceController extends Controller
{
    
    
    //employee attendance view
    public function viewEmpAttendance(){
        $attentData = empAttentdance::select('year')->groupBy('year')->orderBy('year','desc')->get();  //get group by data
        return view('admin_main.employee_management.emp_attentdance.viewEmpAttentdeance',compact('attentData'));

    }


    //employee attendance date wise view
    public function viewAttendanceDate($year){

        $dcry_id = Crypt::decrypt($year);
        $attentData = empAttentdance::select('date')->where('year',$dcry_id)->groupBy('date')->orderBy('id','desc')->get();
        //dd($attentData->toArray());
        return view('admin_main.employee_management.emp_attentdance.viewAttendanceDate',compact('attentData'));
    }


    //add employee attendance view
    public function addEmpAttendance(){

        $empData= allUsers::where('userType','Employee')->get();
        return view('admin_main.employee_management.emp_attentdance.addEmpAttentdeance',compact('empData'));

    }


    //adding employee attendance
    public function addingEmpAttendance(Request $req){
        empAttentdance::where('date', date('Y-m-d', strtotime($req->att_date)))->delete();
        $empCount= count($req->employee_id);
        
        for($i=0; $i < $empCount; $i++){

            $attendStatus = 'attentdance_status'.$i;
            empAttentdance::insert([

                'employee_id' => $req->employee_id[$i],        
                'attentdance_status' => $req->$attendStatus,
                'date' => date('Y-m-d',strtotime($req->att_date)),
                'year' => date('Y',strtotime($req->att_date))
            ]);
        }//end for 
        $msg = array(
    		'message' => 'Employee Attendance Stored Successfully',
    		'alert-type' => 'success'
    	);

    	return redirect()->route('empAttendance.view')->with($msg);

    }




    //edit employee attendance view
    public function editEmpAttendance($date){

        $dcry_id = Crypt::decrypt($date);
        $editableData = empAttentdance::where('date',$dcry_id)->get();
        //$empData= allUsers::where('userType','Employee')->get();
        return view('admin_main.employee_management.emp_attentdance.editEmpAttentdeance',compact('editableData'));

    }
  

    //details employee attendance view
    public function detailsEmpAttendance($date){

        $dcry_id = Crypt::decrypt($date);
        $detailData = empAttentdance::where('date',$dcry_id)->get();
        return view('admin_main.employee_management.emp_attentdance.detailsEmpAttentdeance',compact('detailData'));

    }

}
