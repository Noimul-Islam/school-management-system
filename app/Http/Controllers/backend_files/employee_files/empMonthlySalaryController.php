<?php

namespace App\Http\Controllers\backend_files\employee_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\allUsers;
use App\Models\dasignation;
use App\Models\employeeSalaryLog;
use App\Models\empAttentdance;
use Carbon\Carbon;
use \Crypt;
use PDF;
use DB;

class empMonthlySalaryController extends Controller
{


    //employee monthly salary view
    public function viewMonthlySalary(){

        return view('admin_main.employee_management.emp_month_salary.viewEmpMonthSalary');
    }



    //employee monthly salary get data
    public function getMonthlySalaryData(Request $req){

        $date = date('Y-m',strtotime($req->date));
    	

    	if ($date !='') {
    	 	$where[] = ['date','like',$date.'%'];
    	}
    	 $allData = empAttentdance::select('employee_id')->groupBy('employee_id')->with(['userrelation'])->where($where)->get();
    	 // dd($allData);
    	 $html['thsource']  = '<th>SL</th>';
    	 $html['thsource'] .= '<th>ID No</th>';
    	 $html['thsource'] .= '<th>Employee Name</th>';
         $html['thsource'] .= '<th>Designation</th>';
    	 $html['thsource'] .= '<th>Basic Salary</th>';
         $html['thsource'] .= '<th>Absent</th>';
		 $html['thsource'] .= '<th>Salary for this Month</th>';
    	 $html['thsource'] .= '<th>Action</th>';


    	foreach($allData as $key => $vlu) {
    	 	$totalAttend = empAttentdance::with(['userrelation'])->where($where)->where('employee_id',$vlu->employee_id)->get();
			//dd($totalAttend);
            $countAbsent = count($totalAttend->where('attentdance_status','Absent'));

    	 	$color = 'info';
    	 	$html[$key]['tdsource']  = '<td>'.($key+1).'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['id_no'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['name'].'</td>';
            $html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['designationRel']['dasignation'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['salary'].'/='.'</td>';
            if($countAbsent >1){
            $html[$key]['tdsource'] .='<td style="color:#e04554f7;">'.'<b>'.$countAbsent.' Days'.'</b>'.'</td>';
            }elseif($countAbsent == 0){
            $html[$key]['tdsource'] .='<td style="color:#28a745c7;">'.'<b>'.$countAbsent.' Day'.'</b>'.'</td>';
            }else{
            $html[$key]['tdsource'] .='<td style="color:#e04554f7;">'.'<b>'.$countAbsent.' Day'.'</b>'.'</td>';
            }
            
    	 	
    	 	$originalSalary = (float)$vlu['userrelation']['salary']; //10,000/= 
    	 	$amountPerDay = (float)$originalSalary/30;             //(10,000/30) = 334/=
    	 	$minusAmount = (float)$amountPerDay*(float)$countAbsent; //(334*4) = 1336/=
    	 	$finalSalary = (float)$originalSalary-(float)$minusAmount; //(10,000-1336) = 8,664/=

    	 	$html[$key]['tdsource'] .='<td>'.(int)$finalSalary.'/='.'</td>';
    	 	$html[$key]['tdsource'] .='<td>';
    	 	$html[$key]['tdsource'] .='<a class="btn btn-sm btn-'.$color.'" title="PaySlip" target="_blanks" href="'.route("paySlipMonthlySalary.data",[Crypt::encrypt($vlu->employee_id), Crypt::encrypt(date('Y-m',strtotime($req->date)))]).'">Details Slip</a>';
    	 	$html[$key]['tdsource'] .= '</td>';

    	}  
    	return response()->json(@$html);
    }


    //monthly salary slip
    public function getMonthlySalarySlip(Request $req, $emId, $dateId){

        $dcry_eid = Crypt::decrypt($emId);
		$dcry_date = Crypt::decrypt($dateId);
        
        $id = empAttentdance::where('employee_id',$dcry_eid)->first();
        $date = $dcry_date;
    	if ($date !='') {
    	 	$where[] = ['date','like',$date.'%'];
    	}

        $data['details'] = empAttentdance::with(['userrelation'])->where($where)->where('employee_id',$id->employee_id)->get();
		//dd($data['details']->toArray());
        $pdf = Pdf::loadView('admin_main.employee_management.emp_month_salary.paySlipDetailsPDF',$data);
        return $pdf->stream('employee_monthly_fee_slip.pdf');

    }
}
