<?php

namespace App\Http\Controllers\backend_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\subsAssign;
use App\Models\examTypes;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;
use App\Models\studentMarks;

class defaultController extends Controller
{

    //geting subjects data by class in javascript
    public function getSubjectData(Request $req){

        $class_id = $req->class_id;
        
        $allSubData = subsAssign::with(['subject_relation'])->where('classId',$class_id)->get();
        
        return response()->json($allSubData);
    
    }

    //geting groups data by class in javascript
    public function getGroupData(Request $req){

        $class_id = $req->class_id;
        
        $allGrupData = AssignStudent::with(['group_relation'])->select('group_id')->where('class_id',$class_id)->groupBy('group_id')->get();
        return response()->json($allGrupData);
        
    }


    

    //geting student data for Marks entry by javascript
    public function getStudentsData(Request $req){

        $allMarks = studentMarks::with(['userrelation'])->where('class_id',$req->class_id)->where('year_id',$req->year_id)->where('group_id',$req->group_id)->where('examType_id',$req->examType_id)->where('assign_sub_id',$req->assign_sub_id)->get();
        
        if(sizeof($allMarks) > 0){
            
            return response()->json($allMarks);
        }else{
            
            $allStuData = AssignStudent::with(['userrelation'])->where('year_id',$req->year_id)->where('class_id',$req->class_id)->where('group_id',$req->group_id)->get();
            
            return response()->json($allStuData);
        }
        
    }






















    // //geting student data by javascript
    // public function getStudentsData(Request $req){

    //     $allStuData = AssignStudent::with(['userrelation'])->where('year_id',$req->year_id)->where('class_id',$req->class_id)->where('group_id',$req->group_id)->get();
    //     //dd($allStuData->toArray());
    //     return response()->json($allStuData); //data geting in json format
    // }
}
