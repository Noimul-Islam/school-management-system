<?php

namespace App\Http\Controllers\backend_files\accounts_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\stuFeeAmounts;
use App\Models\stuFeeCategorys;
use App\Models\stuFeeAccount;
use App\Models\StuDiscount;
use App\Models\examTypes;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;
use Carbon\Carbon;
use \Crypt;
use PDF;

class stuFeeController extends Controller
{
    
    //student fee category wise view
    public function viewStudentFee(){

        $allStuFee = stuFeeAccount::select('feeCatgry_id','year_id')->groupBy('feeCatgry_id')->groupBy('year_id')->orderBy('id','desc')->get();
        return view('admin_main.accounts.stu_fee.stuFeeAccountView',compact('allStuFee'));
    }


    //students fee class wise view
    public function viewClassList($feeCat, $year){

        $classData = stuFeeAccount::select('class_id','group_id','feeCatgry_id','year_id')->where('feeCatgry_id',$feeCat)->where('year_id',$year)->groupBy('class_id')->groupBy('group_id')->groupBy('feeCatgry_id')->groupBy('year_id')->get();
        return view('admin_main.accounts.stu_fee.stuClassView',compact('classData'));
    }

    //students fee list
    public function viewStuFeeDetailsList($feeCat, $year, $class, $group){

        $allStuFee = stuFeeAccount::where('feeCatgry_id',$feeCat)->where('year_id',$year)->where('class_id',$class)->where('group_id',$group)->orderBy('id','desc')->get();
        return view('admin_main.accounts.stu_fee.stuFeeFinalList',compact('allStuFee'));
    }



    //add  student fee account view
    public function addStudentFee(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();
        $examType = examTypes::all();
        $feeCatgrys = stuFeeCategorys::all();


        return view('admin_main.accounts.stu_fee.addStuFeeAccountPage',compact('classes','years','examType','feeCatgrys'));
    }



    //get  student fee data
    public function getStudentFeeData(Request $req){

        $year_id = $req->year_id;
        $class_id = $req->class_id;
        $feeCatgry_id = $req->feeCatgry_id;
        $group_id = $req->group_id;
        $date = date('Y-m',strtotime($req->date));    	   
    	 
        $stuData = AssignStudent::with(['discountRelation'])->where('year_id',$year_id)->where('class_id',$class_id)->where('group_id',$group_id)->get();
    	 
    	 $html['thsource']  = '<th>ID No</th>';
    	 $html['thsource'] .= '<th>Student Name</th>';
    	 $html['thsource'] .= '<th>Father Name</th>';
    	 $html['thsource'] .= '<th>Original Fee </th>';
      	 $html['thsource'] .= '<th>Discount Amount</th>';
      	 $html['thsource'] .= '<th>Fee (This Student)</th>';
      	 $html['thsource'] .= '<th>Select</th>';

    	 foreach ($stuData as $key => $std) {
            $feeAmount = stuFeeAmounts::where('feeCatgryId',$feeCatgry_id)->where('classId',$std->class_id)->first();

            $stuFeesAcc = stuFeeAccount::where('student_id',$std->student_id)->where('group_id',$std->group_id)->where('year_id',$std->year_id)->where('class_id',$std->class_id)->where('feeCatgry_id',$feeCatgry_id)->where('date',$date)->first();

            if($stuFeesAcc !=null) {
                $checked = 'checked';
            }else{
                $checked = '';
            }  	 	 
                $color = 'success';
                $html[$key]['tdsource']  = '<td>'.$std['userrelation']['id_no']. '<input type="hidden" name="feeCatgry_id" value= " '.$feeCatgry_id.' " >'.'</td>';

                $html[$key]['tdsource']  .= '<td>'.$std['userrelation']['name']. '<input type="hidden" name="year_id" value= " '.$std->year_id.' " >'.'</td>';

                $html[$key]['tdsource']  .= '<td>'.$std['userrelation']['fatherName']. '<input type="hidden" name="class_id" value= " '.$std->class_id.' " >'.'</td>';

                $html[$key]['tdsource']  .= '<td>'.$feeAmount->amount.'/='.'<input type="hidden" name="date" value= " '.$date.' " >'.'</td>';

                $html[$key]['tdsource'] .= '<td>'.$std['discountRelation']['discount'].'%'.'<input type="hidden" name="group_id" value= " '.$std->group_id.' " >'.'</td>';
            
                $orginalfee = $feeAmount->amount;
                $discount = $std['discountRelation']['discount'];
                $discountablefee = $discount/100*$orginalfee;
                $finalfee = (int)$orginalfee-(int)$discountablefee;    	 	 

                $html[$key]['tdsource'] .='<td>'. '<input type="text" name="amount[]" value="'.$finalfee.' " class="form-control"'.'</td>';
                
                $html[$key]['tdsource'] .='<td>'.'<input type="hidden" name="student_id[]" value="'.$std->student_id.'">'.'<input type="checkbox" name="checkmanage[]" id="'.$key.'" value="'.$key.'" '.$checked.' style="transform: scale(1.5);margin-left: 10px;"> <label for="'.$key.'"> </label> '.'</td>'; 

        }  
    	return response()->json(@$html);

        
    }


    //adding student fee account
    public function addingStudentFee(Request $req){

        $date = date('Y-m',strtotime($req->date));

        if($req->student_id !=null){

            stuFeeAccount::where('year_id',$req->year_id)->where('class_id',$req->class_id)->where('group_id',$req->group_id)->where('feeCatgry_id',$req->feeCatgry_id)->where('date',$req->date)->delete();

            $checkdata = $req->checkmanage;

            if ($checkdata !=null) {
                for ($i=0; $i <count($checkdata) ; $i++) { 
                    $data = new stuFeeAccount();
                    $data->class_id = $req->class_id;
                    $data->group_id = $req->group_id;
                    $data->year_id = $req->year_id;			
                    $data->date = $date;
                    $data->feeCatgry_id = $req->feeCatgry_id;
                    $data->student_id = $req->student_id[$checkdata[$i]];
                    $data->amount = $req->amount[$checkdata[$i]];
                    $data->save();
                }
            } 

            //if (!empty(@$data) || empty($checkdata)) {

            $msg = array(
                'message' => 'Data Stored Successfully',
                'alert-type' => 'success'
            );

            return redirect()->route('studentFeeList.view',[$req->feeCatgry_id, $req->year_id, $req->class_id, $req->group_id])->with($msg);
    	}else{

    		$msg = array(
    		'message' => 'Sorry there are no student data visible',
    		'alert-type' => 'error'
    	    );

    	    return redirect()->back()->with($msg);

    	} 
        
    }


    public function getMonthlyFeeSlip($id){

        $data['details'] = stuFeeAccount::with(['userrelation'])->where('id', $id)->first();
        //dd($data['details']->toArray());

        $pdf = Pdf::loadView('admin_main.accounts.stu_fee.monthlyPaidSlip',$data);
        return $pdf->stream('paid_monthly_fee_slip.pdf');

    }

    public function getExamFeeSlip($id){

        $data['details'] = stuFeeAccount::with(['userrelation'])->where('id', $id)->first();

        $pdf = Pdf::loadView('admin_main.accounts.stu_fee.examFeePaidslip',$data);
        return $pdf->stream('paid_exam_fee_slip.pdf');

    }

}
