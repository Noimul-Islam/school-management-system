<?php

namespace App\Http\Controllers\backend_files\accounts_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\dasignation;
use App\Models\employeeSalaryLog;
use App\Models\empSalaryAccount;
use App\Models\empAttentdance;
use Carbon\Carbon;
use \Crypt;
use PDF;
use DB;


class empSalaryPaymentController extends Controller
{
    
    //all employee paid salary view
    public function viewEmpSalary(){

        $allEmpSalary= empSalaryAccount::orderBy('id','desc')->get();
        return view('admin_main.accounts.emp_salary.empSalaryAccountView',compact('allEmpSalary'));

    }


    //add employee salary payment view
    public function addEmpSalary(){

        return view('admin_main.accounts.emp_salary.addEmpSalaryAccountPage');
        
    }


    //get employee salary data
    public function getEmpSalaryData(Request $req){ 

        $date = date('Y-m',strtotime($req->date));
    	

    	if ($date !='') {
    	 	$where[] = ['date','like',$date.'%'];
    	}
    	 $allData = empAttentdance::select('employee_id')->groupBy('employee_id')->with(['userrelation'])->where($where)->get();
    	 // dd($allData);
    	 $html['thsource']  = '<th>SL</th>';
    	 $html['thsource'] .= '<th>ID No</th>';
    	 $html['thsource'] .= '<th>Employee Name</th>';
         $html['thsource'] .= '<th>Designation</th>';
    	 $html['thsource'] .= '<th>Basic Salary</th>';
         $html['thsource'] .= '<th>Absent</th>';
		 $html['thsource'] .= '<th>Salary for this Month</th>';
    	 $html['thsource'] .= '<th>Select for Paid</th>';


    	foreach($allData as $key => $vlu) {

            $salaryAccount = empSalaryAccount::where('employee_id',$vlu->employee_id)->where('date',$date)->first();

    	 	if($salaryAccount !=null) {
			 	$checked = 'checked';
			 }else{
			 	$checked = '';
			 }

    	 	$totalAttend = empAttentdance::with(['userrelation'])->where($where)->where('employee_id',$vlu->employee_id)->get();
			//dd($totalAttend);
            $countAbsent = count($totalAttend->where('attentdance_status','Absent'));

    	 	$color = 'info';
    	 	$html[$key]['tdsource']  = '<td>'.($key+1).'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['id_no'].'<input type="hidden" name="date" value="'.$date.'" >'.'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['name'].'</td>';
            $html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['designationRel']['dasignation'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$vlu['userrelation']['salary'].'/='.'</td>';
            if($countAbsent >1){
            $html[$key]['tdsource'] .='<td style="color:#e04554f7;">'.'<b>'.$countAbsent.' Days'.'</b>'.'</td>';
            }elseif($countAbsent == 0){
            $html[$key]['tdsource'] .='<td style="color:#28a745c7;">'.'<b>'.$countAbsent.' Day'.'</b>'.'</td>';
            }else{
            $html[$key]['tdsource'] .='<td style="color:#e04554f7;">'.'<b>'.$countAbsent.' Day'.'</b>'.'</td>';
            }
            
    	 	
    	 	$originalSalary = (float)$vlu['userrelation']['salary']; //10,000/= 
    	 	$amountPerDay = (float)$originalSalary/30;             //(10,000/26) = 384/=
    	 	$minusAmount = (float)$amountPerDay*(float)$countAbsent; //(334*4) = 1538/=
    	 	$finalSalary = (float)$originalSalary-(float)$minusAmount; //(10,000-1538) = 8,462/=

            $html[$key]['tdsource'] .='<td>'.(int)$finalSalary.'/='.'<input type="hidden" name="amount[]" value="'.(int)$finalSalary.'" >'.'</td>';
            $html[$key]['tdsource'] .='<td>'.'<input type="hidden" name="employee_id[]" value="'.$vlu->employee_id.'">'.'<input type="checkbox" name="checkmanage[]" id="'.$key.'" value="'.$key.'" '.$checked.' style="transform: scale(1.5);margin-left: 10px;"> <label for="'.$key.'"> </label> '.'</td>';

    	}  

    	return response()->json(@$html);

    }




    //adding/store employee salary payment
    public function addingEmpSalary(Request $req){

        $date = date('Y-m', strtotime($req->date));

		if($req->employee_id !=null){

			empSalaryAccount::where('date',$date)->delete();

			$check = $req->checkmanage;

			if ($check !=null) {
				for ($i=0; $i <count($check) ; $i++) { 
					$salaryData = new empSalaryAccount(); 
					$salaryData->date = $date; 
					$salaryData->employee_id = $req->employee_id[$check[$i]];
					$salaryData->amount = $req->amount[$check[$i]];
					$salaryData->save();
				} 
			} // end if 

			//if (!empty(@$salaryData) || empty($check)) {

			$msg = array(
				'message' => 'Well Done, Data Stored Successfully',
				'alert-type' => 'success'
			);

    		return redirect()->route('empSalary.view')->with($msg);
    	}else{

    		$msg = array(
    		'message' => 'Sorry there are not employee data visible',
    		'alert-type' => 'error'
    	);

    	return redirect()->back()->with($msg);

    	} 
        
    }
}
