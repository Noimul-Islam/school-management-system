<?php

namespace App\Http\Controllers\backend_files\accounts_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\otherCostAccount;
use Image;
use \Crypt;

class otherCostAccController extends Controller
{
    
    
    //other cost account view
    public function viewOtherCost(){

        $allCost = otherCostAccount::orderBy('id','desc')->get();
        return view('admin_main.accounts.other_cost.otherCostAccountView',compact('allCost'));
    }




    //add other cost account view
    public function addOtherCost(){

        return view('admin_main.accounts.other_cost.addOtherCostAccount');
    }



    //adding other cost 
    public function addingOtherCost(Request $req){

        
        $costData = new otherCostAccount();

        if($req->file('image')){

            $image = $req->file('image');
            $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension(); //store the file's name as unique code
            image::make($image)->resize(128,128)->save('Upload/otherCostImage/'.$imageName);//file store path

            $imageUrl =  'Upload/otherCostImage/'.$imageName;
            $costData->image = $imageUrl;
        }

        $costData->desc = $req->desc;
        $costData->date = $req->date;
        $costData->amount = $req->amount;
        $costData->save();

        $msg = array(
    		'message' => 'Other Cost Data Stored Successfully',
    		'alert-type' => 'success'
    	);

    	return redirect()->route('otherCost.view')->with($msg);

    }



    //edit other cost account view
    public function editOtherCost($id){

        $dcry_id =  Crypt::decrypt($id); //decrypted id 
        $editCostData= otherCostAccount::findOrFail($dcry_id);

        return view('admin_main.accounts.other_cost.editOtherCostAccount',compact('editCostData'));
    }



    //editing other cost data
    public function editingOtherCost(Request $req){

        $id = $req->id;
        $costData = otherCostAccount::find($id);

        if($req->file('image')){

            $image = $req->file('image');
            unlink($costData->image);
            $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension(); //store the file's name as unique code
            image::make($image)->resize(128,128)->save('Upload/otherCostImage/'.$imageName);//file store path

            $imageUrl =  'Upload/otherCostImage/'.$imageName;
            $costData->image = $imageUrl;
        }

        $costData->desc = $req->desc;
        $costData->date = $req->date;
        $costData->amount = $req->amount;
        $costData->save();

        $msg = array(
    		'message' => 'Other Cost Data Updated Successfully',
    		'alert-type' => 'success'
    	);

    	return redirect()->route('otherCost.view')->with($msg);

    }


    public function deleteOtherCost($id){

        $dcry_id = Crypt::decrypt($id);
        $costData = otherCostAccount::find($dcry_id);
        unlink($costData->image);
        $costData->delete();

        return redirect()->route('otherCost.view');
    }
}
