<?php

namespace App\Http\Controllers\backend_files\students_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\stuFeeAmounts;
use App\Models\StuDiscount;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;
use Carbon\Carbon;
use \Crypt;
use PDF;

class stuMonthlyFeeController extends Controller
{


    //monthly fee view page
    public function viewMonthlyFee(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();

        return view('admin_main.student_management.stu_monthly_fee.stuMonthlyFeeView',compact('classes','years'));

    }



    //get monthly fee data
    public function getMonthlyFeeData(Request $request){

        $month = $request->month;
        $year_id = $request->year_id;
    	$class_id = $request->class_id; 
		$group_id = $request->group_id;      

    	if ($year_id !='') {
    	 	$where[] = ['year_id','like',$year_id.'%'];
    	}
    	if ($class_id !='') {
    	 	$where[] = ['class_id','like',$class_id.'%'];
    	}
		if ($group_id !='') {
			$where[] = ['group_id','like',$group_id.'%'];
	    }
    	 $allStudent = AssignStudent::with(['discountRelation','group_relation'])->where($where)->get();
    	 // dd($allStudent);
    	 $html['thsource']  = '<th>SL</th>';
    	 $html['thsource'] .= '<th>ID No</th>';
    	 $html['thsource'] .= '<th>Student Name</th>';
    	 $html['thsource'] .= '<th>Roll No</th>';
		 $html['thsource'] .= '<th>Group</th>';
    	 $html['thsource'] .= '<th>Monthly Fee</th>';
    	 $html['thsource'] .= '<th>Discount</th>';
    	 $html['thsource'] .= '<th>Student Fee </th>';
    	 $html['thsource'] .= '<th>Action</th>';


    	foreach($allStudent as $key => $v) {
    	 	$monthlyFee = stuFeeAmounts::where('feeCatgryId','3')->where('classId',$v->class_id)->first();
			//dd($registerFee);
    	 	$color = 'info';
    	 	$html[$key]['tdsource']  = '<td>'.($key+1).'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v['userrelation']['id_no'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v['userrelation']['name'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v->roll.'</td>';
			$html[$key]['tdsource'] .= '<td>'.$v['group_relation']['groups'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$monthlyFee->amount.'/='.'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v['discountRelation']['discount'].'%'.'</td>';
    	 	
    	 	$originalFee = $monthlyFee->amount;
    	 	$discount = $v['discountRelation']['discount'];
    	 	$discountAmount = $discount/100*$originalFee;
    	 	$Finalfee = (float)$originalFee-(float)$discountAmount;

    	 	$html[$key]['tdsource'] .='<td>'.$Finalfee.'/='.'</td>';
    	 	$html[$key]['tdsource'] .='<td>';
    	 	$html[$key]['tdsource'] .='<a class="btn btn-sm btn-'.$color.'" title="PaySlip" target="_blanks" href="'.route("paySlipMonthlyFee.data").'?class_id='.$v->class_id.'&student_id='.$v->student_id.'&month='.$month.'">Fee Slip</a>';
    	 	$html[$key]['tdsource'] .= '</td>';

    	}  
    	return response()->json(@$html);
    }



    // payment slip pdf
    public function getMonthlyFeeSlip(Request $request){

		$student_id = $request->student_id;
		$class_id = $request->class_id;
        $data['month'] = $request->month;

		$data['details'] = AssignStudent::with(['userrelation','discountRelation'])->where('student_id',$student_id)->where('class_id',$class_id)->first();

        $pdf = Pdf::loadView('admin_main.student_management.stu_monthly_fee.paySlipDetailsPDF',$data);
        return $pdf->stream('student_monthly_fee_slip.pdf');


    }
}
