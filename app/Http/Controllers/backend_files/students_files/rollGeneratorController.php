<?php

namespace App\Http\Controllers\backend_files\students_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\StuDiscount;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;


class rollGeneratorController extends Controller
{
    
    //role generate view page
    public function viewRollGenerator(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();
        

        return view('admin_main.student_management.stu_roll_generate.rollGeneratorView',compact('classes','years'));
    }

    //geting student data by javascript
    public function getStudentData(Request $req){

        $allStuData = AssignStudent::with(['userrelation'])->where('year_id',$req->year_id)->where('class_id',$req->class_id)->where('group_id',$req->group_id)->get();
        //dd($allStuData->toArray());
        return response()->json($allStuData); //data geting in json format
    }



    //role adding 
    public function addingStuRoll(Request $req){

        $classId = $req->class_id;
        $yearId = $req->year_id;
        $groupId = $req->group_id; 

        if($req->student_id !=null){

            for($i=0; $i <count($req->student_id); $i++){

                AssignStudent::where('class_id',$classId)->where('year_id',$yearId)->where('group_id',$groupId)->where('student_id', $req->student_id[$i])->update([

                    'roll' => $req->roll[$i]
                ]);
            
            }

            $msg= array(                        
                'message' => 'Student roll added successfully',
                'alert-type' => 'success' 
            );
    
            return redirect()->route('rollGenerate.view')->with($msg);

        }else{

            $msg= array(                        
                'message' => 'Sorry there are no students data',
                'alert-type' => 'error' 
            );
    
            return redirect()->back()->with($msg);
        }
               
    }




}
