<?php

namespace App\Http\Controllers\backend_files\students_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;
//use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\StuDiscount;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use Carbon\Carbon;
use \Crypt;
use Image;
use PDF;
use DB;


class stuRegisterController extends Controller
{
    
    //all registred students default search view
    public function viewRegisterStudent(){

        $classes = stuClasses::all();      
        $years = stuYears::orderBy('id','desc')->take(5)->get();
        
        $classId = stuClasses::orderBy('id','desc')->first()->id; //get last one id data of the class 
        $yearId = stuYears::orderBy('id','desc')->first()->id; //get last one id data of the year

        $allData = AssignStudent::where('class_id',$classId)->where('year_id',$yearId)->get();
        //dd($allData->toArray());
        return view('admin_main.student_management.stu_register.viewStudents',compact('allData','classes','years','classId','yearId'));
    }

    


    //search students by class and year wise
    public function searchStudent(Request $req){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();

        $classId = $req->class_id; //get one id data of class
        $yearId = $req->year_id; //get one id data of year
        $allData = AssignStudent::where('class_id',$classId)->where('year_id',$yearId)->get();
        
        return view('admin_main.student_management.stu_register.viewStudents',compact('allData','classes','years','classId','yearId'));
    }


    //add student view
    public function registerStudent(){

        $classes = stuClasses::all();
        $groups = stuGroups::orderBy('id','desc')->get();
        $years = stuYears::orderBy('id','desc')->take(3)->get();
        return view('admin_main.student_management.stu_register.addStudent',compact('classes','groups','years'));
    }



    //adding new student 
    public function addingStudent(Request $req){

        DB::transaction(function() use($req){
           
            $chkYear = stuYears::find($req->year_id)->years;
            $student = allUsers::where('userType','Student')->orderBy('id','DESC')->first();

            if($student == null){    //if there are no student data into the database then

                $fstReg = 0;
                $stuId = $fstReg+1;

                if($stuId < 10){

                    $id_no = '000'.$stuId;

                }elseif($stuId < 100){

                    $id_no = '00'.$stuId;

                }elseif($stuId < 1000){

                    $id_no = '0'.$stuId;
                }
            }else{

                $student = allUsers::where('userType','Student')->orderBy('id','DESC')->first()->id;

                $stuId = $student+1;

                if($stuId < 10){

                    $id_no = '000'.$stuId;

                }elseif($stuId < 100){

                    $id_no = '00'.$stuId;

                }elseif($stuId < 1000){

                    $id_no = '0'.$stuId;
                }
            }//end else

            $finalId = $chkYear.$id_no; //final unique id like 20220001
            $code= rand(100000, 999999); //6digit

            //allusers table data insert
            $userData = new allUsers();
            $userData->name = $req->name;
            $userData->userType = 'Student';
            $userData->password = Hash::make($code);
            $userData->gender = $req->gender;
            $userData->address = $req->address;
            $userData->phone = $req->phone;
            $userData->fatherName = $req->fatherName;
            $userData->motherName = $req->motherName;
            $userData->religion = $req->religion;
            $userData->id_no = $finalId;
            $userData->dob = date('Y-m-d',strtotime($req->dob));
            $userData->code = $code;

            if($req->file('profile_photo')){
                $image= $req->file('profile_photo');
                $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                Image::make($image)->resize(128,128)->save('Upload/studentImage/'.$imageName);
                $stor_url= 'Upload/studentImage/'.$imageName;
                $userData->profile_photo = $stor_url;
            }//end if
            $userData->save();

            //assign student table data insert
            $stuAssign = new AssignStudent();
            $stuAssign->student_id = $userData->id;
            $stuAssign->class_id = $req->class_id;
            $stuAssign->group_id = $req->group_id;
            $stuAssign->year_id = $req->year_id;
            $stuAssign->save();


            //discount student table data insert
            $discountData = new StuDiscount();
            $discountData->assign_stu_id = $stuAssign->id;
            $discountData->fee_catgry_id = '2';
            $discountData->discount = $req->discount;
            $discountData->save();

        });//end DB method

        $msg= array(                        
            'message' => 'Student Registred successfully',
            'alert-type' => 'success' 
        );

        return redirect()->route('students.view')->with($msg);
    }




    //edit registred student
    public function editStudent($stuId, $id){

        $dcry_stuId = Crypt::decrypt($stuId);
        $dcry_id = Crypt::decrypt($id);

        $classes = stuClasses::all();
        $groups = stuGroups::all();
        $years = stuYears::orderBy('id','desc')->take(3)->get();

        $editStuData = AssignStudent::with(['userrelation','discountRelation'])->where('student_id',$dcry_stuId)->where('id',$dcry_id)->first();
        //dd($editStuData->toArray());
        return view('admin_main.student_management.stu_register.editStudentPage',compact('editStuData','classes','groups','years'));
        
    }




    //editing student data 
    public function editingStudent(Request $req){

        DB::transaction(function() use($req){
            
            $stuId = $req->sid;

            //allusers table data update
            $userData = allUsers::where('id',$stuId)->first();
            $userData->name = $req->name;
            $userData->gender = $req->gender;
            $userData->address = $req->address;
            $userData->phone = $req->phone;
            $userData->fatherName = $req->fatherName;
            $userData->motherName = $req->motherName;
            $userData->religion = $req->religion;
            $userData->dob = date('Y-m-d',strtotime($req->dob));

            if($req->file('profile_photo')){
                $image= $req->file('profile_photo');
                if($userData->profile_photo){
                    unlink($userData->profile_photo);
                }
                $imageName= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                Image::make($image)->resize(128,128)->save('Upload/studentImage/'.$imageName);
                $stor_url= 'Upload/studentImage/'.$imageName;
                $userData->profile_photo = $stor_url;
            }//end if
            $userData->save();

            //assign student table data update
            $stuAssign = AssignStudent::where('id',$req->id)->where('student_id', $stuId)->first();
            $stuAssign->class_id = $req->class_id;
            $stuAssign->group_id = $req->group_id;
            $stuAssign->year_id = $req->year_id;
            $stuAssign->save();


            //discount student table data update
            $discountData = StuDiscount::where('assign_stu_id',$req->id)->first();
            $discountData->discount = $req->discount;
            $discountData->save();

        });//end DB method

        $msg= array(                        
            'message' => 'Student Data Updated successfully',
            'alert-type' => 'success' 
        );

        return redirect()->back()->with($msg);
    }



    //student promote view
    public function promoteStudent($stuId, $id){

        $dcry_stuId = Crypt::decrypt($stuId);
        $dcry_id = Crypt::decrypt($id);

        $classes = stuClasses::all();
        $groups = stuGroups::all();
        $years = stuYears::orderBy('id','desc')->take(1)->get();

        $editStuData = AssignStudent::with(['userrelation','discountRelation'])->where('student_id',$dcry_stuId)->where('id',$dcry_id)->first();
        return view('admin_main.student_management.stu_register.promoteStudentPage',compact('editStuData','classes','groups','years'));
    }



    //student promote view
    public function promotingStudent(Request $req){

        DB::transaction(function() use($req){
            
            $stuId = $req->sid;


            //assign student table data update
            $stuAssign = new AssignStudent();
            $stuAssign->student_id = $stuId;
            $stuAssign->class_id = $req->class_id;
            $stuAssign->group_id = $req->group_id;
            $stuAssign->year_id = $req->year_id;
            $stuAssign->save();


            //discount student table data update
            $discountData = new StuDiscount();
            $discountData->assign_stu_id = $stuAssign->id;
            $discountData->fee_catgry_id = '2';
            $discountData->discount = $req->discount;
            $discountData->save();

        });//end DB method

        $msg= array(                        
            'message' => 'Student Promoted successfully',
            'alert-type' => 'success' 
        );

        return redirect()->back()->with($msg);
    }



    //student details pdf
    public function detailsStudent($stuId, $id){

        $dcry_stuId = Crypt::decrypt($stuId);
        $dcry_id = Crypt::decrypt($id);

        $data['details'] = AssignStudent::with(['userrelation','discountRelation'])->where('student_id',$dcry_stuId)->where('id',$dcry_id)->first();

        $pdf = Pdf::loadView('admin_main.student_management.stu_register.detailsStudentPDF',$data);
        return $pdf->stream('student_details.pdf');
       
    }

}
