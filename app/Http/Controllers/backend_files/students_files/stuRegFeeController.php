<?php

namespace App\Http\Controllers\backend_files\students_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\stuFeeAmounts;
use App\Models\StuDiscount;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;
use Carbon\Carbon;
use \Crypt;
use PDF;

class stuRegFeeController extends Controller
{

    //reg fee view page
    public function viewRegisterFee(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();

        return view('admin_main.student_management.stu_reg_fee.stuRegFeeView',compact('classes','years'));
    }


    //get reg fee data
    public function getRegFeeData(Request $request){

        $year_id = $request->year_id;
    	$class_id = $request->class_id;
		$group_id = $request->group_id;

    	if ($year_id !='') {
    	 	$where[] = ['year_id','like',$year_id.'%'];
    	}
    	if ($class_id !='') {
    	 	$where[] = ['class_id','like',$class_id.'%'];
    	}
		if ($group_id !='') {
			$where[] = ['group_id','like',$group_id.'%'];
	   	}
    	 $allStudent = AssignStudent::with(['discountRelation','group_relation'])->where($where)->get();
    	 // dd($allStudent);
    	 $html['thsource']  = '<th>SL</th>';
    	 $html['thsource'] .= '<th>ID No</th>';
    	 $html['thsource'] .= '<th>Student Name</th>';
    	 $html['thsource'] .= '<th>Roll No</th>';
		 $html['thsource'] .= '<th>Group</th>';
    	 $html['thsource'] .= '<th>Reg Fee</th>';
    	 $html['thsource'] .= '<th>Discount </th>';
    	 $html['thsource'] .= '<th>Student Fee </th>';
    	 $html['thsource'] .= '<th>Action</th>';


    	foreach($allStudent as $key => $v) {
    	 	$registerFee = stuFeeAmounts::where('feeCatgryId','2')->where('classId',$v->class_id)->first();
			//dd($registerFee);
    	 	$color = 'info';
    	 	$html[$key]['tdsource']  = '<td>'.($key+1).'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v['userrelation']['id_no'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v['userrelation']['name'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v->roll.'</td>';
			$html[$key]['tdsource'] .= '<td>'.$v['group_relation']['groups'].'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$registerFee->amount.'/='.'</td>';
    	 	$html[$key]['tdsource'] .= '<td>'.$v['discountRelation']['discount'].'%'.'</td>';
    	 	
    	 	$originalFee = $registerFee->amount;
    	 	$discount = $v['discountRelation']['discount'];
    	 	$discountAmount = $discount/100*$originalFee;
    	 	$Finalfee = (float)$originalFee-(float)$discountAmount;

    	 	$html[$key]['tdsource'] .='<td>'.$Finalfee.'/='.'</td>';
    	 	$html[$key]['tdsource'] .='<td>';
    	 	$html[$key]['tdsource'] .='<a class="btn btn-sm btn-'.$color.'" title="PaySlip" target="_blanks" href="'.route("paySlipRegFee.data").'?class_id='.$v->class_id.'&student_id='.$v->student_id.'">Fee Slip</a>';
    	 	$html[$key]['tdsource'] .= '</td>';

    	}  
    	return response()->json(@$html);
    }


    // payment slip pdf
    public function getRegFeeSlip(Request $request){ 

		$student_id = $request->student_id;
		$class_id = $request->class_id;

		$data['details'] = AssignStudent::with(['userrelation','discountRelation','group_relation'])->where('student_id',$student_id)->where('class_id',$class_id)->first();
		//dd($data['details']->toArray());
        $pdf = Pdf::loadView('admin_main.student_management.stu_reg_fee.regPaySlipDetailsPDF',$data);
        return $pdf->stream('student_registration_fee_slip.pdf');


    }

}
