<?php

namespace App\Http\Controllers\backend_files\students_files;
//dd($allStuData->toArray());
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AssignStudent;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stu_attendence;
use Carbon\Carbon;
use \Crypt;

class stuAttentdanceController extends Controller
{
    
    
    //students attendance view
    public function viewStuAttendence(){

        $attentData = stu_attendence::select('class_id', 'group_id','year_id')->groupBy('class_id')->groupBy('group_id')->groupBy('year_id')->orderBy('class_id','desc')->get();  //get group by data

        return view('admin_main.student_management.stu_attentdance.viewStuAttentdeance',compact('attentData'));

    }



    //students attendance date wise view
    public function viewAttendanceDate($class, $group, $year){

        //$dcry_id = Crypt::decrypt($year);
        $attentData = stu_attendence::select('class_id', 'group_id','year_id','date')->where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->groupBy('class_id')->groupBy('group_id')->groupBy('year_id')->groupBy('date')->orderBy('id','desc')->get();
        return view('admin_main.student_management.stu_attentdance.viewAttendanceDate',compact('attentData'));
    }




    //add students attendance view
    public function addStuAttendance(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(1)->get();
        return view('admin_main.student_management.stu_attentdance.addStuAttentdeance',compact('classes','years'));

    }



    //geting student data by javascript
    public function getStudentData(Request $req){

        $allStuData = AssignStudent::with(['userrelation'])->where('year_id',$req->year_id)->where('class_id',$req->class_id)->where('group_id',$req->group_id)->get();
        
        return response()->json($allStuData); //data geting in json format
    }




    //adding students attendance
    public function addingStuAttendance(Request $req){
        stu_attendence::where('class_id',$req->class_id)->where('group_id',$req->group_id)->where('year_id',$req->year_id)->where('date', date('Y-m-d', strtotime($req->att_date)))->delete();
    
        if($req->student_id != null){
            $stuCount= count($req->student_id);
            
            for($i=0; $i < $stuCount; $i++){

                $attendStatus = 'attentdance_status'.$i;
                stu_attendence::insert([

                    'student_id' => $req->student_id[$i], 
                    'class_id' => $req->class_id,   
                    'group_id' => $req->group_id,
                    'year_id' => $req->year_id,    
                    'attentdance_status' => $req->$attendStatus,
                    'date' => date('Y-m-d',strtotime($req->att_date))
                ]);
            }//end for 
            $msg = array(
                'message' => 'Students Attendance Stored Successfully',
                'alert-type' => 'success'
            );

            return redirect()->route('stuAttendance.date',[$req->class_id, $req->group_id, $req->year_id])->with($msg);
        }else{
            $msg = array(
                'message' => 'Sorry there are no students data',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($msg);
        }
    	

    }


    

    //edit employee attendance view
    public function editStuAttendance($class, $group, $year, $date){

        $editableData = stu_attendence::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('date',$date)->get();
        return view('admin_main.student_management.stu_attentdance.editStuAttentdeance',compact('editableData'));

    }



    //details student attendance view
    public function detailsStuAttendance($class, $group, $year, $date){

        $detailData = stu_attendence::where('class_id',$class)->where('group_id',$group)->where('year_id',$year)->where('date',$date)->get();
        return view('admin_main.student_management.stu_attentdance.detailsStuAttentdeance',compact('detailData'));

    }
}
