<?php

namespace App\Http\Controllers\backend_files\stu_marks_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\stuFeeAmounts;
use App\Models\examTypes;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;
use App\Models\studentMarks;
use Carbon\Carbon;
use \Crypt;
use PDF;
use DB;

class stuMarksController extends Controller
{

    //student marks add/entry view
    public function viewMarksEntry(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(1)->get();
        $examType = examTypes::all();

        return view('admin_main.student-marks.stu_mark_entry.marksEntryView',compact('classes','years','examType'));

    }


    //student marks add/entry view
    public function addingMarksEntry(Request $req){

        $countStudent = $req->student_id;
        $classId = $req->class_id;
        $yearId = $req->year_id;
        $groupId = $req->group_id;
        $examId = $req->examType_id;
        $AssingSubId = $req->assign_sub_id;

        if($countStudent){

            for($i=0; $i <count($req->student_id); $i++){

                studentMarks::insert([

                    'student_id' => $req->student_id[$i],
                    'id_no' => $req->id_no[$i],
                    'class_id' => $classId,
                    'year_id' => $yearId,
                    'group_id' => $groupId,
                    'examType_id' => $examId,
                    'assign_sub_id' => $AssingSubId,
                    'marks' => $req->marks[$i],
                    'created_at' => Carbon::now()
                ]);     
            }

            $msg= array(                        
                'message' => 'Student Exam Marks Submited successfully',
                'alert-type' => 'success' 
            );
    
            return redirect()->route('marksEntry.view')->with($msg);
        }
    }




    //student marks edit view
    public function editMarksView(){

        $classes = stuClasses::all();
        $years = stuYears::orderBy('id','desc')->take(5)->get();
        $groups = stuGroups::all();
        $examType = examTypes::all();

        return view('admin_main.student-marks.stu_marks_edit.editExamMarksPage',compact('classes','years','groups','examType'));

    }


    //get student marks editable data by javascripy ajax
    public function editMarksGetData(Request $req){

        $classId = $req->class_id;
        $yearId = $req->year_id;
        $groupId = $req->group_id;
        $examId = $req->examType_id;
        $AssingSubId = $req->assign_sub_id;

        $getStuData = studentMarks::with(['userrelation'])->where('class_id',$classId)->where('year_id',$yearId)->where('group_id',$groupId)->where('examType_id',$examId)->where('assign_sub_id',$AssingSubId)->get();
        return response()->json($getStuData);
    }

    

    //student marks editing/update
    public function editingMarks(Request $req){

        $countStudent = $req->student_id;
        $classId = $req->class_id;
        $yearId = $req->year_id;
        $groupId = $req->group_id;
        $examId = $req->examType_id;
        $AssingSubId = $req->assign_sub_id;

        //$getStuData = studentMarks::with(['userrelation'])->where('class_id',$classId)->where('year_id',$yearId)->where('group_id',$groupId)->where('examType_id',$examId)->where('assign_sub_id',$AssingSubId)->first();

        if($countStudent){

            for($i=0; $i <count($req->student_id); $i++){

                studentMarks::where('class_id',$classId)->where('year_id',$yearId)->where('group_id',$groupId)->where('examType_id',$examId)->where('assign_sub_id',$AssingSubId)->where('student_id', $req->student_id[$i])->update([

                    'student_id' => $req->student_id[$i],
                    'id_no' => $req->id_no[$i],
                    'class_id' => $classId,
                    'year_id' => $yearId,
                    'group_id' => $groupId,
                    'examType_id' => $examId,
                    'assign_sub_id' => $AssingSubId,
                    'marks' => $req->marks[$i],
                    'created_at' => Carbon::now() 
                ]);     
            }

            $msg= array(                        
                'message' => 'Student Exam Marks Updated successfully',
                'alert-type' => 'success' 
            );
    
            return redirect()->back()->with($msg);
        }else{

    		$msg = array(
    		'message' => 'Sorry there are no data visible',
    		'alert-type' => 'error'
    	    );

    	    return redirect()->back()->with($msg);

    	}
    }
    
}
