<?php

namespace App\Http\Controllers\backend_files\stu_marks_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\allUsers;
use App\Models\AssignStudent;
use App\Models\marksGrade;
use App\Models\examTypes;
use App\Models\stuClasses;
use App\Models\stuGroups;
use App\Models\stuYears;
use App\Models\stuShifts;
use App\Models\studentMarks;
use Carbon\Carbon;
use \Crypt;
use PDF;
use DB;

class gradeEntryController extends Controller
{

    //marks grade view
    public function viewMarksGradeEntry(){

        $allGradeData = marksGrade::all();

        return view('admin_main.student-marks.stu_grade.allGradeViewPage',compact('allGradeData'));
    }


    //add marks grade view
    public function addMarksGrade(){

        return view('admin_main.student-marks.stu_grade.addGradeViewPage');
    }


    //adding marks grade
    public function addingMarksGrade(Request $req){


        $gradeData = new marksGrade();
        $gradeData->grade_name = $req->grade_name;
        $gradeData->grade_point = $req->grade_point;
        $gradeData->start_point = $req->start_point;
        $gradeData->end_point = $req->end_point;
        $gradeData->start_mark = $req->start_mark;
        $gradeData->end_mark = $req->end_mark;
        $gradeData->remarks = $req->remarks;
        $gradeData->save();

        $msg= array(                        
            'message' => 'Marks Grade Inserted Successfully',
            'alert-type' => 'success' 
        );

        return redirect()->route('marksGradeEntry.view')->with($msg);
    }


    //marks grade edit view
    public function editMarksGradeView($id){

        $dcry_id = Crypt::decrypt($id);
        $GradeData = marksGrade::find($dcry_id);
        return view('admin_main.student-marks.stu_grade.editGradeViewPage',compact('GradeData'));
    }


    //adding marks grade
    public function editingMarksGrade(Request $req){

        $gradeData = marksGrade::find($req->id);
        $gradeData->grade_name = $req->grade_name;
        $gradeData->grade_point = $req->grade_point;
        $gradeData->start_point = $req->start_point;
        $gradeData->end_point = $req->end_point;
        $gradeData->start_mark = $req->start_mark;
        $gradeData->end_mark = $req->end_mark;
        $gradeData->remarks = $req->remarks;
        $gradeData->save();

        $msg= array(                        
            'message' => 'Marks Grade Updated Successfully',
            'alert-type' => 'success' 
        );

        return redirect()->route('marksGradeEntry.view')->with($msg);
    }



    //marks grade edit view
    public function deleteMarksGrade($id){

        $dcry_id = Crypt::decrypt($id);
        $GradeData = marksGrade::find($dcry_id);
        $GradeData->delete();

        return redirect()->back();
        
    }
}
