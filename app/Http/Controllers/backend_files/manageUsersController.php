<?php

namespace App\Http\Controllers\backend_files;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\allUsers;
use Carbon\Carbon;
use \Crypt;

class manageUsersController extends Controller
{
    //show all users
    public function viewUser(){

        $allUsers = allUsers::where('userType','Admin')->orderBy('id','desc')->get();
        return view('admin_main.users.all_users',compact('allUsers'));
    }


    //add new user view
    public function addUser(){

        return view('admin_main.users.addUser');
    }



    //add new user process
    public function addingUser(Request $req){

        $email = allUsers::where('email', $req->email)->get();

        # check if email is more than 1
        if(sizeof($email) > 0){ 
            
            $msg = array(
                'message' => "This email is already exist",
                'alert-type' => 'warning'
            );
            return redirect()->back()->withInput()->with($msg);

        }else{

            $code= rand(100000, 999999); // random pass code generated 0-9 (6digit)
            
            allUsers::insert([

                'name' => $req->name,
                'email' => $req->email,
                'userType' => 'Admin',
                'password' => Hash::make($code),
                'code' => $code,
                'role' => $req->role,
                'created_at' => Carbon::now()
            ]);
    
            $msg = array(
                'message' => "New User Added Successfully",
                'alert-type' => 'success'
            );
            return redirect()->route('Users.view')->with($msg);

        }
    }




    //edit User admin view
    public function editUser($id){

        $dcry_id = Crypt::decrypt($id);
        $userData = allUsers::findOrFail($dcry_id);

        return view('admin_main.users.editUser', compact('userData'));
    }




    //Editing user process
    public function editingUser(Request $request){

        $id= $request->id;
        $name= $request->name;
        $email= $request->email;
        $role= $request->role;

        $data = allUsers::find($id);
        $data->name = $name;
        $data->email = $email;
        $data->role = $role;
        $data->save();

        $msg = array(
            'message' => "User Updated Successfully",
            'alert-type' => 'success'
        );
        return redirect()->route('Users.view')->with($msg);

    }



    //delete user
    public function deleteUser($id){

        $dcry_id =  Crypt::decrypt($id); //decrypted id 
        $userData= allUsers::findOrFail($dcry_id);

        $userData->delete();
        //User::findOrFail($dcry_id)->delete();

        $msg = array(
            'message' => 'The User Deleted Successfully', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($msg);
    }
}
