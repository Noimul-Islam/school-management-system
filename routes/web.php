<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backend_files\usersController;
use App\Http\Controllers\backend_files\profileController;
use App\Http\Controllers\backend_files\manageUsersController;
use App\Http\Controllers\backend_files\defaultController;

use App\Http\Controllers\backend_files\setup_files\stuClassesController;
use App\Http\Controllers\backend_files\setup_files\stuYearsController;
use App\Http\Controllers\backend_files\setup_files\stuGroupController;
use App\Http\Controllers\backend_files\setup_files\qualifyController;
use App\Http\Controllers\backend_files\setup_files\stuFeeCategoryController;
use App\Http\Controllers\backend_files\setup_files\stuFeeAmountController;
use App\Http\Controllers\backend_files\setup_files\examTypeController;
use App\Http\Controllers\backend_files\setup_files\subjectsController;
use App\Http\Controllers\backend_files\setup_files\subAssignController;
use App\Http\Controllers\backend_files\setup_files\designationController;
use App\Http\Controllers\backend_files\setup_files\empLeavePerposeSetupController;

use App\Http\Controllers\backend_files\students_files\stuRegisterController;
use App\Http\Controllers\backend_files\students_files\rollGeneratorController;
use App\Http\Controllers\backend_files\students_files\stuAttentdanceController;
use App\Http\Controllers\backend_files\students_files\stuRegFeeController;
use App\Http\Controllers\backend_files\students_files\stuMonthlyFeeController;
use App\Http\Controllers\backend_files\students_files\stuExamFeeController;

use App\Http\Controllers\backend_files\employee_files\empRegistrationController;
use App\Http\Controllers\backend_files\employee_files\empSalaryController;
use App\Http\Controllers\backend_files\employee_files\empLeaveController;
use App\Http\Controllers\backend_files\employee_files\empAttentdanceController;
use App\Http\Controllers\backend_files\employee_files\empMonthlySalaryController;

use App\Http\Controllers\backend_files\stu_marks_files\stuMarksController;
use App\Http\Controllers\backend_files\stu_marks_files\gradeEntryController;

use App\Http\Controllers\backend_files\accounts_files\stuFeeController;
use App\Http\Controllers\backend_files\accounts_files\empSalaryPaymentController;
use App\Http\Controllers\backend_files\accounts_files\otherCostAccController;

use App\Http\Controllers\backend_files\report_files\profitReportController;
use App\Http\Controllers\backend_files\report_files\marksheetReportController;
use App\Http\Controllers\backend_files\report_files\empAttendReportController;
use App\Http\Controllers\backend_files\report_files\stuResultReportController; //student Id card generate included





Route::group(['middleware' => 'prevent-back-history'],function(){

//login page route
Route::get('/', function () {
    return view('admin_main.adminLoginPage');
});



//admin authentication group route
Route::prefix('admin')->group(function(){
    Route::get('/register', [usersController::class, 'indexRegister'])->name('regist.form');
    Route::post('/register/process', [usersController::class, 'registProcess'])->name('register.Process');
    Route::get('/login', [usersController::class, 'indexLogin'])->name('login.form');
    Route::post('/login/process', [usersController::class, 'loginProcess'])->name('admin.logging');
    Route::get('/logOut', [usersController::class, 'logOut'])->name('admin.logOut')->middleware('user');
    Route::get('/dashbord', [usersController::class, 'dashbord'])->name('admin.dashbord')->middleware('user');
    
});


//Manage profile & password group controller route
Route::controller(profileController::class)->group(function(){
    Route::get('/view/profile','viewProfile')->name('view.profile')->middleware('user');
    Route::get('/view/edit/profile','viewEditProfile')->name('edit.profile')->middleware('user');
    Route::post('/updating/profile','updateProfile')->name('updating.profile')->middleware('user');
    Route::get('/change/pass','viewChangePass')->name('view.change.pass')->middleware('user');
    Route::post('/updating/pass','updatingPass')->name('updationg.pass')->middleware('user');
   
});


//Manage Users group controller route
Route::controller(manageUsersController::class)->group(function(){
    Route::get('/view/user','viewUser')->name('Users.view')->middleware('user'); //all users
    Route::get('/add/user','addUser')->name('User.add')->middleware('user');
    Route::post('/adding/user','addingUser')->name('User.adding')->middleware('user');
    Route::get('/edit/user/{id}','editUser')->name('User.edit')->middleware('user');
    Route::post('/editing/user','editingUser')->name('User.editing')->middleware('user');
    Route::get('/delete/user/{id}','deleteUser')->name('User.delete')->middleware('user');
   
});



Route::prefix('setup')->group(function(){
    //Class Setup management group controller
    Route::controller(stuClassesController::class)->group(function(){
        Route::get('/view/classes','viewClasses')->name('classes.view')->middleware('user');
        Route::get('/add/classes','addClasses')->name('classes.add')->middleware('user');
        Route::post('/adding/classes','addingClass')->name('class.adding')->middleware('user');
        Route::get('/edit/class/{id}','editClass')->name('class.edit')->middleware('user');
        Route::post('/editing/class','editingClass')->name('class.editing')->middleware('user');
        Route::get('/delete/class/{id}','deleteClass')->name('class.delete')->middleware('user');
    });

    //Year Setup management group controller
    Route::controller(stuYearsController::class)->group(function(){
        Route::get('/view/years','viewYears')->name('years.view')->middleware('user');
        Route::get('/add/year','addYear')->name('year.add')->middleware('user');
        Route::post('/adding/year','addingyear')->name('year.adding')->middleware('user');
        Route::get('/edit/year/{id}','editYear')->name('year.edit')->middleware('user');
        Route::post('/editing/year','editingYear')->name('year.editing')->middleware('user');
        Route::get('/delete/year/{id}','deleteYear')->name('year.delete')->middleware('user');
    });

    //Group Setup management group controller
    Route::controller(stuGroupController::class)->group(function(){
        Route::get('/view/groups','viewGroups')->name('groups.view')->middleware('user');
        Route::get('/add/group','addGroup')->name('group.add')->middleware('user');
        Route::post('/adding/group','addingGroup')->name('group.adding')->middleware('user');
        Route::get('/edit/group/{id}','editGroup')->name('group.edit')->middleware('user');
        Route::post('/editing/group','editingGroup')->name('group.editing')->middleware('user');
        Route::get('/delete/group/{id}','deleteGroup')->name('group.delete')->middleware('user');
    });

    //Fee category group controller
    Route::controller(stuFeeCategoryController::class)->group(function(){
        Route::get('/view/fee/category','viewFeeCatgry')->name('feeCategory.view')->middleware('user');
        Route::get('/add/fee/category','addFeeCatgry')->name('feeCategory.add')->middleware('user');
        Route::post('/adding/fee/category','addingFeeCatgry')->name('feeCategory.adding')->middleware('user');
        Route::get('/edit/fee/category/{id}','editFeeCatgry')->name('feeCategory.edit')->middleware('user');
        Route::post('/editing/fee/category','editingFeeCatgry')->name('feeCategory.editing')->middleware('user');
        Route::get('/delete/fee/category/{id}','deleteFeeCatgry')->name('feeCategory.delete')->middleware('user');
    });

    //Fee category AMOUNT group controller
    Route::controller(stuFeeAmountController::class)->group(function(){
        Route::get('/view/fee/amount','viewFeeAmount')->name('feeAmount.view')->middleware('user');
        Route::get('/add/fee/amount','addFeeAmount')->name('feeAmount.add')->middleware('user');
        Route::post('/adding/fee/amount','addingFeeAmount')->name('feeAmount.adding')->middleware('user');
        Route::get('/edit/fee/amount/{feeCatgryId}','editFeeAmount')->name('feeAmount.edit')->middleware('user');
        Route::post('/editing/fee/amount','editingFeeAmount')->name('feeAmount.editing')->middleware('user');
        Route::get('/details/fee/amount/{feeCatgryId}','detailsFeeAmount')->name('feeAmount.details')->middleware('user');
    });

    //Exam Type group controller
    Route::controller(examTypeController::class)->group(function(){
        Route::get('/view/exam/types','viewExamType')->name('examType.view')->middleware('user');
        Route::get('/add/exam/type','addExamType')->name('examType.add')->middleware('user');
        Route::post('/adding/exam/type','addingExamType')->name('examType.adding')->middleware('user');
        Route::get('/edit/exam/type/{id}','editExamType')->name('examType.edit')->middleware('user');
        Route::post('/editing/exam/type','editingExamType')->name('examType.editing')->middleware('user');
        Route::get('/delete/exam/type/{id}','deleteExamType')->name('examType.delete')->middleware('user');
    });

    //Subjects group controller
    Route::controller(subjectsController::class)->group(function(){
        Route::get('/view/all/subjects','viewSubjects')->name('subjects.view')->middleware('user');
        Route::get('/add/subject','addSubject')->name('subject.add')->middleware('user');
        Route::post('/adding/subject','addingSubjecte')->name('subject.adding')->middleware('user');
        Route::get('/edit/subject/{id}','editSubject')->name('subject.edit')->middleware('user');
        Route::post('/editing/subject','editingSubject')->name('subject.editing')->middleware('user');
        Route::get('/delete/subject/{id}','deleteSubject')->name('subject.delete')->middleware('user');
    });

    //Assign Subjects group controller
    Route::controller(subAssignController::class)->group(function(){
        Route::get('/view/assigned/subjects','viewAssignSub')->name('assignSubs.view')->middleware('user');
        Route::get('/add/subject/assign','addSubAssign')->name('SubAssign.add')->middleware('user');
        Route::post('/adding/subject/assign','addingSubAssign')->name('SubAssign.adding')->middleware('user');
        Route::get('/edit/assigned/subject/{classId}','editAssignSub')->name('AssignSub.edit')->middleware('user');
        Route::post('/editing/assigned/subject','editingAssignSub')->name('AssignSub.editing')->middleware('user');
        Route::get('/details/assigned/subject/{classId}','detailsAssignSub')->name('AssignSub.details')->middleware('user');
    });

    //Qualification Setup management group controller
    Route::controller(qualifyController::class)->group(function(){
        Route::get('/view/qualification','viewQualify')->name('qualification.view')->middleware('user');
        Route::get('/add/qualification','addQualify')->name('qualification.add')->middleware('user');
        Route::post('/adding/qualification','addingQualify')->name('qualification.adding')->middleware('user');
        Route::get('/edit/qualification/{id}','editQualify')->name('qualification.edit')->middleware('user');
        Route::post('/editing/qualification','editingQualify')->name('qualification.editing')->middleware('user');
        Route::get('/delete/qualification/{id}','deleteQualify')->name('qualification.delete')->middleware('user');
    });

    //designations group controller
    Route::controller(designationController::class)->group(function(){
        Route::get('/view/designations','viewDesignatios')->name('designations.view')->middleware('user');
        Route::get('/add/designation','addDesignation')->name('designation.add')->middleware('user');
        Route::post('/adding/designation','addingDesignation')->name('designation.adding')->middleware('user');
        Route::get('/edit/designation/{id}','editDesignation')->name('designation.edit')->middleware('user');
        Route::post('/editing/designation','editingDesignation')->name('designation.editing')->middleware('user');
        Route::get('/delete/designation/{id}','deleteDesignation')->name('designation.delete')->middleware('user');
    });

    //leave perpouse group controller
    Route::controller(empLeavePerposeSetupController::class)->group(function(){
        Route::get('/view/leave/purposes','viewPurposes')->name('purposes.view')->middleware('user');
        Route::get('/add/leave/purpose','addPurpose')->name('purpose.add')->middleware('user');
        Route::post('/adding/leave/purpose','addingPurpose')->name('purpose.adding')->middleware('user');
        Route::get('/edit/leave/purpose/{id}','editPurpose')->name('purpose.edit')->middleware('user');
        Route::post('/editing/leave/purpose','editingPurpose')->name('purpose.editing')->middleware('user');
        Route::get('/delete/leave/purpose/{id}','deletePurpose')->name('purpose.delete')->middleware('user');
    });
});





//Students managment prefix route
Route::prefix('students')->group(function(){

    //all students view and student register group controller
    Route::controller(stuRegisterController::class)->group(function(){
        Route::get('/view/page','viewRegisterStudent')->name('students.view')->middleware('user');  //all students
        Route::get('/add/page','registerStudent')->name('student.register')->middleware('user'); //add student
        Route::post('/adding/page','addingStudent')->name('student.adding')->middleware('user');
        Route::get('/search/process','searchStudent')->name('student.search')->middleware('user');
        Route::get('/edit/page/{stuId}/{id}','editStudent')->name('student.edit')->middleware('user');
        Route::post('/editing/page','editingStudent')->name('student.editing')->middleware('user');
        Route::get('/promote/page/{stuId}/{id}','promoteStudent')->name('student.promote')->middleware('user');
        Route::post('/promoting/page','promotingStudent')->name('student.promoting')->middleware('user');
        Route::get('/details/pdf/{stuId}/{id}','detailsStudent')->name('student.details')->middleware('user');
    });

    //student roll generator group controller
    Route::controller(rollGeneratorController::class)->group(function(){
        Route::get('/roll/generate/page','viewRollGenerator')->name('rollGenerate.view')->middleware('user');  
        Route::get('/data/get','getStudentData')->name('get.student.data')->middleware('user'); 
        Route::post('/role/adding','addingStuRoll')->name('student.roll.adding')->middleware('user');
    });
    //student id card generator group controller
    Route::controller(stuResultReportController::class)->group(function(){
        Route::get('/student/id/card/page','viewStuIdCard')->name('stuIdCard.view')->middleware('user'); 
        Route::get('/student/id/card/get/data','getStuIdCardData')->name('getStuIdCard.data')->middleware('user'); 
        Route::get('/student/id/card/print/{class_id}/{year_id}/{group_id}','idCardPrint')->name('idCard.print')->middleware('user'); 
    });

    //student attendance group controller 
    Route::controller(stuAttentdanceController::class)->group(function(){
        Route::get('/attendance/page','viewStuAttendence')->name('stuAttendence.view')->middleware('user');  
        Route::get('/attendance/date/page/{class}/{group}/{year}','viewAttendanceDate')->name('stuAttendance.date')->middleware('user'); 
        Route::get('/attendance/add/page','addStuAttendance')->name('stuAttendance.add')->middleware('user'); 
        Route::get('/attendance/data/get','getStudentData')->name('get.student.data')->middleware('user');
        Route::post('/attendance/adding','addingStuAttendance')->name('stuAttendance.adding')->middleware('user');
        Route::get('/attendance/details/{class}/{group}/{year}/{date}','detailsStuAttendance')->name('stuAttendance.details')->middleware('user');
        Route::get('/attendance/edit/page/{class}/{group}/{year}/{date}','editStuAttendance')->name('stuAttendance.edit')->middleware('user');
    });

    //student reg fee group controller
    Route::controller(stuRegFeeController::class)->group(function(){
        Route::get('/register/fee/page','viewRegisterFee')->name('registerFee.view')->middleware('user');  
        Route::get('/register/fee/data/get','getRegFeeData')->name('getRegFee.data')->middleware('user');
        Route::get('/register/fee/paySlip','getRegFeeSlip')->name('paySlipRegFee.data')->middleware('user');
    });

    //student monthly fee group controller
    Route::controller(stuMonthlyFeeController::class)->group(function(){
        Route::get('/monthly/fee/page','viewMonthlyFee')->name('monthlyFee.view')->middleware('user');  
        Route::get('/monthly/fee/data/get','getMonthlyFeeData')->name('getMonthlyFee.data')->middleware('user');
        Route::get('/monthly/fee/paySlip','getMonthlyFeeSlip')->name('paySlipMonthlyFee.data')->middleware('user');
                
    });

    //student exam fee group controller
    Route::controller(stuExamFeeController::class)->group(function(){
        Route::get('/exam/fee/page','viewExamFee')->name('examFee.view')->middleware('user');  
        Route::get('/exam/fee/data/get','getExamFeeData')->name('getExamFee.data')->middleware('user');
        Route::get('/exam/fee/paySlip','getExamFeeSlip')->name('paySlipExamFee.data')->middleware('user');
                
    });

});




//Employee managment prefix route
Route::prefix('employee')->group(function(){

    //employee register group controller
    Route::controller(empRegistrationController::class)->group(function(){
        Route::get('/view/page','viewRegisterEmployees')->name('employees.view')->middleware('user');  //all employees
        Route::get('/add/page','registerEmployee')->name('employee.register')->middleware('user'); //add employee
        Route::post('/adding/page','addingEmployee')->name('employee.adding')->middleware('user');
        Route::get('/edit/page/{empId}','editEmployee')->name('employee.edit')->middleware('user');
        Route::post('/editing/page','editingEmployee')->name('employee.editing')->middleware('user');
        Route::get('/details/pdf/{empId}','detailsEmployee')->name('employee.details')->middleware('user');
    });


    //employee salary manage group controller
    Route::controller(empSalaryController::class)->group(function(){
        Route::get('/salary/page','viewEmpSalarys')->name('empSalarys.view')->middleware('user');  
        Route::get('/salary/increment/page/{empId}','empSalaryIncrement')->name('empSalary.increment')->middleware('user'); 
        Route::post('/salary/incrementing','salaryIncrementing')->name('salary.incrementing')->middleware('user');
        Route::get('/salary/details/{empId}','detailsSalaryHistory')->name('empSalary.details')->middleware('user');
    });


    //employee Leave manage group controller
    Route::controller(empLeaveController::class)->group(function(){
        Route::get('/leaves/page','viewEmpLeave')->name('empLeave.view')->middleware('user');  
        Route::get('/leave/add/page','addEmpLeave')->name('empLeave.add')->middleware('user');
        Route::post('/leave/adding/page','addingEmpLeave')->name('empLeave.adding')->middleware('user');
        Route::get('/leave/details/page/{Id}','detailsEmpLeave')->name('empLeave.details')->middleware('user');
        Route::get('/leave/edit/page/{Id}','editEmpLeave')->name('empLeave.edit')->middleware('user');
        Route::post('/leave/editing/page','editingEmpLeave')->name('empLeave.editing')->middleware('user');
        Route::get('/leave/delete/page/{Id}','deleteEmpLeave')->name('empLeave.delete')->middleware('user');
        
    });

    //employee attendance group controller 
    Route::controller(empAttentdanceController::class)->group(function(){
        Route::get('/attendance/page','viewEmpAttendance')->name('empAttendance.view')->middleware('user');  
        Route::get('/attendance/date/page/{year}','viewAttendanceDate')->name('empAttendance.date')->middleware('user'); 
        Route::get('/attendance/add/page','addEmpAttendance')->name('empAttendance.add')->middleware('user'); 
        Route::post('/attendance/adding','addingEmpAttendance')->name('empAttendance.adding')->middleware('user');
        Route::get('/attendance/details/{date}','detailsEmpAttendance')->name('empAttendance.details')->middleware('user');
        Route::get('/attendance/edit/page/{date}','editEmpAttendance')->name('empAttendance.edit')->middleware('user');
    });

    //employee monthly salary group controllers
    Route::controller(empMonthlySalaryController::class)->group(function(){
        Route::get('/monthly/salary/page','viewMonthlySalary')->name('monthSalary.view')->middleware('user');  
        Route::get('/monthly/salary/get','getMonthlySalaryData')->name('getMonthSalary.data')->middleware('user'); 
        Route::get('/monthly/salary/paySlip/{empId}/{date}','getMonthlySalarySlip')->name('paySlipMonthlySalary.data')->middleware('user');   
    });

});




//Students marks managment prefix route
Route::prefix('marks')->group(function(){

    //students marks entry group controllers
    Route::controller(stuMarksController::class)->group(function(){
        Route::get('/entry/page','viewMarksEntry')->name('marksEntry.view')->middleware('user');  
        Route::post('/adding','addingMarksEntry')->name('marksEntry.adding')->middleware('user'); 
        Route::get('/edit/page','editMarksView')->name('marksEdit.view')->middleware('user');
        Route::get('/edit/data/get','editMarksGetData')->name('editMarks.getStudentsData')->middleware('user');
        Route::post('/editing','editingMarks')->name('marks.editing')->middleware('user'); //update
    });

    //marks grades entry group controllers
    Route::controller(gradeEntryController::class)->group(function(){
        Route::get('/grade/entry/page','viewMarksGradeEntry')->name('marksGradeEntry.view')->middleware('user'); 
        Route::get('/grade/add/page','addMarksGrade')->name('marksGrade.add')->middleware('user'); 
        Route::post('/grade/adding','addingMarksGrade')->name('marksGrade.adding')->middleware('user'); 
        Route::get('/grade/edit/page/{id}','editMarksGradeView')->name('MarksGrade.edit')->middleware('user');
        Route::post('/grade/editing','editingMarksGrade')->name('MarksGrade.editing')->middleware('user');//update
        Route::get('/grade/delete/{id}','deleteMarksGrade')->name('MarksGrade.delete')->middleware('user');
    });

});

//stubject's, group's, student's data get by class waise 
Route::get('marks/subjects/data/get', [defaultController::class, 'getSubjectData'])->name('marks.getSubjectData');
Route::get('marks/groups/data/get', [defaultController::class, 'getGroupData'])->name('marks.getGroupData');
Route::get('marks/students/data/get', [defaultController::class, 'getStudentsData'])->name('marks.getStudentsData');




//accounts managment prefix route
Route::prefix('account')->group(function(){


    //students fee payment group controllers
    Route::controller(stuFeeController::class)->group(function(){
        Route::get('/student/fee/page','viewStudentFee')->name('studentFee.view')->middleware('user'); 
        Route::get('/student/fee/class/page/{feeCat}/{year}','viewClassList')->name('studentClass.view')->middleware('user');
        Route::get('/student/fee/list/page/{feeCat}/{year}/{class}/{group}','viewStuFeeDetailsList')->name('studentFeeList.view')->middleware('user');
        Route::get('/student/fee/add/page','addStudentFee')->name('studentFee.add')->middleware('user'); 
        Route::post('/student/fee/adding','addingStudentFee')->name('studentFee.adding')->middleware('user'); 
        Route::get('/student/fee/get/data','getStudentFeeData')->name('studentFeeData.get')->middleware('user');
        Route::get('/student/monthly/fee/Slip/{id}','getMonthlyFeeSlip')->name('paidSlipMonthlyFee.data')->middleware('user');
        Route::get('/student/exam/fee/Slip/{id}','getExamFeeSlip')->name('paidSlipExamFee.data')->middleware('user');
    });


    //employee salary payment group controllers
    Route::controller(empSalaryPaymentController::class)->group(function(){
        Route::get('/employee/salary/page','viewEmpSalary')->name('empSalary.view')->middleware('user'); 
        Route::get('/employee/salary/add/page','addEmpSalary')->name('empSalary.add')->middleware('user'); 
        Route::post('/employee/salary/adding','addingEmpSalary')->name('empSalary.adding')->middleware('user'); 
        Route::get('/employee/salary/get/data','getEmpSalaryData')->name('empSalaryData.get')->middleware('user');
    });

    //other cost group controllers
    Route::controller(otherCostAccController::class)->group(function(){
        Route::get('/other/costs/page','viewOtherCost')->name('otherCost.view')->middleware('user'); 
        Route::get('/other/cost/add/page','addOtherCost')->name('otherCost.add')->middleware('user'); 
        Route::post('/other/cost/adding','addingOtherCost')->name('otherCost.adding')->middleware('user'); 
        Route::get('/other/cost/edit/page/{id}','editOtherCost')->name('otherCost.edit')->middleware('user');
        Route::post('/other/cost/editing','editingOtherCost')->name('otherCost.editing')->middleware('user');
        Route::get('/other/cost/delete/{id}','deleteOtherCost')->name('otherCost.delete')->middleware('user');
    });

});






//report managment prefix route
Route::prefix('report')->group(function(){

    //monthly yearly profit group controllers
    Route::controller(profitReportController::class)->group(function(){
        Route::get('/monthly/profit/page','viewMonthlyProfit')->name('monthlyProfit.view')->middleware('user'); 
        Route::get('/profit/get/data','getProfitReportData')->name('getProfitReport.data')->middleware('user'); 
        Route::get('/profit/details/pdf','detailsProfitReport')->name('ProfitReportPdf.details')->middleware('user');
    });

    //marksheet report generate group controllers
    Route::controller(marksheetReportController::class)->group(function(){
        Route::get('/marksheet/generate/page','viewMarksheetReport')->name('markSheetReport.view')->middleware('user'); 
        Route::get('/marksheet/get/data','getMarksheetReportData')->name('getMarksheetReport.data')->middleware('user'); 
        Route::get('/marksheet/print/{id}','printMarksheetReport')->name('markSheetReport.print')->middleware('user');
    });

    //employee attendence report group controllers
    Route::controller(empAttendReportController::class)->group(function(){
        Route::get('/employee/attendance/view/page','viewEmpAttendReport')->name('empAttendReport.view')->middleware('user'); 
        Route::get('/employee/attendance/get/data','getEmpAttendData')->name('getempAttendReport.data')->middleware('user'); 
    });

    //student result report group controllers
    Route::controller(stuResultReportController::class)->group(function(){
        Route::get('/student/result/view/page','viewStuResultReport')->name('stuResultReport.view')->middleware('user'); 
        Route::get('/student/result/get/data','getStuResultData')->name('getStuResultReport.data')->middleware('user'); 
        Route::get('/student/result/print/{class_id}/{year_id}/{group_id}/{exam_id}','printResultsReport')->name('sturesultReport.print')->middleware('user');
    });

    //student Id card generate 
    Route::controller(stuResultReportController::class)->group(function(){
        Route::get('/student/id/card/page','viewStuIdCard')->name('stuIdCard.view')->middleware('user'); 
        Route::get('/student/id/card/get/data','getStuIdCardData')->name('getStuIdCard.data')->middleware('user'); 
        Route::get('/student/id/card/print/{class_id}/{year_id}/{group_id}','idCardPrint')->name('idCard.print')->middleware('user'); 
    });


});

require __DIR__.'/auth.php';

}); //prevent back history after log out




